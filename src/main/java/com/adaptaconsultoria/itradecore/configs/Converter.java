package com.adaptaconsultoria.itradecore.configs;

import java.beans.PropertyEditorSupport;

import org.springframework.data.jpa.repository.JpaRepository;

public class Converter extends PropertyEditorSupport {

	private JpaRepository<Object, Long> repository;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Converter(JpaRepository repository) {
		this.repository = repository;
	}

	@Override
	public void setAsText(String text) {
		Object object = null;
		try {
			// transforma a String com o id em um long
			Long id = new Long(text);
			// recupera no db o perfil do id referido
			object = repository.findById(id).get();
			// add o objeto perfil encontrado no objeto user no controller através do método
			// setValue da super-classe.
		} catch (Exception e) {
		}
		super.setValue(object);
	}

}
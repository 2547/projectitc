package com.adaptaconsultoria.itradecore.configs;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.adaptaconsultoria.itradecore.models.LogAccess;
import com.adaptaconsultoria.itradecore.services.LogAccessService;

@Configuration
public class Interceptor extends HandlerInterceptorAdapter {

	@Autowired private LogAccessService logAcessoService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		Map<String, String[]> parameters = request.getParameterMap();
		Set<Entry<String, String[]>> entrySet = parameters.entrySet();
		Iterator<Entry<String, String[]>> it = entrySet.iterator();

		String parametros = "";
		boolean f = true;
		while(it.hasNext()){
			Map.Entry<String,String[]> entry = (Map.Entry<String,String[]>)it.next();
			String key     = entry.getKey();
			String[] value = entry.getValue();

			if (f) f = false;
			else parametros += "; ";

			parametros += key + ": ";
			boolean first = true;
			for (String val : value) {
				if (first) first = false;
				else parametros += ", ";
				parametros += val;
			}
		}

		System.out.println(request.getMethod() + ": " + request.getRequestURI());

		LogAccess logAcesso = new LogAccess();
		logAcesso.setIpAddress(request.getRemoteAddr());
		logAcesso.setUrl(request.getRequestURL().toString());
		logAcesso.setUri(request.getRequestURI());
		logAcesso.setContext(request.getContextPath());
		logAcesso.setMethod(request.getMethod());
		logAcesso.setQueryString(request.getQueryString());
		logAcesso.setParameters(parametros);
		logAcesso.setName(request.getServerName());
		logAcesso.setDeviceName(request.getLocalName());

		logAcesso = logAcessoService.save(logAcesso);

		return super.preHandle(request, response, handler);
	}

}

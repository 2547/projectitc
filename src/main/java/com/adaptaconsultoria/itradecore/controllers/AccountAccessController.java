package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.repositories.AccountRepository;
import com.adaptaconsultoria.itradecore.services.ctrls.AccountControllerService;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Controller
@RequestMapping("/accountaccess")
public class AccountAccessController {

	@Autowired private AccountRepository accountRepository;
	@Autowired private AccountControllerService accountControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(accountRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@GetMapping
	public ModelAndView get() {
		return accountControllerService.getList();
	}

	@GetMapping(path = "/redirect")
	public ModelAndView getRedirect(Long id, HttpServletRequest req) {
		return accountControllerService.getRedirect(id, req);
	}

}

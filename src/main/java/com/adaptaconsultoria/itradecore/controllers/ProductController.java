package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.configs.Converter;
import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.models.Plan;
import com.adaptaconsultoria.itradecore.objects.pojo.Product;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;
import com.adaptaconsultoria.itradecore.repositories.PlanRepository;
import com.adaptaconsultoria.itradecore.repositories.ProductLanguageRepository;
import com.adaptaconsultoria.itradecore.repositories.ProductRepository;
import com.adaptaconsultoria.itradecore.services.ctrls.ProductControllerService;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private CurrencyRepository currencyRepository;

	@Autowired
	private PlanRepository planRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductControllerService productControllerService;

	@Autowired
	private ProductLanguageRepository productLanguageRepository;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Currency.class, "currency", new Converter(currencyRepository));
		binder.registerCustomEditor(Plan.class, "plan", new Converter(planRepository));
	}

	@GetMapping(value = "/list")
	public ModelAndView list() {
		return productControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(productRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(productControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return productControllerService.getPost(id);
	}

	@GetMapping(value = "/getlanguages")
	public ResponseEntity<?> getRules(Long productId) {
		return ResponseEntity.ok(this.productLanguageRepository.findByPartnerId(productId));
	}

	@PostMapping
	@ResponseBody
	public Object post(@RequestBody Product product) {
		return productControllerService.save(product);
	}

}

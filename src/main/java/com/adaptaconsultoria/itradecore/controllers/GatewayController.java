package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.configs.Converter;
import com.adaptaconsultoria.itradecore.models.DocSequence;
import com.adaptaconsultoria.itradecore.models.Gateway;
import com.adaptaconsultoria.itradecore.repositories.DocSequenceRepository;
import com.adaptaconsultoria.itradecore.repositories.GatewayRepository;
import com.adaptaconsultoria.itradecore.services.ctrls.GatewayControllerService;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Controller
@RequestMapping("/gateway")
public class GatewayController {

	@Autowired private GatewayRepository gatewayRepository;
	@Autowired private DocSequenceRepository docSequenceRepository;
	@Autowired private GatewayControllerService gatewayControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(DocSequence.class, "remittanceSequence", new Converter(docSequenceRepository));
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return gatewayControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(gatewayRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(gatewayControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return gatewayControllerService.getPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(Gateway gateway) {
		return ResponseEntity.ok(gatewayControllerService.save(gateway));
	}

}

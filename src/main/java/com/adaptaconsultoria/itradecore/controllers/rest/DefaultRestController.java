package com.adaptaconsultoria.itradecore.controllers.rest;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.objects.rest.Token;
import com.adaptaconsultoria.itradecore.services.TokenService;
import com.adaptaconsultoria.itradecore.utils.JsonUtility;

public class DefaultRestController {

	@Autowired private TokenService tokenService;
	@Autowired private HttpServletRequest request;

	public Boolean validateFields(DefaultIn in, DefaultOut out, String uri) throws Exception {
		return validateFields(in, out, uri, true);
	}

	public Boolean validateFields(DefaultIn in, DefaultOut out, String uri, Boolean transacional) throws Exception {
		Boolean ok = true;
		DefaultError error = new DefaultError();

		String ipAddress = request.getRemoteAddr();

		if (StringUtils.isBlank(in.getToken())) {
			error.setError("token is required!");
			ok = false;
		} else {
			Token token = tokenService.checkToken(in.getToken(), ipAddress, uri, JsonUtility.toJson(in), transacional);
			if (token == null) {
				throw new Exception("Token could not be renewed!");
			}
			out.setToken(token.getToken());
			if (StringUtils.isNotBlank(token.getError())) {
				ok = false;
				error.setError(token.getError());
			}
		}

		if (!ok) {
			out.setError(error);
		}

		out.setHasError(!ok);
		return ok;
	}

}

package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.EventOrder;
import com.adaptaconsultoria.itradecore.objects.in.EventOrderPostIn;
import com.adaptaconsultoria.itradecore.objects.out.EventOrderPostOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.services.EventOrderService;
import com.adaptaconsultoria.itradecore.services.gateways.compralo.CompraloPostback;

@RestController
@RequestMapping("/api/eventorder")
public class EventRestController {

	@Autowired private EventOrderService eventOrderService;

	@PostMapping
	public EventOrderPostOut post(@RequestBody EventOrderPostIn in) {
		EventOrderPostOut out = new EventOrderPostOut();
		try {
			EventOrder order = eventOrderService.createEventOrder(in);
			out.setUrl(order.getUrl());
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

 	@PostMapping(path = "/postback")
	public boolean postPostBack(CompraloPostback postback) {
 		try {
 			if (postback.getStatus().equals("0")) {
 				System.out.println("Order: " + postback.getToken() + " - Pending...");
 				return true;
 			} else if (postback.getStatus().equals("1")) {
 				System.out.println("Order: " + postback.getToken() + " - Paid.");
 				return eventOrderService.pay(postback.getToken());
 			}
		} catch (Exception e) {
		}
 		return false;
	}

}

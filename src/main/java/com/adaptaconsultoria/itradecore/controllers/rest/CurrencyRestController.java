package com.adaptaconsultoria.itradecore.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.objects.in.CurrencyQuoteGetIn;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.CurrencyGetOut;
import com.adaptaconsultoria.itradecore.objects.out.CurrencyQuoteGetOut;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;
import com.adaptaconsultoria.itradecore.services.CurrencyQuoteService;

@RestController
@RequestMapping("/api/currency")
public class CurrencyRestController extends DefaultRestController {

	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private AppSessionRepository appSessionRepository;
	@Autowired private CurrencyQuoteService currencyQuoteService;

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		CurrencyGetOut out = new CurrencyGetOut();
		try {
			if (this.validateFields(in, out, "/api/currency", false)) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				List<Currency> currencies = currencyRepository.findByCompany(appSession.getCompany());
				out.setCurrencies(currencies);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/quote")
	public DefaultOut getQuote(CurrencyQuoteGetIn in) {
		CurrencyQuoteGetOut out = new CurrencyQuoteGetOut();
		try {
			if (this.validateFields(in, out, "/api/currency/quote")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setQuotes(currencyQuoteService.getCurrencyQuotes(in, appSession));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

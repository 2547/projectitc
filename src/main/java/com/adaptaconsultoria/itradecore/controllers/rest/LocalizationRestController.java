package com.adaptaconsultoria.itradecore.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.LocalizationGetOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.objects.pojo.CountryJson;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.LocalizationService;
import com.adaptaconsultoria.itradecore.utils.JsonUtility;

@RestController
@RequestMapping("/api/localization")
public class LocalizationRestController extends DefaultRestController {

	@Autowired private LocalizationService localizationService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	@SuppressWarnings("unchecked")
	public DefaultOut get(DefaultIn in) {
		LocalizationGetOut out = new LocalizationGetOut();
		try {
			if (this.validateFields(in, out, "/api/localization", false)) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				String obj = localizationService.get(appSession);
				out.setCountriesStr(obj);
				try {
					out.setCountries((List<CountryJson>) JsonUtility.fromJson(obj));
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

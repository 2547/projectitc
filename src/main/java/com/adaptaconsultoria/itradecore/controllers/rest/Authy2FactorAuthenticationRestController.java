package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.Authy2FAPostIn;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.AuthyService;

@RestController
@RequestMapping("/api/authy2fa")
public class Authy2FactorAuthenticationRestController extends DefaultRestController {

	@Autowired private AuthyService authyService;
	@Autowired private AppSessionRepository appSessionRepository;

	@PostMapping(path = "/enable")
	public DefaultOut postEnable(@RequestBody DefaultIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validateFields(in, out, "/api/authy2fa/enable")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				authyService.createUser(appSession);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@PostMapping(path = "/disable")
	public DefaultOut postDisable(@RequestBody Authy2FAPostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validateFields(in, out, "/api/authy2fa/disable")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				authyService.removeUser(in.getCode(), appSession);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@PostMapping(path = "/verify")
	public DefaultOut postVerify(@RequestBody Authy2FAPostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validateFields(in, out, "/api/authy2fa/verify")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				authyService.verifyUser(in.getCode(), appSession);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.BinaryGetIn;
import com.adaptaconsultoria.itradecore.objects.in.BinarySidePostIn;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.BinaryGetOut;
import com.adaptaconsultoria.itradecore.objects.out.BinarySideGetOut;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.BinaryService;

@RestController
@RequestMapping("/api/binary")
public class BinaryRestController extends DefaultRestController {

	@Autowired private BinaryService binaryService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(BinaryGetIn in) {
		BinaryGetOut out = new BinaryGetOut();
		try {
			if (this.validateFields(in, out, "/api/binary")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setBinaries(com.adaptaconsultoria.itradecore.objects.rest.Binary.fromModels(binaryService.getTree(in, appSession)));

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/side")
	public DefaultOut getSide(DefaultIn in) {
		BinarySideGetOut out = new BinarySideGetOut();
		try {
			if (this.validateFields(in, out, "/api/binary/side")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setSide(binaryService.getSide(appSession));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@PostMapping(path = "/side")
	public DefaultOut postSide(@RequestBody BinarySidePostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validateFields(in, out, "/api/binary/side")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				binaryService.setSide(in.getSide(), appSession);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.CareerGetOut;
import com.adaptaconsultoria.itradecore.objects.out.CareerStatementGetOut;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.CareerService;

@RestController
@RequestMapping("/api/career")
public class CareerRestController extends DefaultRestController {

	@Autowired private CareerService careerService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		CareerGetOut out = new CareerGetOut();
		try {
			if (this.validateFields(in, out, "/api/career")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
//				out = careerService.getCareer(appSession, out);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/statement")
	public DefaultOut getStatement(DefaultIn in) {
		CareerStatementGetOut out = new CareerStatementGetOut();
		try {
			if (this.validateFields(in, out, "/api/career/statement")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out = careerService.getCareer(appSession, out);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

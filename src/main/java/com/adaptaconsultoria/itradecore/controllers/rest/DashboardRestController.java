package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.DashboardGetOut;
import com.adaptaconsultoria.itradecore.objects.out.DashboardNetworkGetOut;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.DashboardService;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardRestController extends DefaultRestController {

	@Autowired private DashboardService dashboardService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		DashboardGetOut out = new DashboardGetOut();
		try {
			if (this.validateFields(in, out, "/api/dashboard")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setIndicators(dashboardService.getIndicators(appSession));

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/network")
	public DefaultOut getNetwork(DefaultIn in) {
		DashboardNetworkGetOut out = new DashboardNetworkGetOut();
		try {
			if (this.validateFields(in, out, "/api/dashboard")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setNetworks(dashboardService.getNetwork(appSession));

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

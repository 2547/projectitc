package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.in.WithdrawalPostIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.WithdrawalGetOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.WithdrawalService;

@RestController
@RequestMapping("/api/withdrawal")
public class WithdrawalRestController extends DefaultRestController {

	@Autowired private WithdrawalService withdrawalService;
	@Autowired private AppSessionRepository appSessionRepository;

	@PostMapping
	public DefaultOut post(@RequestBody WithdrawalPostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validateFields(in, out, "/api/withdrawal")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				withdrawalService.doWithdrawal(in, appSession);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		WithdrawalGetOut out = new WithdrawalGetOut();
		try {
			if (this.validateFields(in, out, "/api/withdrawal")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out = withdrawalService.checkParameters(appSession, out);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.StatementGetIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.StatementGetOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.StatementService;

@RestController
@RequestMapping("/api/statement")
public class StatementRestController extends DefaultRestController {

	@Autowired private StatementService statementService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(StatementGetIn in) {
		StatementGetOut out = new StatementGetOut();
		try {
			if (this.validateFields(in, out, "/api/statement")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setStatements(statementService.getStatements(in, appSession));

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

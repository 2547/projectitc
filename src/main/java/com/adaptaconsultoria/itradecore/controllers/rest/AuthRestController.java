package com.adaptaconsultoria.itradecore.controllers.rest;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.AuthPostIn;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.AuthGetOut;
import com.adaptaconsultoria.itradecore.objects.out.AuthPostOut;
import com.adaptaconsultoria.itradecore.objects.out.error.AuthPostError;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.objects.rest.Account;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.AccountService;
import com.adaptaconsultoria.itradecore.services.AppSessionService;
import com.adaptaconsultoria.itradecore.services.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthRestController extends DefaultRestController {

	@Autowired private AuthService authService;
	@Autowired private HttpServletRequest request;
	@Autowired private AccountService accountService;
	@Autowired private AppSessionService appSessionService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public AuthGetOut get(DefaultIn in) {
		AuthGetOut out = new AuthGetOut();
		try {
			if (this.validateFields(in, out, "/api/auth", true)) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				appSession.setIpAddress(request.getRemoteAddr());
				appSession = appSessionService.save(appSession);

				out.setAccount(Account.fromModel(accountService.getAccount(appSession)));

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@PostMapping
	public AuthPostOut post(@RequestBody AuthPostIn in) {
		AuthPostOut out = new AuthPostOut();
		try {
			if (this.validateFields(in, out)) {
				AppSession appSession = authService.getAccessToken(in, "/api/auth");
				if (appSession == null) {
					AuthPostError error = new AuthPostError();
					error.setError("Token could not be created!");
					out.setError(error);
				} else {
					out.setAccount(Account.fromModel(accountService.getAccount(appSession)));
					out.setToken(appSession.getToken());
				}
			}
		} catch (Exception e) {
			AuthPostError error = new AuthPostError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	private Boolean validateFields(AuthPostIn in, AuthPostOut out) {
		Boolean ok = true;
		AuthPostError error = new AuthPostError();

		if (StringUtils.isBlank(in.getAppToken())) {
			error.setAppToken("appToken is required!");
			ok = false;
		}

		if (StringUtils.isBlank(in.getAppPassword())) {
			error.setAppPassword("appPassword is required!");
			ok = false;
		}

		if (!ok) {
			out.setError(error);
		}

		out.setHasError(!ok);
		return ok;
	}
}
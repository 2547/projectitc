package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.AccountFindGetIn;
import com.adaptaconsultoria.itradecore.objects.in.AccountGetIn;
import com.adaptaconsultoria.itradecore.objects.in.AccountPostIn;
import com.adaptaconsultoria.itradecore.objects.out.AccountFindGetOut;
import com.adaptaconsultoria.itradecore.objects.out.AccountGetOut;
import com.adaptaconsultoria.itradecore.objects.out.AccountPostOut;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AccountRepository;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.repositories.AutoCompleteRepository;
import com.adaptaconsultoria.itradecore.services.AccountService;
import com.adaptaconsultoria.itradecore.services.AppSessionService;

@RestController
@RequestMapping("/api/account")
public class AccountRestController extends DefaultRestController {

	@Autowired private AccountService accountService;
	@Autowired private AccountRepository accountRepository;
	@Autowired private AppSessionService appSessionService;
	@Autowired private AppSessionRepository appSessionRepository;
	@Autowired private AutoCompleteRepository autoCompleteRepository;

	@PostMapping
	public DefaultOut post(@RequestBody AccountPostIn in) {
		AccountPostOut out = new AccountPostOut();
		try {
			if (this.validateFields(in, out, "/api/account", false)) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Account account = accountService.createAccount(in, appSession);

				out.setDidLogin(in.isDoLogin() && account.getUser() != null);
				if (out.isDidLogin()) {
					appSession.setUser(account.getUser());
//					out.setUser(User.fromModel(appSession.getUser()));
					out.setAccount(com.adaptaconsultoria.itradecore.objects.rest.Account.fromModel(account));
				}

				appSessionService.save(appSession);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(AccountGetIn in) {
		AccountGetOut out = new AccountGetOut();
		try {
			if (this.validateFields(in, out, "/api/account")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Account account = accountService.getAccount(appSession);

				if (account == null) {
					throw new Exception("Account not fount!");
				}
				out.setAccount(com.adaptaconsultoria.itradecore.objects.rest.Account.fromModel(account));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/find")
	public DefaultOut getFind(AccountFindGetIn in) {
		AccountFindGetOut out = new AccountFindGetOut();
		try {
			if (this.validateFields(in, out, "/api/account/find", false)) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());

				try {
					Account account = accountRepository.findTop1ByCompanyAndUserAndIsactiveTrue(appSession.getCompany(), appSession.getUser());
					if (account == null) {
						throw new Exception();
					} else {
						in.setQuery("%"+in.getQuery()+"%");
					}
				} catch (Exception e) {
				}
				out.setAccounts(autoCompleteRepository.findAccountNoByCompany(appSession.getCompany().getId(), in.getQuery()));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

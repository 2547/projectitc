package com.adaptaconsultoria.itradecore.controllers.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.objects.in.RecoveryPasswordPostIn;
import com.adaptaconsultoria.itradecore.objects.in.RecoveryPasswordRedefinePostIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.services.AppSessionService;
import com.adaptaconsultoria.itradecore.services.RecoveryPasswordService;

@RestController
@RequestMapping("/api/recoverypassword")
public class RecoveryPasswordRestController extends DefaultRestController {

	@Autowired private AppSessionService appSessionService;
	@Autowired private AppSessionRepository appSessionRepository;
	@Autowired private RecoveryPasswordService recoveryPasswordService;

	@PostMapping
	public DefaultOut post(@RequestBody RecoveryPasswordPostIn in, HttpServletRequest req) {
		DefaultOut out = new DefaultOut();
		String uri = "/api/recoverypassword";
		try {
			if (this.validateFields(in, out, uri, false)) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());

				recoveryPasswordService.sendEmailRecovery(in, appSession, uri, req);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@PostMapping(path = "/redefine")
	public DefaultOut postRedefine(@RequestBody RecoveryPasswordRedefinePostIn in, HttpServletRequest req) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validateFields(in, out, "/api/recoverypassword/redefine")) {

				AppSession appSession = recoveryPasswordService.redefinePassword(in, appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken()));
				appSessionService.closeAllUserSession(appSession.getUser());

				out.setToken(null);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}

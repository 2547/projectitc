package com.adaptaconsultoria.itradecore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Order;
import com.adaptaconsultoria.itradecore.objects.in.OrderGetIn;
import com.adaptaconsultoria.itradecore.objects.in.OrderPostIn;
import com.adaptaconsultoria.itradecore.objects.in.OrderPostPayIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.OrderGetOut;
import com.adaptaconsultoria.itradecore.objects.out.OrderPostOut;
import com.adaptaconsultoria.itradecore.objects.out.OrderPostPayOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.repositories.OrderRepository;
import com.adaptaconsultoria.itradecore.services.AccountService;
import com.adaptaconsultoria.itradecore.services.OrderService;
import com.adaptaconsultoria.itradecore.services.gateways.compralo.CompraloPostback;

@RestController
@RequestMapping("/api/order")
public class OrderRestController extends DefaultRestController {

	@Autowired private OrderService orderService;
	@Autowired private AccountService accountService;
	@Autowired private OrderRepository orderRepository;
	@Autowired private AppSessionRepository appSessionRepository;

	@PostMapping
	public DefaultOut post(@RequestBody OrderPostIn in) {
		OrderPostOut out = new OrderPostOut();
		try {
			if (this.validateFields(in, out, "/api/order")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Order order = orderService.createOrder(in, appSession);
				out.setOrderCode(order.getId());
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(OrderGetIn in) {
		OrderGetOut out = new OrderGetOut();
		try {
			if (this.validateFields(in, out, "/api/order")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				if (in.getOrderCode() != null) {
					try {
						Order order = orderRepository.findByCompanyAndAccountAndId(appSession.getCompany(), accountService.getAccount(appSession), in.getOrderCode());
						if (order == null) {
							throw new Exception();
						}
						out.setOrder(com.adaptaconsultoria.itradecore.objects.rest.Order.fromModel(order));
					} catch (Exception e) {
						throw new Exception("Order not found!");
					}
				} else {
					out.setOrders(com.adaptaconsultoria.itradecore.objects.rest.Order.fromModels(orderRepository.findByFunctionAccount(accountService.getAccount(appSession).getId())));
				}
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@PostMapping("/pay")
	public DefaultOut pay(@RequestBody OrderPostPayIn in) {
		OrderPostPayOut out = new OrderPostPayOut();
		try {
			if (this.validateFields(in, out, "/api/order/pay")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setOrder(com.adaptaconsultoria.itradecore.objects.rest.Order.fromModel(orderService.pay(in.getOrderCode(), appSession)));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

 	@PostMapping(path = "/postback")
	public boolean postPostBack(CompraloPostback postback) {
 		try {
 			if (postback.getStatus().equals("0")) {
 				System.out.println("Order: " + postback.getToken() + " - Pending...");
 				return true;
 			} else if (postback.getStatus().equals("1")) {
 				System.out.println("Order: " + postback.getToken() + " - Paid.");
 				return orderService.payout(postback.getToken());
 			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
 		return false;
	}

}

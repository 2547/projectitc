package com.adaptaconsultoria.itradecore.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Plan;
import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.PlanGetOut;
import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;
import com.adaptaconsultoria.itradecore.repositories.PlanRepository;

@RestController
@RequestMapping("/api/plan")
public class PlanRestController extends DefaultRestController {

	@Autowired private PlanRepository planRepository;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		PlanGetOut out = new PlanGetOut();
		try {
			if (this.validateFields(in, out, "/api/plan")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				List<Plan> plan = planRepository.findByCompanyAndIsactiveTrue(appSession.getCompany());
				out.setPlan(plan);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	

	

}

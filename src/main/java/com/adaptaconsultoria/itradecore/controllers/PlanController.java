package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.objects.pojo.Plan;
import com.adaptaconsultoria.itradecore.repositories.PlanRepository;
import com.adaptaconsultoria.itradecore.repositories.PlanRuleRepository;
import com.adaptaconsultoria.itradecore.services.ctrls.PlanControllerService;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Controller
@RequestMapping("/plan")
public class PlanController {

	@Autowired
	private PlanRepository planRepository;

	@Autowired
	private PlanControllerService planControllerService;

	@Autowired
	private PlanRuleRepository planRuleRepository;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
	}

	@GetMapping(value = "/list")
	public ModelAndView list() {
		return planControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(planRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(planControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return planControllerService.getPost(id);
	}

	@GetMapping(value = "/getplanRule")
	public ResponseEntity<?> getRules(Long planId) {
		return ResponseEntity.ok(this.planRuleRepository.findByPartnerId(planId));
	}

	@PostMapping
	@ResponseBody
	public Object post(@RequestBody Plan plan) {
		return planControllerService.save(plan);
	}

}

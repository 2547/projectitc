package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import com.adaptaconsultoria.itradecore.repositories.AutoCompleteRepository;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Controller
@RequestMapping("/autocompete")
public class AutoCompleteController {

	@Autowired private AutoCompleteRepository autoCompleteRepository;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
	}

	@GetMapping(value = "/partner")
	public ResponseEntity<?> autocompetePartner(String query) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findPartnerByCompany(SessionUtility.getUserLoggedin().getCompany().getId(), query));
	}

	@GetMapping(value = "/leader")
	public ResponseEntity<?> autocompeteLeader(String query) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findPartnerLeaderByCompany(SessionUtility.getUserLoggedin().getCompany().getId(), query));
	}

	@GetMapping(value = "/country")
	public ResponseEntity<?> autocompeteCountry(String query) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findCountryByCompany(SessionUtility.getUserLoggedin().getCompany().getId(), query));
	}

	@GetMapping(value = "/region")
	public ResponseEntity<?> autocompeteRegion(String query, String country) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findRegionByCompanyAndCountry(SessionUtility.getUserLoggedin().getCompany().getId(), country, query));
	}

	@GetMapping(value = "/city")
	public ResponseEntity<?> autocompeteCity(String query, String region, String country) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findCityByCompanyAndRegionAndCountry(SessionUtility.getUserLoggedin().getCompany().getId(), region, country, query));
	}

	@GetMapping(value = "/accountnoentity")
	public ResponseEntity<?> autocompeteAccountNoEntity(String query) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findAccountNoEntityByCompany(SessionUtility.getUserLoggedin().getCompany().getId(), query));
	}

	@GetMapping(value = "/account")
	public ResponseEntity<?> autocompeteAccount(String query) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findAccountByCompany(SessionUtility.getUserLoggedin().getCompany().getId(), query));
	}

	@GetMapping(value = "/accountpayout")
	public ResponseEntity<?> autocompeteAccountPayout(String query, String account) {
		if (StringUtils.isNotBlank(query)) query = "%"+query+"%";
    	return ResponseEntity.ok(autoCompleteRepository.findAccountPayoutByCompanyAndAccount(SessionUtility.getUserLoggedin().getCompany().getId(), query, account));
	}

}

package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.configs.Converter;
import com.adaptaconsultoria.itradecore.models.Event;
import com.adaptaconsultoria.itradecore.models.Gateway;
import com.adaptaconsultoria.itradecore.repositories.EventOrderRepository;
import com.adaptaconsultoria.itradecore.repositories.EventRepository;
import com.adaptaconsultoria.itradecore.repositories.GatewayRepository;
import com.adaptaconsultoria.itradecore.services.ctrls.EventControllerService;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;


@Controller
@RequestMapping("/event")
public class EventController {

	@Autowired private EventRepository eventRepository;
	@Autowired private GatewayRepository gatewayRepository;
	@Autowired private EventOrderRepository eventorderRepository;
	@Autowired private EventControllerService eventControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		 binder.registerCustomEditor(Gateway.class, "gateway", new Converter(gatewayRepository));
	}
	
	

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return eventControllerService.getList();
	}

	
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(eventRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(eventControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return eventControllerService.getPost(id);
	}
	
	@GetMapping(value = "/geteventordertrue")
	public ResponseEntity<?> getRulesTrue(Long eventId) {
		return ResponseEntity.ok(this.eventorderRepository.findByCompanyAndEventAndIsPaidTrue(SessionUtility.getUserLoggedin().getCompany(), this.eventRepository.findById(eventId).get()));
	}
	
	@GetMapping(value = "/geteventorderfalse")
	public ResponseEntity<?> getRulesFalse(Long eventId) {
		return ResponseEntity.ok(this.eventorderRepository.findByCompanyAndEventAndIsPaidFalse(SessionUtility.getUserLoggedin().getCompany(), this.eventRepository.findById(eventId).get()));
	}

	@PostMapping
	public ResponseEntity<?> post(Event event) {
		return ResponseEntity.ok(eventControllerService.save(event));
	}

}

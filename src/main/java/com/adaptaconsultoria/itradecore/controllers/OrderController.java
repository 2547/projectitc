package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.repositories.OrderRepository;
import com.adaptaconsultoria.itradecore.services.OrderService;
import com.adaptaconsultoria.itradecore.services.ctrls.OrderControllerService;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired private OrderRepository orderRepository;
	@Autowired private OrderControllerService orderControllerService;
	@Autowired private OrderService orderService;

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return orderControllerService.getList();
	}
    
    @ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(orderRepository.findByCompanyAndIsPaidFalse(SessionUtility.getUserLoggedin().getCompany()));
	}
    
	@PostMapping(value="/pay")
	public ResponseEntity<?> pay(Long orderId) {
		return ResponseEntity.ok(this.orderService.payByUser(orderId, SessionUtility.getUserLoggedin().getId()));
	}

}

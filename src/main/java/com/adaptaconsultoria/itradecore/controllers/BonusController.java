package com.adaptaconsultoria.itradecore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.models.Bonus;
import com.adaptaconsultoria.itradecore.repositories.BonusRepository;
import com.adaptaconsultoria.itradecore.services.ctrls.BonusControllerService;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Controller
@RequestMapping("/bonus")
public class BonusController {

	@Autowired private BonusRepository bonusRepository;
	@Autowired private BonusControllerService bonusControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return bonusControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(bonusRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(bonusControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return bonusControllerService.getPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(Bonus bonus) {
		return ResponseEntity.ok(bonusControllerService.save(bonus));
	}

}

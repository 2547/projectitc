package com.adaptaconsultoria.itradecore.utils;

import org.springframework.security.core.context.SecurityContextHolder;

import com.adaptaconsultoria.itradecore.models.User;

public class SessionUtility {

	private SessionUtility() {
	}

	public static User getUserLoggedin() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

}

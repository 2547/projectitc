package com.adaptaconsultoria.itradecore.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Subselect;

import lombok.Data;

@Data
@Entity
@Subselect("")
public class AutoComplete {

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "text")
	private String text;

}

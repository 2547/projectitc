package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Subselect;

import lombok.Data;

@Data
@Entity
@Subselect("")
public class LinearTree implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "p_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "p_level")
	private Integer level;

	@ManyToOne
	@JoinColumn(name = "p_child_account_id")
	private Account childAccount;

}

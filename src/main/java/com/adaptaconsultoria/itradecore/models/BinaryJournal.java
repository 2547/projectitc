package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "binary_journal")
public class BinaryJournal implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "binary_journal_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "binary_network_id")
	private BinaryNetwork binaryNetwork;

	@Column(name = "left_points")
	private Long leftPoints = 0L;

	@Column(name = "right_points")
	private Long rightPoints = 0L;

	@Size(max = 60)
	private String description;

	@ManyToOne
	@JoinColumn(name = "order_line_id")
	private OrderLine orderLine;

	@ManyToOne
	@JoinColumn(name = "binary_payment_id")
	private BinaryPayment binaryPayment;

}

package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "withdrawal")
public class Withdrawal implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "withdrawal_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;

	@NotNull
	@Column(name = "dateplanned")
	private Date dateplanned;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "account_payout_id")
	private AccountPayout accountPayout;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@NotNull
	private BigDecimal amount;

	@NotNull
	private BigDecimal fee = BigDecimal.ZERO;

	@NotNull
	@Column(name = "payoutcost")
	private BigDecimal payoutCost = BigDecimal.ZERO;

	@NotNull
	@Column(name = "netamount")
	private BigDecimal netAmount;

	@NotNull
	private Boolean processed = false;

	@ManyToOne
	@JoinColumn(name = "remittance_id")
	private Remittance remittance;

}

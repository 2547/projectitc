package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.adaptaconsultoria.itradecore.models.enums.TeamSide;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "graduation")
public class Graduation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "graduation_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "career_id")
	private Career career;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;

	@NotNull
	@Column(name = "ispaid")
	private Boolean isPaid = false;

	@NotNull
	private BigDecimal balance;

	@NotNull
	@Column(name = "team")
	@Enumerated(EnumType.STRING)
	private TeamSide team;

	@NotNull
	private Date reached = new Date();

}

package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "event")
public class Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "event_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotEmpty
	@Size(max = 120)
	private String name;

	@Column(columnDefinition = "text")
	private String description;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "sale_until")
	private Date saleUntil;

	@Column(name = "welcome_msg_token")
	private Long welcomeMsgToken;

	@Column(name = "confirmation_msg_token")
	private Long confirmationMsgToken;

	@Size(max = 255)
	private String hash;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "gateway_id")
	private Gateway gateway;

	@Size(max = 255)
	@Column(name = "back_url")
	private String backUrl;
	
	//Lista
//	@JsonIgnore
//	@LazyCollection(LazyCollectionOption.FALSE)
//	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
//	private List<EventOrder> eventOrder = new ArrayList<>();

}

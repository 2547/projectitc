package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "bonus")
public class Bonus implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "bonus_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotEmpty
	@Size(max = 10)
	private String code;

	@NotEmpty
	@Size(max = 80)
	private String name;

	@Column(columnDefinition = "text")
	private String description;

	@NotNull
	@Column(name = "dynamiccompression")
	private Boolean dynamicCompression = false;

	@NotNull
	@Column(name = "isrent")
	private Boolean isRent = false;

}

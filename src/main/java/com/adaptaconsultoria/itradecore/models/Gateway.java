package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.adaptaconsultoria.itradecore.models.enums.ClassName;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "gateway")
public class Gateway implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "gateway_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotEmpty
	@Size(max = 255)
	private String name;

	@Column(columnDefinition = "text")
	private String description;

	@Size(max = 255)
	@Column(name = "apikey")
	private String apiKey;

	@Column(name = "value_length")
	private Long valueLength;
	
	@Size(max = 255)
	@Column(name = "value_mask")
	private String valueMask;

	@ManyToOne
	@JoinColumn(name = "docsequence_id")
	private DocSequence docSequence;

	@Column(name = "classname")
	@Enumerated(EnumType.STRING)
	private ClassName className;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;

	@NotNull
	@Column(name = "payin")
	private Boolean payIn;

	@Column(name = "payin_fee")
	private BigDecimal payInFee;

	@NotNull
	private Boolean payout;

	@Column(name = "payout_fee")
	private BigDecimal payoutFee;

	@Size(max = 255)
	private String icon;

	@NotNull
	private Boolean isdefault = false;

	private Integer lifetime;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

}

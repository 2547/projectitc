package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "app_session")
public class AppSession implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "app_session_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "app_id")
	private App app;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@NotNull
	@Column(name = "ip_address", length = 40)
	private String ipAddress;

	@NotNull
	@Column(length = 256)
	private String token;

	@NotNull
	@Column(length = 256)
	private String tokenOriginal;

	@NotNull
	private Date created = new Date();

	@NotNull
	private Date validto;

	@NotNull
	@Column(length = 60)
	private String servicename;

	@Column(columnDefinition = "text")
	private String serviceargs;

	@NotNull
	private Boolean isactive = false;

    @Override
    public AppSession clone() throws CloneNotSupportedException {
        return (AppSession) super.clone();
    }

}

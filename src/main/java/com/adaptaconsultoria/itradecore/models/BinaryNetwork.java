package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "binary_network")
public class BinaryNetwork implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "binary_network_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "left_id")
	private BinaryNetwork left;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "right_id")
	private BinaryNetwork right;

	@NotNull
	@Column(name = "left_points")
	private Long leftPoints = 0L;

	@NotNull
	@Column(name = "right_points")
	private Long rightPoints = 0L;

	@NotNull
	@Column(name = "left_total_points")
	private Long leftTotalPoints = 0L;

	@NotNull
	@Column(name = "right_total_points")
	private Long rightTotalPoints = 0L;

	@NotNull
	@Column(name = "left_direct")
	private Long leftDirect = 0L;

	@NotNull
	@Column(name = "right_direct")
	private Long rightDirect = 0L;

	@NotNull
	@Column(name = "left_team")
	private Long leftTeam = 0L;

	@NotNull
	@Column(name = "right_team")
	private Long rightTeam = 0L;

	@NotNull
	private Date positioned = new Date();

}

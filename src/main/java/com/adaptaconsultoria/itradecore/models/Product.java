package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Audited
@Table(name = "product")
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "product_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotEmpty
	@Size(max = 120)
	private String name;

	@NotEmpty
	@Size(max = 20)
	private String value;

	@Column(name = "shortdescription", columnDefinition = "text")
	private String shortDescription;

	@Column(columnDefinition = "text")
	private String description;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@NotNull
	private BigDecimal price;

	@NotNull
	@Column(name = "\"binary\"")
	private Integer binary = 0;

	@NotNull
	private Integer career = 0;

	@ManyToOne
	@JoinColumn(name = "plan_id")
	private Plan plan;

	@NotNull
	@Column(name = "isvalidactivity")
	private Boolean isValidActivity = false;

	@NotNull
	@Column(name = "isvalidqualify")
	private Boolean isValidQualify = true;

	@Size(max = 255)
	@Column(name = "bigimg")
	private String bigImg;

	@Size(max = 255)
	@Column(name = "smallimg")
	private String smallImg;

	@JsonIgnore
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProductLanguage> languages = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getBinary() {
		return binary;
	}

	public void setBinary(Integer binary) {
		this.binary = binary;
	}

	public Integer getCareer() {
		return career;
	}

	public void setCareer(Integer career) {
		this.career = career;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Boolean getIsValidActivity() {
		return isValidActivity;
	}

	public void setIsValidActivity(Boolean isValidActivity) {
		this.isValidActivity = isValidActivity;
	}

	public Boolean getIsValidQualify() {
		return isValidQualify;
	}

	public void setIsValidQualify(Boolean isValidQualify) {
		this.isValidQualify = isValidQualify;
	}

	public String getBigImg() {
		return bigImg;
	}

	public void setBigImg(String bigImg) {
		this.bigImg = bigImg;
	}

	public String getSmallImg() {
		return smallImg;
	}

	public void setSmallImg(String smallImg) {
		this.smallImg = smallImg;
	}

	public List<ProductLanguage> getLanguages() {
		return languages;
	}

	public void setLanguages(List<ProductLanguage> languages) {
		this.languages = languages;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

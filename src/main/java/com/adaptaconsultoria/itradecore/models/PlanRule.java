package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.adaptaconsultoria.itradecore.models.enums.TargetDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "plan_rule")
public class PlanRule implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "plan_rule_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "plan_id")
	private Plan plan;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "bonus_id")
	private Bonus bonus;

	@NotNull
	private BigDecimal rate;

	@Column(name = "levelup")
	private String levelUp;

	@NotNull
	@Column(name = "validateactivity")
	private Boolean validateActivity = true;

	@NotNull
	@Column(name = "identifylevel")
	private Boolean identifyLevel = true;

//	@NotEmpty
//	@Size(max = 60)
//	@Column(name = "deadline_type")
//	private String deadlineType;

	@NotEmpty
	@Size(max = 40)
	@Column(name = "deadline_param")
	private String deadlineParam;
	
	@Column(name = "deadline_type")
	@Enumerated(EnumType.STRING)
	private TargetDate targetdate;

	

	

}

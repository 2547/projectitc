package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Subselect;

import lombok.Data;

@Data
@Entity
@Subselect("")
public class DashboardIndicator implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Long id;

	private String description;

	private String currency;

	private BigDecimal amount;

	@Transient
	private BigDecimal amountBTC;

}

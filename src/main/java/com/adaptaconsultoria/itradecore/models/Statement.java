package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Subselect;

import lombok.Data;

@Data
@Entity
@Subselect("")
public class Statement implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Long id;

	private String description;

	private Date datetrx;

	private String currency;

	private BigDecimal amount;

	private String status;

}

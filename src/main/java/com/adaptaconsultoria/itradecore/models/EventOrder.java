package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "event_order")
public class EventOrder implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "event_order_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "event_id")
	private Event event;

	@NotEmpty
	@Size(max = 80)
	private String ticket;

	@NotEmpty
	@Size(max = 255)
	private String email;

	@NotNull
	private Integer qty;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@NotNull
	private BigDecimal amount;
	
	@Column(columnDefinition = "text")
	private String description;

	@Column(name = "ispaid")
	private Boolean isPaid = false;

	@Size(max = 255)
	private String url;

	@Size(max = 255)
	private String hash;

	@Transient
	private String postbackUrl;

}

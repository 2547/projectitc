package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Subselect;

import lombok.Data;

@Data
@Entity
@Subselect("")
public class DashboardNetwork implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Long id;

	private String name;

	private String username;

	private String positioned;

}

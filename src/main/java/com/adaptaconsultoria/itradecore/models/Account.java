package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.adaptaconsultoria.itradecore.models.enums.BinarySide;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "account")
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "account_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@NotEmpty
	@Size(max = 13)
	@Column(name = "accountno")
	private String accountNo;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	@Size(max = 20)
	private String taxid;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "sponsor_account_id")
	private Account sponsorAccount;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "address_id")
	private Address address;

	@NotNull
	@Column(name = "binaryside")
	@Enumerated(EnumType.STRING)
	private BinarySide binarySide = BinarySide.A;

	@NotNull
	@Column(name = "qualifiedbinary")
	private boolean qualifiedBinary = false;

	@NotNull
	@Column(name = "activitypaid")
	private boolean activityPaid = false;

	@NotNull
	@Column(name = "iscompanyaccount")
	private boolean isCompanyAccount = false;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "default_career_id")
	private Career defaultCareer;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "career_id")
	private Career career;

	@Size(max = 255)
	@Column(name = "id2fa")
	private String id2fa;

}

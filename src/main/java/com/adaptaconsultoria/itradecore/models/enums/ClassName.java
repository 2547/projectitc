package com.adaptaconsultoria.itradecore.models.enums;

public enum ClassName {

	CompraloService("Compralo");

	private String description;

	ClassName(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}

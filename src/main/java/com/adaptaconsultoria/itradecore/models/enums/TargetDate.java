package com.adaptaconsultoria.itradecore.models.enums;

public enum TargetDate {

	FIRST_SUNDAY("first sunday"),
	FIRST_MONDAY("first monday"),
	FIRST_TUESDAY("first tuesday"),
	FIRST_WEDNESDAY("first wednesday"),
	FIRST_THURSDAY("first thursday"),
	FIRST_FRIDAY("first friday"),
	FIRST_SATURDAY("first saturday"),
	SECOND_SUNDAY("second sunday"),
	SECOND_MONDAY("second monday"),
	SECOND_TUESDAY("second tuesday"),
	SECOND_WEDNESDAY("second wednesday"),
	SECOND_THURSDAY("second thursday"),
	SECOND_FRIDAY("second friday"),
	SECOND_SATURDAY("second saturday"),
	THIRD_SUNDAY("third sunday"),
	THIRD_MONDAY("third monday"),
	THIRD_TUESDAY("third tuesday"),
	THIRD_WEDNESDAY("third wednesday"),
	THIRD_THURSDAY("third thursday"),
	THIRD_FRIDAY("third friday"),
	THIRD_SATURDAY("third saturday"),
	FOURTH_SUNDAY("fourth sunday"),
	FOURTH_MONDAY("fourth monday"),
	FOURTH_TUESDAY("fourth tuesday"),
	FOURTH_WEDNESDAY("fourth wednesday"),
	FOURTH_THURSDAY("fourth thursday"),
	FOURTH_FRIDAY("fourth friday"),
	FOURTH_SATURDAY("fourth saturday");
	
	
	private String description;

	TargetDate(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}

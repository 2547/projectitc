package com.adaptaconsultoria.itradecore.models.enums;

public enum TeamSide {

	L("Left"),
	R("Right");

	private String description;

	TeamSide(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}

package com.adaptaconsultoria.itradecore.models.enums;

public enum EventType {

	A("Active"),
	B("Blocked"),
	C("Canceled");

	private String description;

	EventType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}

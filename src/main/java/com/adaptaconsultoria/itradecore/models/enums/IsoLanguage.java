package com.adaptaconsultoria.itradecore.models.enums;

public enum IsoLanguage {

	pt_BR("Português"),
	es_PY("Paraguay"),
	en_US("English");

	private String description;

	IsoLanguage(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}

package com.adaptaconsultoria.itradecore.models.enums;

public enum BinarySide {

	A("Auto"),
	L("Left"),
	R("Right");

	private String description;

	BinarySide(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}

package com.adaptaconsultoria.itradecore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.adaptaconsultoria.itradecore.models.enums.IsoLanguage;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "product_language")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductLanguage implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "product_language_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@NotNull
	@Column(name = "iso_language")
	@Enumerated(EnumType.STRING)
	private IsoLanguage isoLanguage;


	@Column(name = "productkey")
	private String productKey;

}

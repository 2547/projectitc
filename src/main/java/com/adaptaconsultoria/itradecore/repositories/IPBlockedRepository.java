package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.IPBlocked;

public interface IPBlockedRepository extends JpaRepository<IPBlocked, Long> {

	IPBlocked findTop1ByIpAddressAndIsactiveTrue(String ipAddress);

}

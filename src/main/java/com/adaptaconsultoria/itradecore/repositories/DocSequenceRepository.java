package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.DocSequence;

public interface DocSequenceRepository extends JpaRepository<DocSequence, Long> {

	List<DocSequence> findByCompany(Company company);

}

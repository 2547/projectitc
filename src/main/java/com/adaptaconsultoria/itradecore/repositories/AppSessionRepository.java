package com.adaptaconsultoria.itradecore.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.User;

public interface AppSessionRepository extends JpaRepository<AppSession, Long> {

	AppSession findTop1ByTokenAndIpAddressAndValidtoGreaterThanAndIsactiveTrue(String token, String ipAddress, Date validTo);

	AppSession findTop1ByTokenAndValidtoGreaterThanAndIsactiveTrue(String token, Date validTo);

	AppSession findTop1ByTokenOriginal(String token);

	AppSession findTop1ByTokenOriginalAndValidtoGreaterThanAndIsactiveTrue(String token, Date validTo);

	AppSession findTop1ByTokenAndIsactiveTrue(String token);

	List<AppSession> findByUserAndIsactiveTrue(User user);

}

package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

	Country findTop1ByCompanyAndIsoCodeIgnoreCaseAndIsactiveTrue(Company company, String isoCode);

	List<Country> findByCompanyAndIsactiveTrue(Company company);

}

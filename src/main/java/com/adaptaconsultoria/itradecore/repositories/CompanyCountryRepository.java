package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.CompanyCountry;
import com.adaptaconsultoria.itradecore.models.Country;

public interface CompanyCountryRepository extends JpaRepository<CompanyCountry, Long> {

	CompanyCountry findTop1ByCompanyAndCountryAndIsactiveTrue(Company company, Country country);

}

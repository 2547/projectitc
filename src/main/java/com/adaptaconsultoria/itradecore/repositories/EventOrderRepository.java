package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Event;
import com.adaptaconsultoria.itradecore.models.EventOrder;

public interface EventOrderRepository extends JpaRepository<EventOrder, Long> {

	@Query("select e from EventOrder e where e.event.id = :id")
	List<EventOrder> findByPartnerId(@Param("id") Long id);

	List<EventOrder> findByCompany(Company company);

	EventOrder findByCompanyAndId(Company company, Long id);

	EventOrder findTop1ByHash(String hash);
	
	List<EventOrder> findByCompanyAndEvent(Company company , Event event);
	
	List <EventOrder> findByCompanyAndEventAndIsPaidTrue(Company company,Event event);
	
	List <EventOrder> findByCompanyAndEventAndIsPaidFalse(Company company, Event event);

}

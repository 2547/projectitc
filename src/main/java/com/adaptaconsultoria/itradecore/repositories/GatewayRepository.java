package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Gateway;

public interface GatewayRepository extends JpaRepository<Gateway, Long> {

	List<Gateway> findByCompany(Company company);

	List<Gateway> findByCompanyAndIsactiveTrue(Company company);

	Gateway findTop1ByCompanyAndId(Company company, Long id);

	Gateway findTop1ByCompanyAndIdAndIsactiveTrue(Company company, Long id);

	Gateway findTop1ByCompanyAndIsdefaultTrue(Company company);

}

package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.City;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Region;

public interface CityRepository extends JpaRepository<City, Long> {

	City findTop1ByCompanyAndCodeIgnoreCaseAndIsactiveTrue(Company company, String code);

	City findTop1ByCompanyAndRegionAndCodeIgnoreCaseAndIsactiveTrue(Company company, Region region,  String code);

	List<City> findByCompanyAndRegionAndIsactiveTrue(Company company, Region region);

}

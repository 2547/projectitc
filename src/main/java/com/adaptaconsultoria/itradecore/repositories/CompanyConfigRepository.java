package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.CompanyConfig;

public interface CompanyConfigRepository extends JpaRepository<CompanyConfig, Long> {

	CompanyConfig findTop1ByCompanyAndIsactiveTrue(Company company);

}

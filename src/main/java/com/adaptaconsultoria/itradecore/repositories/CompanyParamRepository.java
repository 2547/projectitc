package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.CompanyParam;

public interface CompanyParamRepository extends JpaRepository<CompanyParam, Long> {

	CompanyParam findTop1ByCompanyAndNameAndIsactiveTrue(Company company, String name);

}

package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.DashboardNetwork;

public interface DashboardNetworkRepository extends JpaRepository<DashboardNetwork, Long> {

	@Query(value = "SELECT id, \"name\", username, CAST(positioned AS text) FROM account_dashboard_network(:accountId)", nativeQuery = true)
	List<DashboardNetwork> getNetwork(@Param("accountId") Long accountId);

}

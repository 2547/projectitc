package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.PlanRule;


public interface PlanRuleRepository extends JpaRepository<PlanRule, Long> {


	@Query("select p from PlanRule p where p.plan.id = :id")
	List<PlanRule> findByPartnerId(@Param("id") Long id);

	

}

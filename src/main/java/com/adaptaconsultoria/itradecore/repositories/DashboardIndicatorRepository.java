package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.DashboardIndicator;

public interface DashboardIndicatorRepository extends JpaRepository<DashboardIndicator, Long> {

	@Query(value = "SELECT * FROM account_dashboard(:accountId)", nativeQuery = true)
	List<DashboardIndicator> getIndicators(@Param("accountId") Long accountId);

}

package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.App;

public interface AppRepository extends JpaRepository<App, Long> {

	App findTop1ByTokenAndIsactiveTrue(String appToken);

}

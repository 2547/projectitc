package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Country;
import com.adaptaconsultoria.itradecore.models.User;

public interface AccountRepository extends JpaRepository<Account, Long> {

	List<Account> findByCompany(Company company);

	Account findTop1ByCompanyAndId(Company company, Long id);

	Account findTop1ByCompanyAndAccountNo(Company company, String accountNo);

	Account findTop1ByCompanyAndAccountNoAndIsactiveTrue(Company company, String accountNo);

	Account findTop1ByCompanyAndUser(Company company, User user);

	Account findTop1ByCompanyAndUserAndIsactiveTrue(Company company, User user);

	Account findTop1ByCompanyAndCountryAndTaxidIgnoreCase(Company company, Country country, String taxid);

	@Query("select a from Account a where a.company = :company and a.country = :country and a.user.phone = :phone")
	Account findTop1ByCompanyAndCountryAndPhone(@Param("company") Company company, @Param("country") Country country, @Param("phone") String phone);

	@Query("select a from Account a where a.company = :company and (a.accountNo = :query or a.user.login = :query)")
	Account findTop1ByCompanyAndAccountNoOrLogin(@Param("company") Company company, @Param("query") String query);

}

package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.BinaryJournal;
import com.adaptaconsultoria.itradecore.models.BinaryNetwork;
import com.adaptaconsultoria.itradecore.models.Company;

public interface BinaryJournalRepository extends JpaRepository<BinaryJournal, Long> {

	List<BinaryJournal> findByCompanyAndBinaryNetwork(Company company, BinaryNetwork binaryNetwork);

}

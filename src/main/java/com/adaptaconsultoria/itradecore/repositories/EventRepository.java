package com.adaptaconsultoria.itradecore.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Event;

public interface EventRepository extends JpaRepository<Event, Long> {

	@Query(value = "SELECT nextval('event_event_id_seq')", nativeQuery = true)
	Long nextId();

	List<Event> findByCompany(Company company);
		
	Event findTop1ByCompanyAndId(Company company, Long id);

	Event findByCompanyAndId(Company company, Long id);
	
	@Query(value =  " SELECT e.* FROM \"event\" e " + 
				    " JOIN event_order eo " + 
					" ON e.event_id = eo.event_id " + 
					" WHERE eo.ispaid = FALSE " + 
					" AND e.event_id = :eventId AND e.company_id = :companyId ", nativeQuery = true)
	List<Event> findByCompanyAndEventAndIsPaidFalse(Long companyId, Long eventId);
	

	Event findTop1ByHashAndSaleUntilGreaterThanEqualAndIsactiveTrue(String hash, Date saleUntil);

}

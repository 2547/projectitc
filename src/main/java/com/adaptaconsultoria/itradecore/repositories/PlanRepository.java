package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Plan;

public interface PlanRepository extends JpaRepository<Plan, Long> {

	Plan findTop1ByCompanyAndId(Company company, Long id);

	List<Plan> findByCompany(Company company);

	List<Plan> findByCompanyAndIsactiveTrue(Company company);

}

package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.BinaryNetwork;
import com.adaptaconsultoria.itradecore.models.Company;

public interface BinaryNetworkRepository extends JpaRepository<BinaryNetwork, Long> {

	BinaryNetwork findTop1ByCompanyAndAccount(Company company, Account account);

	@Query(value = "SELECT CAST(binary_tree(:accountId, null) AS TEXT)", nativeQuery = true)
	String getTreeAccount(@Param("accountId") Long accountId);

	@Query(value = "SELECT CAST(binary_tree(:accountId, :level) AS TEXT)", nativeQuery = true)
	String getTreeAccountAndLevel(@Param("accountId") Long accountId, @Param("level") Integer level);

	@Query(value = "SELECT binary_ischild(:accountChildId, :accountParentId) AS ischild ", nativeQuery = true)
	boolean getBinaryIsChild(@Param("accountChildId") Long accountChildId, @Param("accountParentId") Long accountParentId);

}

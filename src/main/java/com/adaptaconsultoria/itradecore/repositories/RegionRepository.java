package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Country;
import com.adaptaconsultoria.itradecore.models.Region;

public interface RegionRepository extends JpaRepository<Region, Long> {

	Region findTop1ByCompanyAndCodeIgnoreCaseAndIsactiveTrue(Company company, String code);

	Region findTop1ByCompanyAndCountryAndCodeIgnoreCaseAndIsactiveTrue(Company company, Country country,  String code);

	List<Region> findByCompanyAndCountryAndIsactiveTrue(Company company, Country country);

}

package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.LogAccess;

public interface LogAccessRepository extends JpaRepository<LogAccess, Long> {

}
package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

	@Query(value = "SELECT nextval('order_order_id_seq')", nativeQuery = true)
	Long nextId();

	List<Order> findByCompany(Company company);

	List<Order> findByCompanyAndIsPaidFalse(Company company);

	List<Order> findByCompanyAndIsactiveTrue(Company company);

	Order findTop1ByCompanyAndId(Company company, Long id);

	Order findByCompanyAndId(Company company, Long id);

	Order findByCompanyAndAccountAndId(Company company, Account account, Long id);

	List<Order> findByCompanyAndAccount(Company company, Account account);

	@Query(value = "SELECT o.* FROM \"order\" o WHERE order_id IN (SELECT orderid AS order_id FROM order_list(:accountId))", nativeQuery = true)
	List<Order> findByFunctionAccount(Long accountId);

	@Query(value = "SELECT order_paidbyuser(:orderId, :userId)", nativeQuery = true)
	Boolean payByUser(@Param("orderId") Long orderId, @Param("userId") Long userId);

	Order findTop1ByHash(String hash);

	@Query(value = "SELECT order_paidbybalance(:orderId)", nativeQuery = true)
	boolean payByBalance(@Param("orderId") Long orderId);

	@Query(value = "SELECT order_paidbygateway(:orderId)", nativeQuery = true)
	boolean payByGateway(@Param("orderId") Long orderId);

}

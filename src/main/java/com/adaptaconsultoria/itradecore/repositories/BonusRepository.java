package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Bonus;
import com.adaptaconsultoria.itradecore.models.Company;

public interface BonusRepository extends JpaRepository<Bonus, Long> {

	List<Bonus> findByCompany(Company company);

	Bonus findTop1ByCompanyAndId(Company company, Long id);

}

package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.LinearTree;

public interface LinearTreeRepository extends JpaRepository<LinearTree, Long> {

	@Query(value = "SELECT * FROM linear_tree(:accountId, :level)", nativeQuery = true)
	List<LinearTree> getTreeAccountAndLevel(@Param("accountId") Long accountId, @Param("level") Integer level);

}

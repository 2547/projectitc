package com.adaptaconsultoria.itradecore.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.Statement;

public interface StatementRepository extends JpaRepository<Statement, Long> {

	@Query(value = "SELECT * FROM account_statements(:accountId, null, null)", nativeQuery = true)
	List<Statement> getStatements(@Param("accountId") Long accountId);

	@Query(value = "SELECT * FROM account_statements(:accountId, :startDate, null)", nativeQuery = true)
	List<Statement> getStatementsByStartDate(@Param("accountId") Long accountId, @Param("startDate") Date startDate);

	@Query(value = "SELECT * FROM account_statements(:accountId, :startDate, :endDate)", nativeQuery = true)
	List<Statement> getStatementsByStartDateAndEndDate(@Param("accountId") Long accountId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

}

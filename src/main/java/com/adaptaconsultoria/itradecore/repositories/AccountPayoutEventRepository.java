package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.AccountPayoutEvent;

public interface AccountPayoutEventRepository extends JpaRepository<AccountPayoutEvent, Long> {
}

package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AccountPayout;
import com.adaptaconsultoria.itradecore.models.Company;

public interface AccountPayoutRepository extends JpaRepository<AccountPayout, Long> {

	AccountPayout findTop1ByCompanyAndId(Company company, Long id);

	List<AccountPayout> findByCompanyAndAccount(Company company, Account account);

}

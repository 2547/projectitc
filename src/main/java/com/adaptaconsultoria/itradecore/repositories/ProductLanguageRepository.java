package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.itradecore.models.ProductLanguage;

public interface ProductLanguageRepository extends JpaRepository<ProductLanguage, Long> {

	@Query("select p from ProductLanguage p where p.product.id = :id")
	List<ProductLanguage> findByPartnerId(@Param("id") Long id);

}

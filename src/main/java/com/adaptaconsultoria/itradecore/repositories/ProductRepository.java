package com.adaptaconsultoria.itradecore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	Product findTop1ByCompanyAndId(Company company, Long id);

	List<Product> findByCompany(Company company);

	List<Product> findByCompanyAndIsactiveTrue(Company company);

}

package com.adaptaconsultoria.itradecore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.itradecore.models.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}

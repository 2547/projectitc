package com.adaptaconsultoria.itradecore.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.City;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.CompanyCountry;
import com.adaptaconsultoria.itradecore.models.Country;
import com.adaptaconsultoria.itradecore.models.Region;
import com.adaptaconsultoria.itradecore.objects.pojo.CityJson;
import com.adaptaconsultoria.itradecore.objects.pojo.CountryJson;
import com.adaptaconsultoria.itradecore.objects.pojo.RegionJson;
import com.adaptaconsultoria.itradecore.repositories.CityRepository;
import com.adaptaconsultoria.itradecore.repositories.CompanyCountryRepository;
import com.adaptaconsultoria.itradecore.repositories.CountryRepository;
import com.adaptaconsultoria.itradecore.repositories.RegionRepository;
import com.adaptaconsultoria.itradecore.utils.DateUtility;
import com.adaptaconsultoria.itradecore.utils.JsonUtility;

@Service
public class LocalizationService {

	@Autowired private ServletContext servletContext;
	@Autowired private CityRepository cityRepository;
	@Autowired private RegionRepository regionRepository;
	@Autowired private CountryRepository countryRepository;
	@Autowired private CompanyCountryRepository companyCountryRepository;

	public String get(AppSession appSession) throws Exception {
		String pathname = getPath(appSession.getCompany());

		boolean createFile = false;
		File file = new File(pathname);
		if (file.exists()) {
			BasicFileAttributes attr = Files.readAttributes(Paths.get(pathname), BasicFileAttributes.class);
			Date dtFile = new Date(attr.creationTime().toMillis());
			createFile = !DateUtility.dateToString(dtFile, "yyyy-MM-dd").equals(DateUtility.dateToString(new Date(), "yyyy-MM-dd"));
		} else {
			createFile = true;
		}

		String obj;
		if (createFile) {
			try {
				obj = save(appSession.getCompany());
			} catch (Exception e) {
				obj = "";
			}
		} else {
			obj = readFile(appSession.getCompany());
		}
		return obj;
	}

	public String save(Company company) throws Exception {

		List<CountryJson> countries = new ArrayList<>();

		for (Country country : countryRepository.findByCompanyAndIsactiveTrue(company)) {

			CompanyCountry companyCountry = companyCountryRepository.findTop1ByCompanyAndCountryAndIsactiveTrue(company, country);
			if (companyCountry == null) {
				continue;
			}

			CountryJson c = new CountryJson();
			c.setIsoCode(country.getIsoCode());
			c.setName(country.getName());
			c.setDdi(country.getDdi());
			c.setIsoLanguage(country.getIsoLanguage().toString());
			c.setCode(country.getCode());
			c.setTaxidLabel(companyCountry.getTaxidLabel());
			c.setTaxidMandatory(companyCountry.getTaxidMandatory());

			countries.add(c);

			for (Region region : regionRepository.findByCompanyAndCountryAndIsactiveTrue(company, country)) {

				RegionJson r = new RegionJson();
				r.setCode(region.getCode());
				r.setName(region.getName());

				c.getRegions().add(r);

				for (City city : cityRepository.findByCompanyAndRegionAndIsactiveTrue(company, region)) {

					CityJson ct = new CityJson();
					ct.setCode(city.getCode());
					ct.setName(city.getName());

					r.getCities().add(ct);

				}

			}

		}

		String realPath = getPath(company);

		String obj = JsonUtility.toJson(countries);
		try (FileWriter file = new FileWriter(realPath)) {
			file.write(obj);
			System.out.println("JSON Object: \n" + obj + "\n");
			System.out.println("Successfully Copied JSON Object to File...\n");
			System.out.println(realPath);
		}

		return obj;
	}

	private String getPath(Company company) {
		String realPath = servletContext.getRealPath("");
		realPath += "country" + company.getId() + ".json";
		return realPath;
	}

	private String readFile(Company company) {

		String obj = "";
		try {
			FileReader file = new FileReader(getPath(company));
			BufferedReader arq = new BufferedReader(file);

			String linha = arq.readLine();

			while (linha != null) {
				obj += linha;
				linha = arq.readLine();
			}

			file.close();
		} catch (IOException e) {
			System.err.printf("Fail to read file: %s.\n", e.getMessage());
		}

		return obj;
	}

}

package com.adaptaconsultoria.itradecore.services;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.App;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.objects.in.AuthPostIn;
import com.adaptaconsultoria.itradecore.repositories.AccountRepository;

@Service
public class AuthService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired private AppService appService;
	@Autowired private UserService userService;
	@Autowired private HttpServletRequest request;
	@Autowired private IPBlockedService ipBlockedService;
	@Autowired private AccountRepository accountRepository;
	@Autowired private AppSessionService appSessionService;

	public AppSession getAccessToken(AuthPostIn auth, String serviceName) throws Exception {
		return getAccessToken(auth.getAppToken(), auth.getAppPassword(), auth.getLogin(), auth.getUserPassword(), serviceName);
	}
	public AppSession getAccessToken(String appToken, String appPassword, String login, String userPassword, String serviceName) throws Exception {

		String ipAddress = request.getRemoteAddr();

		App app = appService.checkAppLogin(appToken, appPassword);
		if (app == null) {
			throw new Exception("Invalid App!");
		}

		if (ipBlockedService.isIpAddressBlocked(ipAddress)) {
			throw new Exception("Access blocked!");
		}

		User user = null;
		if (StringUtils.isNotBlank(login)) {
			user = userService.checkLogin(app.getCompany(), login, userPassword);
			if (user == null) {
				throw new Exception("Invalid User!");
			}
			if (!user.getCompany().equals(app.getCompany())) {
				throw new Exception("Invalid User!");
			}
			Account account = accountRepository.findTop1ByCompanyAndUserAndIsactiveTrue(app.getCompany(), user);
			if (account == null) {
				throw new Exception("Invalid User!");
			}
		}

		AppSession appSession = appSessionService.saveToken(app, user, ipAddress, serviceName);
		if (appSession == null) {
			throw new Exception("Token cannot be created!");
		}

		return appSession;
	}

	public AppSession getAccessToken(App app, User user, String ipAddress, String serviceName) throws Exception {
		AppSession appSession = appSessionService.saveToken(app, user, ipAddress, serviceName);
		if (appSession == null) {
			throw new Exception("Token cannot be created!");
		}
		return appSession;
	}

}

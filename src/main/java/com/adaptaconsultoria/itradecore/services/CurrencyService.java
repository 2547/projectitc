package com.adaptaconsultoria.itradecore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;

@Service
public class CurrencyService {

	@Autowired private CurrencyRepository currencyRepository;

	public Currency save(Currency currency) {
		return currencyRepository.save(currency);
	}

	@Transactional
	public void delete(Long currencyId) throws Exception {
		Optional<Currency> currency = currencyRepository.findById(currencyId);
		if (currency.isPresent()) {
			currencyRepository.delete(currency.get());
		} else {
			throw new Exception("Currency not found!");
		}
	}

}

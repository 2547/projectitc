package com.adaptaconsultoria.itradecore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.models.enums.TargetDate;
import com.adaptaconsultoria.itradecore.objects.pojo.Plan;
import com.adaptaconsultoria.itradecore.repositories.BonusRepository;
import com.adaptaconsultoria.itradecore.repositories.PlanRepository;
import com.adaptaconsultoria.itradecore.services.PlanService;
import com.adaptaconsultoria.itradecore.utils.PageUtil;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Service
public class PlanControllerService {

	@Autowired
	private PlanService planService;

	@Autowired
	private PlanRepository planRepository;

	@Autowired
	private BonusRepository bonusRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("plan/list");
		pageUtil.setURI("/plan");
		pageUtil.setPageTitle("Plan");
		pageUtil.setTitle("Plan");
		pageUtil.setInnerTitle("Plan");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("plan-list-table");
		pageUtil.setJs("plan-list.js");
		pageUtil.setAttr("menuId", "plan-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("plan/form");
		pageUtil.setURI("/plan");
		pageUtil.setPageTitle("Plan");
		pageUtil.setTitle("Plan");
		pageUtil.setInnerTitle("Plan");
		pageUtil.setFormId("plan-form");
		pageUtil.setTableId("plan-rule-list-table");
		pageUtil.setJs("plan.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "planlist");
		pageUtil.setAttr("menuId", "plan-menu");

		com.adaptaconsultoria.itradecore.models.Plan plan = new com.adaptaconsultoria.itradecore.models.Plan();

		Plan obj = new Plan();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				plan = planRepository.findTop1ByCompanyAndId(company, id);
//				Optional.ofNullable(plan.getName()).ifPresent(p ->{
//					obj.setDescription(p);
//				});
				
				obj.setId(plan.getId());
				obj.setName(plan.getName());
				obj.setIsactive(plan.getIsactive());
				obj.setDescription(plan.getDescription());
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("plan");
		pageUtil.setAttr("targetdates", TargetDate.values());
		pageUtil.setAttr("bonuss", bonusRepository.findByCompany(company));
//		pageUtil.setAttr("plans",planRepository.findByCompany(company));

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Plan plan) {
		HashMap<Object, Object> map = new HashMap<>();
		com.adaptaconsultoria.itradecore.models.Plan planModel = new com.adaptaconsultoria.itradecore.models.Plan();

		try {
			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = plan.getId() != null;
			planModel.setName(plan.getName());
			planModel.setDescription(plan.getDescription());

			if (isEditing) {
				planModel = planRepository.findById(plan.getId()).get();
				planModel.getPlanRule().clear();
				planModel.setUpdatedBy(user);
				planModel.setUpdated(new Date());
			} else {
				planModel.setCompany(user.getCompany());
				planModel.setCreatedBy(user);
			}

			if (!plan.getPlanRule().isEmpty()) {

				for (com.adaptaconsultoria.itradecore.models.PlanRule planrule : plan.getPlanRule()) {
					planrule.setCompany(planModel.getCompany());
					planrule.setCreatedBy(user);
					planrule.setPlan(planModel);

				}

				planModel.setPlanRule(plan.getPlanRule());
			}

			planModel = planService.save(planModel);

			map.put("sucesso", true);
			map.put("obj", planModel);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", planModel);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			planService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

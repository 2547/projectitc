package com.adaptaconsultoria.itradecore.services.ctrls;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Event;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.repositories.EventRepository;
import com.adaptaconsultoria.itradecore.repositories.GatewayRepository;
import com.adaptaconsultoria.itradecore.services.EventService;
import com.adaptaconsultoria.itradecore.utils.PageUtil;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Service
public class EventControllerService {

	@Autowired private EventService eventService;
	@Autowired private EventRepository eventRepository;
	@Autowired private GatewayRepository gatewayRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("event/list");
		pageUtil.setURI("/event");
		pageUtil.setPageTitle("Event");
		pageUtil.setTitle("Event");
		pageUtil.setInnerTitle("Event");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("event-list-table");
		pageUtil.setJs("event-list.js");
		pageUtil.setAttr("menuId", "event-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("event/form");
		pageUtil.setURI("/event");
		pageUtil.setPageTitle("Event");
		pageUtil.setTitle("Event");
		pageUtil.setInnerTitle("Event");
		pageUtil.setFormId("event-form");
		pageUtil.setJs("event.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "eventlist");
		pageUtil.setAttr("menuId", "event-menu");

		Event obj = new Event();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = eventRepository.findByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		try {
			pageUtil.setAttr("gateways", gatewayRepository.findByCompanyAndIsactiveTrue(company));
		} catch (Exception e) {
		}

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("event");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Event event) {
		HashMap<Object, Object> map = new HashMap<>();
		try {
			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = event.getId() != null;
		
			if (isEditing) {
				
		
			} else {
				
			}
			
			event.setCompany(user.getCompany());
			event.setCreatedBy(user);
			
			
//			if (ps.getGateway() != null && ps.getGateway().getId() != null) {
//				try {
//					Optional<Gateway> opGateway = this.gatewayRepository.findById(ps.getGateway().getId());
//					if (opGateway.isPresent())
//					ps.setGateway(this.gatewayRepository.findById(ps.getGateway().getId()).get());
//				} catch (Exception e) {
//					throw new Exception("Gateway not found!");
//				}
//			}
			
			event.setIsactive(event.getIsactive());
			event.setName(event.getName());
			event.setDescription(event.getDescription());
			event.setSaleUntil(event.getSaleUntil());
			event.setWelcomeMsgToken(event.getWelcomeMsgToken());
			event.setConfirmationMsgToken(event.getConfirmationMsgToken());
			event.setHash(event.getHash());
			event.setBackUrl(event.getBackUrl());
			
			

			event = eventService.save(event);
			map.put("sucesso", true);
			map.put("obj", event);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", event);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			eventService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

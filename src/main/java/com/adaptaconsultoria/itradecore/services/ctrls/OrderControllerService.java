package com.adaptaconsultoria.itradecore.services.ctrls;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.utils.PageUtil;

@Service
public class OrderControllerService {

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("order/list");
		pageUtil.setURI("/order");
		pageUtil.setPageTitle("Order");
		pageUtil.setTitle("Order");
		pageUtil.setInnerTitle("Order");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("order-list-table");
		pageUtil.setJs("order-list.js");
		pageUtil.setAttr("menuId", "order-menu");
		return pageUtil.getModel();
	}

//	public ModelAndView getPost(Long id) {
//		PageUtil pageUtil = new PageUtil("order/form");
//		pageUtil.setURI("/order");
//		pageUtil.setPageTitle("Order");
//		pageUtil.setTitle("Order");
//		pageUtil.setInnerTitle("Order");
//		pageUtil.setFormId("order-form");
//		pageUtil.setJs("order.js");
//		pageUtil.setAttr("commandName", "obj");
//		pageUtil.setAttr("mi", "orderlist");
//		pageUtil.setAttr("menuId", "order-menu");
//
//		Order obj = new Order();
//		Company company = SessionUtility.getUserLoggedin().getCompany();
//
//		if (id != null) {
//			pageUtil.setSubTitle("Edit");
//			try {
//				obj = bonusRepository.findTop1ByCompanyAndId(company, id);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		} else {
//			pageUtil.setSubTitle("New");
//		}
//
//		pageUtil.setAttr("obj", obj);
//		pageUtil.setCommandName("bonus");
//
//		return pageUtil.getModel();
//	}

//	public HashMap<Object, Object> save(Bonus bonus) {
//		HashMap<Object, Object> map = new HashMap<>();
//		try {
//
//			User user = SessionUtility.getUserLoggedin();
//			Boolean isEditing = bonus.getId() != null;
//
//			Bonus ps = new Bonus();
//			if (isEditing) {
//				ps = bonusRepository.findById(bonus.getId()).get();
//				ps.setUpdatedBy(user);
//				ps.setUpdated(new Date());
//			} else {
//				ps.setCompany(user.getCompany());
//				ps.setCreatedBy(user);
//			}
//			ps.setIsactive(bonus.getIsactive());
//			ps.setCode(bonus.getCode());
//			ps.setName(bonus.getName());
//			ps.setDescription(bonus.getDescription());
//			ps.setDynamicCompression(bonus.getDynamicCompression());
//
//			ps = bonusService.save(ps);
//			map.put("sucesso", true);
//			map.put("obj", ps);
//			map.put("message", "Success!");
//			map.put("isEditing", isEditing);
//		} catch (Exception e) {
//			map.put("sucesso", false);
//			map.put("obj", bonus);
//			map.put("message", e.getLocalizedMessage());
//		}
//		return map;
//	}
//
//	public boolean delete(Long id) {
//		try {
//			bonusService.delete(id);
//		} catch (Exception e) {
//			return false;
//		}
//		return true;
//	}

}

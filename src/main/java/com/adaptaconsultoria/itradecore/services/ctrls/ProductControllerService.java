package com.adaptaconsultoria.itradecore.services.ctrls;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.models.Plan;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.objects.pojo.Product;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;
import com.adaptaconsultoria.itradecore.repositories.PlanRepository;
import com.adaptaconsultoria.itradecore.repositories.ProductRepository;
import com.adaptaconsultoria.itradecore.services.ProductService;
import com.adaptaconsultoria.itradecore.utils.PageUtil;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Service
public class ProductControllerService {

	@Autowired private ProductService productService;
	@Autowired private ProductRepository productRepository;
	
	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private PlanRepository planRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("product/list");
		pageUtil.setURI("/product");
		pageUtil.setPageTitle("Product");
		pageUtil.setTitle("Product");
		pageUtil.setInnerTitle("Product");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("product-list-table");
		pageUtil.setJs("product-list.js");
		pageUtil.setAttr("menuId", "product-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("product/form");
		pageUtil.setURI("/product");
		pageUtil.setPageTitle("Product");
		pageUtil.setTitle("Product");
		pageUtil.setInnerTitle("Product");
		pageUtil.setFormId("product-form");
		pageUtil.setTableId("product-language-list-table");
		pageUtil.setJs("product.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "productlist");
		pageUtil.setAttr("menuId", "product-menu");

		com.adaptaconsultoria.itradecore.models.Product product = new com.adaptaconsultoria.itradecore.models.Product();
		
		Product obj = new Product();
		Company company = SessionUtility.getUserLoggedin().getCompany();
		
		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				product = productRepository.findTop1ByCompanyAndId(company, id);
				
				Optional.ofNullable(product.getPlan()).ifPresent(p -> {
					obj.setPlan(p.getId());
				});
				
				Optional.ofNullable(product.getDescription()).ifPresent(p ->{
					obj.setDescription(p);
				});
				Optional.ofNullable(product.getShortDescription()).ifPresent(p ->{
					obj.setShortDescription(p);
				});
				Optional.ofNullable(product.getBigImg()).ifPresent(p ->{
					obj.setBigImg(p);
				});
				Optional.ofNullable(product.getSmallImg()).ifPresent(p ->{
					obj.setSmallImg(p);
				});
				obj.setId(product.getId());
				obj.setBinary(product.getBinary());
				obj.setCareer(product.getCareer());
				obj.setIsactive(product.getIsactive());
				obj.setIsValidActivity(product.getIsValidActivity());
				obj.setIsValidQualify(product.getIsValidQualify());
				obj.setName(product.getName());
				obj.setPrice(product.getPrice());
				obj.setValue(product.getValue());
				obj.setCurrency(product.getCurrency().getId());
				
		
				
								
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("product");
		pageUtil.setAttr("currencies",currencyRepository.findByCompany(company));
		pageUtil.setAttr("plans",planRepository.findByCompany(company));

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Product product) {
		HashMap<Object, Object> map = new HashMap<>();
		com.adaptaconsultoria.itradecore.models.Product productModel = new com.adaptaconsultoria.itradecore.models.Product();
		
		try {
			productModel.setCareer(product.getCareer());
			
			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = product.getId() != null;

			if (isEditing) {
				productModel = productRepository.findById(product.getId()).get();
				productModel.getLanguages().clear();
				productModel.setUpdatedBy(user);
				productModel.setUpdated(new Date());
			} else {
				productModel.setCompany(user.getCompany());
				productModel.setCreatedBy(user);
			}
			
			try {
				Optional<Plan> opPlan = this.planRepository.findById(product.getPlan());
				if (opPlan.isPresent())
					productModel.setPlan(opPlan.get());
			} catch (Exception e) {

			}
			
			Optional<Currency> opCurrency = this.currencyRepository.findById(product.getCurrency());
			if (opCurrency.isPresent())
				productModel.setCurrency(opCurrency.get());

			productModel.setIsactive(product.isIsactive());
			productModel.setBinary(product.getBinary());
			productModel.setCareer(product.getCareer());	
			productModel.setDescription(product.getDescription());
			productModel.setIsValidActivity(product.getIsValidActivity());
			productModel.setIsValidQualify(product.getIsValidQualify());
			productModel.setName(product.getName());		
			productModel.setPrice(product.getPrice());
			productModel.setShortDescription(product.getShortDescription());
			productModel.setValue(product.getValue());
			productModel.setIsactive(product.isIsactive());
			
			if (!product.getLanguages().isEmpty()) {
				for (com.adaptaconsultoria.itradecore.models.ProductLanguage x: product.getLanguages()) {
					x.setCompany(productModel.getCompany());
					x.setCreatedBy(user);
					x.setProduct(productModel);
				}
				productModel.setLanguages(product.getLanguages());
			}

			productModel = productService.save(productModel);
			
			map.put("sucesso", true);
			map.put("obj", productModel);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", productModel);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			productService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	

}

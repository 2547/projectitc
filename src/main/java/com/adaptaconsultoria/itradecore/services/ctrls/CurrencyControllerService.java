package com.adaptaconsultoria.itradecore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;
import com.adaptaconsultoria.itradecore.services.CurrencyService;
import com.adaptaconsultoria.itradecore.utils.PageUtil;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Service
public class CurrencyControllerService {

	@Autowired private CurrencyService currencyService;
	@Autowired private CurrencyRepository currencyRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("currency/list");
		pageUtil.setURI("/currency");
		pageUtil.setPageTitle("Currency");
		pageUtil.setTitle("Currency");
		pageUtil.setInnerTitle("Currency");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("currency-list-table");
		pageUtil.setJs("currency-list.js");
		pageUtil.setAttr("menuId", "currency-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("currency/form");
		pageUtil.setURI("/currency");
		pageUtil.setPageTitle("Currency");
		pageUtil.setTitle("Currency");
		pageUtil.setInnerTitle("Currency");
		pageUtil.setFormId("currency-form");
		pageUtil.setJs("currency.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "currencylist");
		pageUtil.setAttr("menuId", "currency-menu");

		Currency obj = new Currency();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = currencyRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("currency");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Currency currency) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = currency.getId() != null;

			Currency ps = new Currency();
			if (isEditing) {
				ps = currencyRepository.findById(currency.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(currency.getIsactive());
			ps.setCode(currency.getCode());
			ps.setName(currency.getName());
			ps.setSign(currency.getSign());
			ps.setPrecision(currency.getPrecision());
			ps.setIcon(currency.getIcon());

			ps = currencyService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", currency);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			currencyService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

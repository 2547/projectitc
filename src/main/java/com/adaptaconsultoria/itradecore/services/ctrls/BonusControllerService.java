package com.adaptaconsultoria.itradecore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.models.Bonus;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.repositories.BonusRepository;
import com.adaptaconsultoria.itradecore.services.BonusService;
import com.adaptaconsultoria.itradecore.utils.PageUtil;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Service
public class BonusControllerService {

	@Autowired private BonusService bonusService;
	@Autowired private BonusRepository bonusRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("bonus/list");
		pageUtil.setURI("/bonus");
		pageUtil.setPageTitle("Bonus");
		pageUtil.setTitle("Bonus");
		pageUtil.setInnerTitle("Bonus");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("bonus-list-table");
		pageUtil.setJs("bonus-list.js");
		pageUtil.setAttr("menuId", "bonus-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("bonus/form");
		pageUtil.setURI("/bonus");
		pageUtil.setPageTitle("Bonus");
		pageUtil.setTitle("Bonus");
		pageUtil.setInnerTitle("Bonus");
		pageUtil.setFormId("bonus-form");
		pageUtil.setJs("bonus.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "bonuslist");
		pageUtil.setAttr("menuId", "bonus-menu");

		Bonus obj = new Bonus();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = bonusRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("bonus");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Bonus bonus) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = bonus.getId() != null;

			Bonus ps = new Bonus();
			if (isEditing) {
				ps = bonusRepository.findById(bonus.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(bonus.getIsactive());
			ps.setCode(bonus.getCode());
			ps.setIsRent(bonus.getIsRent());
			ps.setName(bonus.getName());
			ps.setDescription(bonus.getDescription());
			ps.setDynamicCompression(bonus.getDynamicCompression());

			ps = bonusService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", bonus);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			bonusService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

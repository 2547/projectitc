package com.adaptaconsultoria.itradecore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Gateway;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.repositories.DocSequenceRepository;
import com.adaptaconsultoria.itradecore.repositories.GatewayRepository;
import com.adaptaconsultoria.itradecore.services.GatewayService;
import com.adaptaconsultoria.itradecore.utils.PageUtil;
import com.adaptaconsultoria.itradecore.utils.SessionUtility;

@Service
public class GatewayControllerService {

	@Autowired private GatewayService gatewayService;
	@Autowired private GatewayRepository gatewayRepository;
	@Autowired private DocSequenceRepository docSequenceRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("gateway/list");
		pageUtil.setURI("/gateway");
		pageUtil.setPageTitle("Gateway");
		pageUtil.setTitle("Gateway");
		pageUtil.setInnerTitle("Gateway");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("gateway-list-table");
		pageUtil.setJs("gateway-list.js");
		pageUtil.setAttr("menuId", "gateway-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("gateway/form");
		pageUtil.setURI("/gateway");
		pageUtil.setPageTitle("Gateway");
		pageUtil.setTitle("Gateway");
		pageUtil.setInnerTitle("Gateway");
		pageUtil.setFormId("gateway-form");
		pageUtil.setJs("gateway.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "gatewaylist");
		pageUtil.setAttr("menuId", "gateway-menu");

		Gateway obj = new Gateway();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = gatewayRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("docSequences", docSequenceRepository.findByCompany(company));

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("gateway");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Gateway gateway) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = gateway.getId() != null;

			Gateway ps = new Gateway();
			if (isEditing) {
				ps = gatewayRepository.findById(gateway.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(gateway.getIsactive());
			ps.setName(gateway.getName());
			ps.setDescription(gateway.getDescription());
			ps.setApiKey(gateway.getApiKey());
			ps.setValueLength(gateway.getValueLength());
			ps.setDocSequence(gateway.getDocSequence());
			ps.setClassName(gateway.getClassName());
			ps.setAccount(gateway.getAccount());
			ps.setPayIn(gateway.getPayIn());
			ps.setPayInFee(gateway.getPayInFee());
			ps.setPayout(gateway.getPayout());
			ps.setPayoutFee(gateway.getPayoutFee());

			ps = gatewayService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", gateway);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			gatewayService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

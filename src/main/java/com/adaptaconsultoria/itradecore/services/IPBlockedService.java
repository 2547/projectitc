package com.adaptaconsultoria.itradecore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.IPBlocked;
import com.adaptaconsultoria.itradecore.repositories.IPBlockedRepository;

@Service
public class IPBlockedService {

	@Autowired private IPBlockedRepository ipBlockedRepository;

	public Boolean isIpAddressBlocked(String ipAddress) {
		IPBlocked ipBlocked = ipBlockedRepository.findTop1ByIpAddressAndIsactiveTrue(ipAddress);
		if (ipBlocked != null) {
			return true;
		}
		return false;
	}

}
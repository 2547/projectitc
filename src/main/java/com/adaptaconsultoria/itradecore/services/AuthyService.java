package com.adaptaconsultoria.itradecore.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.CompanyConfig;
import com.adaptaconsultoria.itradecore.objects.pojo.AuthyCreateUser;
import com.adaptaconsultoria.itradecore.objects.pojo.AuthyVerifyUser;
import com.adaptaconsultoria.itradecore.repositories.CompanyConfigRepository;
import com.adaptaconsultoria.itradecore.services.util.HttpRequest;
import com.google.gson.Gson;

@Service
public class AuthyService {

	private static String AUTHY_URL  = "https://api.authy.com/protected/json";
	private static String USER_URL   = "/users/new";
	private static String VERIFY_URL = "/verify/{cod}/{user}";

	@Autowired private AccountService accountService;
	@Autowired private CompanyConfigRepository companyConfigRepository;

	public void createUser(AppSession appSession) throws Exception {

		CompanyConfig config = companyConfigRepository.findTop1ByCompanyAndIsactiveTrue(appSession.getCompany());
		if (config == null) {
			throw new Exception("Company Config not found!");
		}
		if (StringUtils.isBlank(config.getToken2fa())) {
			throw new Exception("Token not found in configuration!");
		}

		String token2fa = config.getToken2fa();

		Account account = accountService.getAccount(appSession);

		if (StringUtils.isBlank(account.getUser().getEmail())) {
			throw new Exception("E-mail not found!");
		}
		String userEmail = account.getUser().getEmail();

		if (StringUtils.isBlank(account.getUser().getPhone())) {
			throw new Exception("Phone not found!");
		}
		String userPhone = account.getUser().getPhone();

		if (account.getCountry().getCode() == null) {
			throw new Exception("Country Code not found!");
		}
		Integer userCountryCode = account.getCountry().getCode();

		List<NameValuePair> nameValuePairs = new ArrayList<>();
		nameValuePairs.add(new BasicNameValuePair("user[email]", userEmail));
		nameValuePairs.add(new BasicNameValuePair("user[cellphone]", userPhone));
		nameValuePairs.add(new BasicNameValuePair("user[country_code]", userCountryCode.toString()));

		AuthyCreateUser user = new Gson().fromJson(HttpRequest.post(AUTHY_URL + USER_URL, nameValuePairs, "X-Authy-API-Key: " + token2fa), AuthyCreateUser.class);

		if (!user.getSuccess()) {
			throw new Exception(user.getMessage());
		}

		account.setId2fa(user.getUser().getId().toString());
		account = accountService.save(account);

	}

	public void removeUser(Long code2fa, AppSession appSession) throws Exception {

		if (code2fa == null) {
			throw new Exception("Check your Code 2FA, 2-Factor Authentication could not be verified!");
		}

		verifyUser(code2fa, appSession);

		Account account = accountService.getAccount(appSession);
		account.setId2fa(null);
		account = accountService.save(account);

	}

	public void verifyUser(Long code, AppSession appSession) throws Exception {

		Account account = accountService.getAccount(appSession);
		if (StringUtils.isBlank(account.getId2fa())) {
			throw new Exception("2FA is disable!");
		}

		if (code == null) {
			throw new Exception("Code is required!");
		}

		CompanyConfig config = companyConfigRepository.findTop1ByCompanyAndIsactiveTrue(appSession.getCompany());
		if (config == null) {
			throw new Exception("Company Config not found!");
		}
		if (StringUtils.isBlank(config.getToken2fa())) {
			throw new Exception("Token not found in configuration!");
		}
		String token2fa = config.getToken2fa();

		String verifyUrl = VERIFY_URL.replace("{cod}", code.toString()).replace("{user}", account.getId2fa());
		AuthyVerifyUser verify = new Gson().fromJson(HttpRequest.get(AUTHY_URL + verifyUrl, "X-Authy-API-Key: " + token2fa), AuthyVerifyUser.class);

		if (!verify.getSuccess()) {
			throw new Exception(verify.getMessage());
		}

	}

}

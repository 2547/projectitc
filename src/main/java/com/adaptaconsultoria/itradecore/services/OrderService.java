package com.adaptaconsultoria.itradecore.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.models.Order;
import com.adaptaconsultoria.itradecore.models.OrderLine;
import com.adaptaconsultoria.itradecore.models.ProductLanguage;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.models.enums.IsoLanguage;
import com.adaptaconsultoria.itradecore.objects.in.OrderPostIn;
import com.adaptaconsultoria.itradecore.repositories.CompanyRepository;
import com.adaptaconsultoria.itradecore.repositories.GatewayRepository;
import com.adaptaconsultoria.itradecore.repositories.OrderRepository;
import com.adaptaconsultoria.itradecore.repositories.ProductRepository;
import com.adaptaconsultoria.itradecore.services.gateways.GatewayReturn;
import com.adaptaconsultoria.itradecore.utils.DateUtility;

@Service
public class OrderService {

	@Autowired private HttpServletRequest request;

	@Autowired private UserService userService;
	@Autowired private AccountService accountService;
	@Autowired private GatewayService gatewayService;
	@Autowired private EmailAlcService emailAlcService;
	@Autowired private OrderRepository orderRepository;
	@Autowired private CompanyRepository companyRepository;
	@Autowired private ProductRepository productRepository;
	@Autowired private GatewayRepository gatewayRepository;

	@Transactional
	public Order save(Order order) {
		if (order.getId() == null) {
			order.setId(orderRepository.nextId());
		}
		return orderRepository.save(order);
	}

	public Order pay(Long orderId, AppSession appSession) throws Exception {
		if (orderId == null) {
			throw new Exception("Order is null!");
		}
		Order order = orderRepository.findByCompanyAndId(appSession.getCompany(), orderId);
		if (order == null) {
			throw new Exception("Order is null!");
		}
		if (order.getIsPaid()) {
			return order;
		}
		if (StringUtils.isNotBlank(order.getUrl())) {
			return order;
		}
		if (order.getGateway() == null) {
			boolean paid = orderRepository.payByBalance(order.getId());
			if (paid) {
				order = orderRepository.findById(order.getId()).get();
				try {
					emailAlcService.sendEmailPaidOrder(order);
				} catch (Exception e) {
					System.out.println("\n\nPaid Order e-mail could not be sent!\n");
				}
				return order;
			} else {
				throw new Exception("Balance is not avaliable!");
			}
		} else {

			String urlReq = request.getRequestURL().toString().split(request.getContextPath()+"/api")[0];
			String postbackUrl = urlReq + request.getContextPath() + "/api/eventorder/postback";
			System.out.println("\n\nPOSTBACK_URL: " + postbackUrl + "\n");
			order.setPostbackUrl(postbackUrl);

			GatewayReturn ret = gatewayService.payOrder(order);
			order.setUrl(ret.getUrl());
			order.setHash(ret.getHash());

			if (order.getGateway().getLifetime() != null) {
				Date deadline = new Date();
				order.setDeadline(DateUtility.addMinute(deadline, order.getGateway().getLifetime()));
			}
		}
		return orderRepository.save(order);
	}

	public boolean payout(String token) throws Exception {
		if (StringUtils.isBlank(token)) {
			return false;
		}
		Order order = orderRepository.findTop1ByHash(token);
		boolean paid = payGateway(order);
		if (!paid) {
			throw new Exception("Order could not be paid!");
		}
		try {
			emailAlcService.sendEmailPaidOrder(order);
		} catch (Exception e) {
			System.out.println("\n\nPaid Order e-mail could not be sent!\n");
		}
		return true;
	}

	public boolean payGateway(Order order) {
		if (order == null) {
			return false;
		}
		if (order.getIsPaid()) {
			return true;
		}
		return orderRepository.payByGateway(order.getId());
	}

	public Order createOrder(OrderPostIn in, AppSession appSession) throws Exception {

		User userCreating = userService.getUserCreating(appSession);

		Order order = validateCreate(in, appSession);

		if (order.getId() == null) {
			order.setCreatedBy(userCreating);
			order.setCreated(new Date());
		} else {
			order.setUpdatedBy(userCreating);
			order.setUpdated(new Date());
		}

		if (order.getTotalLines().compareTo(BigDecimal.ZERO) == 0) {
			order.setIsPaid(true);
		}

		order = save(order);

		if (order.getIsPaid()) {
			try {
				emailAlcService.sendEmailReceivedOrder(order);
			} catch (Exception e) {
				System.out.println("\n\nPaid Order e-mail could not be sent!\n");
			}
		}

		return order;

	}
	
	public Object payByUser(Long orderId, Long userId) {
		try {
			return this.orderRepository.payByUser(orderId, userId);
		} catch (Exception e) {
			return e.getLocalizedMessage();
		}
	}

	private Order validateCreate(OrderPostIn in, AppSession appSession) throws Exception {

		Order order = new Order();
		order.setCompany(appSession.getCompany());
		order.setAccount(accountService.getAccount(appSession));

		if (in.getCode() != null) {
			throw new Exception("Permission denied!");
		}

		if (order.getId() != null && order.getIsPaid()) {
			throw new Exception("Order is paid!");
		}

		order.setDocumentNo(in.getDocumentNo());

		BigDecimal gatewayFee = null;
		if (in.getGatewayCode() != null) {
			try {
				order.setGateway(gatewayRepository.findTop1ByCompanyAndIdAndIsactiveTrue(appSession.getCompany(), in.getGatewayCode()));
				gatewayFee = order.getGateway().getPayInFee();
			} catch (Exception e) {
				throw new Exception("Gateway not found!");
			}
		} else {
			try {
				Company company = companyRepository.findById(appSession.getCompany().getId()).get();
				if (company == null) {
					throw new Exception();
				}
				gatewayFee = company.getPayInFee();
			} catch (Exception e) {
				throw new Exception("Company is not found!");
			}
		}
		if (gatewayFee == null) {
			gatewayFee = BigDecimal.ZERO;
		}
		order.setFee(gatewayFee);

		order = validateLines(in, order, appSession);
		order.setCurrency(order.getLines().get(0).getProduct().getCurrency());

		return order;
	}

	private Order validateLines(OrderPostIn in, Order order, AppSession appSession) throws Exception {
		Currency currency = null;
		if (in.getLines().size() <= 0) {
			throw new Exception("Lines is empty!");
		}
		BigDecimal grandTotal = BigDecimal.ZERO;

		if (order.getId() != null) {
			List<com.adaptaconsultoria.itradecore.objects.rest.OrderLine> inRemove = new ArrayList<>();
			List<OrderLine> lineRemove = new ArrayList<>();

			for (OrderLine orderLine : order.getLines()) {
				boolean encontrou = false;
				for (com.adaptaconsultoria.itradecore.objects.rest.OrderLine line : in.getLines()) {
					if (line.getCode() == null) {
						continue;
					}
					if (line.getCode().equals(orderLine.getId())) {
						encontrou = true;
						inRemove.add(line);

						orderLine = validateLine(orderLine, line, appSession);
						try {
							if (currency == null) {
								currency = orderLine.getProduct().getCurrency();
							} else if (!currency.getId().equals(orderLine.getProduct().getCurrency().getId())) {
								throw new Exception();
							}
						} catch (Exception e) {
							throw new Exception("Must be the same currencies!");
						}
						orderLine.setOrder(order);
						grandTotal = grandTotal.add(orderLine.getLineAmount());

						break;
					}
				}
				if (!encontrou) {
					lineRemove.add(orderLine);
				}
			}

			in.getLines().removeAll(inRemove);
			order.getLines().removeAll(lineRemove);
		}

		for (com.adaptaconsultoria.itradecore.objects.rest.OrderLine line : in.getLines()) {
			if (line.getCode() != null) {
				throw new Exception("Line with code (" + line.getCode() + ") not found!");
			}
			OrderLine orderLine = validateLine(null, line, appSession);
			try {
				if (currency == null) {
					currency = orderLine.getProduct().getCurrency();
				} else if (!currency.getId().equals(orderLine.getProduct().getCurrency().getId())) {
					throw new Exception();
				}
			} catch (Exception e) {
				throw new Exception("Must be the same currencies!");
			}
			orderLine.setOrder(order);
			order.getLines().add(orderLine);
			grandTotal = grandTotal.add(orderLine.getLineAmount());

		}

		order.setTotalLines(grandTotal);
		if (order.getFee().compareTo(BigDecimal.ZERO) > 0) {
			try {
				order.setFee(
					grandTotal
						.multiply(order.getFee())
						.divide(new BigDecimal("100.00"), BigDecimal.ROUND_HALF_DOWN)
						.setScale(2, BigDecimal.ROUND_HALF_DOWN)
				);
				if (order.getFee().compareTo(BigDecimal.ZERO) <= 0) {
					throw new Exception();
				}
				order.setFee(order.getFee());
			} catch (Exception e) {
				throw new Exception("Fee could not be calculated!");
			}
		}
		order.setGrandTotal(grandTotal.add(order.getFee()));

		return order;
	}

	private OrderLine validateLine(OrderLine orderLine, com.adaptaconsultoria.itradecore.objects.rest.OrderLine in, AppSession appSession) throws Exception {

		User user = userService.getUserCreating(appSession);
		if (orderLine == null) {
			orderLine = new OrderLine();
			orderLine.setCompany(appSession.getCompany());
			orderLine.setCreatedBy(user);
			orderLine.setCreated(new Date());
		} else {
			orderLine.setUpdatedBy(user);
			orderLine.setUpdated(new Date());
		}

		if (in.getProductCode() == null) {
			throw new Exception("productCode is required!");
		}
		try {
			orderLine.setProduct(productRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getProductCode()));
		} catch (Exception e) {
			throw new Exception("Product not found!");
		}

		orderLine.setPrice(orderLine.getProduct().getPrice());
		if (in.getQty() == null) {
			throw new Exception("Qty is required!");
		} else if (in.getQty().compareTo(BigDecimal.ZERO) <= 0) {
			throw new Exception("Qty should be greater than zero!");
		}

		orderLine.setQty(in.getQty());
		orderLine.setLineAmount(orderLine.getQty().multiply(orderLine.getPrice()));

		if (orderLine.getLineAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new Exception("LineAmount should be greater than zero!");
		}

		if (orderLine.getProduct().getLanguages().size() > 0) {
			if (StringUtils.isBlank(in.getIsoLanguage())) {
				throw new Exception("IsoLanguague is required for this product!");
			}
			try {
				IsoLanguage lang = null;
				for (ProductLanguage language : orderLine.getProduct().getLanguages()) {
					if (language.getIsoLanguage().equals(IsoLanguage.valueOf(in.getIsoLanguage()))) {
						lang = language.getIsoLanguage();
						break;
					}
				}
				if (lang == null) {
					throw new Exception();
				}
				orderLine.setIsoLanguage(lang);
			} catch (Exception e) {
				throw new Exception("IsoLanguague not found!");
			}
		}

		return orderLine;
	}

}

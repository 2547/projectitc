package com.adaptaconsultoria.itradecore.services;

import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.models.Event;
import com.adaptaconsultoria.itradecore.models.EventOrder;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.objects.in.EventOrderPostIn;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;
import com.adaptaconsultoria.itradecore.repositories.EventOrderRepository;
import com.adaptaconsultoria.itradecore.repositories.EventRepository;
import com.adaptaconsultoria.itradecore.services.gateways.GatewayReturn;

@Service
public class EventOrderService {

	@Autowired private UserService userService;
	@Autowired private HttpServletRequest request;
	@Autowired private GatewayService gatewayService;
	@Autowired private EventRepository eventRepository;
	@Autowired private EmailAlcService emailAlcService;
	@Autowired private EventOrderService eventOrderService;
	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private EventOrderRepository eventOrderRepository;

	@Transactional
	public EventOrder save(EventOrder eventOrder) {
		return eventOrderRepository.save(eventOrder);
	}

	public EventOrder createEventOrder(EventOrderPostIn in) throws Exception {
		EventOrder order = new EventOrder();

		if (StringUtils.isBlank(in.getEventHash())) {
			throw new Exception("EventHash is required!");
		}
		try {
			Event event = eventRepository.findTop1ByHashAndSaleUntilGreaterThanEqualAndIsactiveTrue(in.getEventHash(), new Date());
			if (event == null) {
				throw new Exception();
			}

			order.setEvent(event);
			order.setCompany(event.getCompany());

		} catch (Exception e) {
			throw new Exception("Event not found!");
		}

		User userCreating;
		try {
			userCreating = userService.getUserCreating(null);
		} catch (Exception e) {
			userCreating = order.getEvent().getCreatedBy();
		}

		order.setCreatedBy(userCreating);

		if (StringUtils.isBlank(in.getCurrency())) {
			throw new Exception("Currency is required!");
		}
		try {
			Currency currency = currencyRepository.findByCompanyAndCode(order.getCompany(), in.getCurrency());
			if (currency == null) {
				throw new Exception();
			}
			order.setCurrency(currency);
		} catch (Exception e) {
			throw new Exception("Currency not found!");
		}

		if (StringUtils.isBlank(in.getEmail())) {
			throw new Exception("Email is required!");
		}
		order.setEmail(in.getEmail());

		if (StringUtils.isBlank(in.getTicket())) {
			throw new Exception("Ticket is required!");
		}
		order.setTicket(in.getTicket());

		if (in.getQty() == null) {
			throw new Exception("Qty is required!");
		}
		order.setQty(in.getQty());

		if (in.getAmount() == null) {
			throw new Exception("Amount is required!");
		}
		if (in.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			order.setIsPaid(true);
		}
		order.setAmount(in.getAmount());

		order.setDescription(in.getDescription());

		if (!order.getIsPaid()) {
			if (order.getEvent().getGateway() != null) {
				String urlReq = request.getRequestURL().toString().split(request.getContextPath()+"/api")[0];
				String postbackUrl = urlReq + request.getContextPath() + "/api/eventorder/postback";
				System.out.println("\n\nPOSTBACK_URL: " + postbackUrl + "\n");
				order.setPostbackUrl(postbackUrl);
				GatewayReturn ret = gatewayService.payEventOrder(order);
				order.setUrl(ret.getUrl());
				order.setHash(ret.getHash());
			}
		}

		try {
			emailAlcService.sendEmailWelcomeEvent(order);
		} catch (Exception e) {
			System.out.println("\n\nWellcome Event Order e-mail could not be sent!\n");
		}

		return eventOrderService.save(order);
	}

	public boolean pay(String token) {
		try {
			if (StringUtils.isBlank(token)) {
				return false;
			}
			EventOrder eventOrder = eventOrderRepository.findTop1ByHash(token);
			if (!eventOrder.getIsPaid()) {
				eventOrder.setIsPaid(true);
				try {
					emailAlcService.sendEmailPaidEvent(eventOrder);
				} catch (Exception e) {
					System.out.println("\n\nWellcome Event Order e-mail could not be sent!\n");
				}
				save(eventOrder);
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

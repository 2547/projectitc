package com.adaptaconsultoria.itradecore.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AccountPayout;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Gateway;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.objects.in.AccountPayoutPostIn;
import com.adaptaconsultoria.itradecore.repositories.AccountPayoutRepository;
import com.adaptaconsultoria.itradecore.repositories.GatewayRepository;

@Service
public class AccountPayoutService {

	@Autowired private UserService userService;
	@Autowired private AuthyService authyService;
	@Autowired private AccountService accountService;
	@Autowired private GatewayRepository payoutRepository;
	@Autowired private AccountPayoutRepository accountPayoutRepository;

	public AccountPayout save(AccountPayout accountPayout) {
		return accountPayoutRepository.save(accountPayout);
	}

	public AccountPayout createAccountPayout(AccountPayoutPostIn in, AppSession appSession) throws Exception {
		AccountPayout payout = validateCreate(in, appSession);
		return save(payout);
	}

	private AccountPayout validateCreate(AccountPayoutPostIn in, AppSession appSession) throws Exception {

		Account account = accountService.getAccount(appSession);

		if (StringUtils.isNotBlank(account.getId2fa())) {
			if (in.getCode2fa() == null) {
				throw new Exception("Check your Code 2FA, 2-Factor Authentication could not be verified!");
			}

			authyService.verifyUser(in.getCode2fa(), appSession);

		}

		User userCreating = userService.getUserCreating(appSession);

		AccountPayout payout = new AccountPayout();
		try {
			if (in.getCode() != null) {
				payout = accountPayoutRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getCode());
				if (payout == null) {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			throw new Exception("AccountPayout not found!");
		}

		if (payout.getId() == null) {
			payout.setCompany(appSession.getCompany());
			payout.setCreatedBy(userCreating);
			payout.setAccount(account);
		} else {
			payout.setUpdatedBy(userCreating);
			payout.setUpdated(new Date());
			if (!payout.getAccount().getId().equals(account.getId())) {
				throw new Exception("Permission denied!");
			}
		}

		if (in.getGatewayCode() == null) {
			throw new Exception("GatewayCode is required!");
		}

		try {
			Gateway p = payoutRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getGatewayCode());
			if (p == null) {
				throw new Exception();
			}
			payout.setGateway(p);
		} catch (Exception e) {
			throw new Exception("Gateway not found!");
		}

		if (StringUtils.isBlank(in.getValue())) {
			throw new Exception("Value is required!");
		}
		payout.setValue(in.getValue());

		return payout;
	}

	public List<AccountPayout> getAccountPayouts(AppSession appSession) throws Exception {
		try {
			Account account = accountService.getAccount(appSession);
			return accountPayoutRepository.findByCompanyAndAccount(account.getCompany(), account);
		} catch (Exception ignore) {
		}
		return new ArrayList<>();
	}

}

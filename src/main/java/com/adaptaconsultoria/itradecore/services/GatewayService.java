package com.adaptaconsultoria.itradecore.services;

import java.math.BigDecimal;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.EventOrder;
import com.adaptaconsultoria.itradecore.models.Gateway;
import com.adaptaconsultoria.itradecore.models.Order;
import com.adaptaconsultoria.itradecore.objects.rest.CurrencyQuote;
import com.adaptaconsultoria.itradecore.repositories.GatewayRepository;
import com.adaptaconsultoria.itradecore.services.gateways.GatewayInterface;
import com.adaptaconsultoria.itradecore.services.gateways.GatewayReturn;

@Service
public class GatewayService {

	private static final String CLASS_PACKAGE = "com.adaptaconsultoria.itradecore.services.gateways.";

	@Autowired private GatewayRepository gatewayRepository;
	@Autowired private CurrencyQuoteService currencyQuoteService;

	public Gateway save(Gateway gateway) {
		return gatewayRepository.save(gateway);
	}

	@Transactional
	public void delete(Long gatewayId) throws Exception {
		Optional<Gateway> gateway = gatewayRepository.findById(gatewayId);
		if (gateway.isPresent()) {
			gatewayRepository.delete(gateway.get());
		} else {
			throw new Exception("Gateway not found!");
		}
	}

	@Transactional
	public GatewayReturn payEventOrder(EventOrder eventOrder) throws Exception {
		try {

			if (eventOrder.getIsPaid())
				return null;

			if (eventOrder.getEvent().getGateway() == null)
				return null;

		} catch (Exception e) {
			return null;
		}

		Gateway gateway = eventOrder.getEvent().getGateway();
		String className = CLASS_PACKAGE + gateway.getClassName();

	    GatewayInterface gatewayInterface = null;
	    try {
	    	gatewayInterface = (GatewayInterface) Class.forName(className).newInstance();
	    } catch (Exception e) {
			throw new Exception("Classname not found!");
	    }

	    BigDecimal gt = eventOrder.getAmount();
	    eventOrder.setAmount(getCurrencyQuote(eventOrder.getCompany(), eventOrder.getEvent().getGateway(), eventOrder.getCurrency().getCode(), eventOrder.getAmount()));

	    gatewayInterface.createObject(eventOrder);

	    eventOrder.setAmount(gt);

	    return gatewayInterface.pay();
	}

	@Transactional
	public GatewayReturn payOrder(Order order) throws Exception {
		try {
			if (order.getIsPaid()) {
				return null;
			}
			if (order.getGateway() == null)
				return null;
		} catch (Exception e) {
			return null;
		}

		Gateway gateway = order.getGateway();
		String className = CLASS_PACKAGE + gateway.getClassName();

	    GatewayInterface gatewayInterface = null;
	    try {
	    	gatewayInterface = (GatewayInterface) Class.forName(className).newInstance();
	    } catch (Exception e) {
			throw new Exception("Classname not found!");
	    }

	    BigDecimal gt = order.getGrandTotal();
		order.setGrandTotal(getCurrencyQuote(order.getCompany(), order.getGateway(), order.getCurrency().getCode(), order.getGrandTotal()));

	    gatewayInterface.createObject(order);

	    order.setGrandTotal(gt);

	    return gatewayInterface.pay();
	}

	private BigDecimal getCurrencyQuote(Company company, Gateway gateway, String currencyCode, BigDecimal value) throws Exception {

	    if (!currencyCode.equalsIgnoreCase(gateway.getCurrency().getCode())) {
	    	boolean find = false;
	    	for (CurrencyQuote quote : currencyQuoteService.getCurrencyQuotes(company, currencyCode, value)) {
	    		if (quote.getCurrency().equalsIgnoreCase(gateway.getCurrency().getCode())) {
	    			find = true;
	    			return quote.getValue();
	    		}
	    	}
	    	if (!find) {
	    		throw new Exception("Currency Quote not found!");
	    	}
	    }

		return value;
	}

}

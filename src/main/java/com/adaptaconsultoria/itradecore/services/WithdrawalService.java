package com.adaptaconsultoria.itradecore.services;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.AccountPayout;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.CompanyConfig;
import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.models.Withdrawal;
import com.adaptaconsultoria.itradecore.objects.in.WithdrawalPostIn;
import com.adaptaconsultoria.itradecore.objects.out.WithdrawalGetOut;
import com.adaptaconsultoria.itradecore.repositories.CompanyConfigRepository;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;
import com.adaptaconsultoria.itradecore.utils.DateUtility;

@Service
public class WithdrawalService {

	@Autowired private UserService userService;
	@Autowired private AccountService accountService;
	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private AccountPayoutService accountPayoutService;
	@Autowired private CompanyConfigRepository companyConfigRepository;

	public void doWithdrawal(WithdrawalPostIn in, AppSession appSession) throws Exception {

		if (!canRequest(appSession)) {
			throw new Exception("Withdrawal cannot be performed!");
		}

		if (in.getAccountPayoutCode() == null) {
			throw new Exception("AccountPayoutCode is required!");
		}

		Withdrawal withdrawal = new Withdrawal();

		for (AccountPayout payout : accountPayoutService.getAccountPayouts(appSession)) {
			if (in.getAccountPayoutCode().equals(payout.getId())) {
				withdrawal.setAccountPayout(payout);
				break;
			}
		}

		if (withdrawal.getAccountPayout() == null) {
			throw new Exception("AccountPayout not found!");
		}

		if (StringUtils.isBlank(in.getCurrency())) {
			throw new Exception("Currency is required!");
		}
		try {
			Currency currency = currencyRepository.findByCompanyAndCode(appSession.getCompany(), in.getCurrency());
			if (currency == null) {
				throw new Exception();
			}
			withdrawal.setCurrency(currency);
		} catch (Exception e) {
			throw new Exception("Currency not found!");
		}

		if (in.getAmount() == null) {
			throw new Exception("Amount is required!");
		} else if (in.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new Exception("Amount should be greater than zero!");
		}
		withdrawal.setAmount(in.getAmount());

		CompanyConfig companyConfig = companyConfigRepository.findTop1ByCompanyAndIsactiveTrue(appSession.getCompany());

		withdrawal.setDateplanned(DateUtility.addDay(new Date(), companyConfig.getWithdrawalDTP()));

		try {
			if (companyConfig.getCompany().getPayoutFee().compareTo(BigDecimal.ZERO) > 0) {
				BigDecimal fee = withdrawal.getAmount()
						.multiply(companyConfig.getCompany().getPayoutFee())
						.divide(new BigDecimal("100.00"), BigDecimal.ROUND_HALF_DOWN)
						.setScale(2, BigDecimal.ROUND_HALF_DOWN);
				if (fee.compareTo(BigDecimal.ZERO) <= 0) {
					throw new Exception();
				}
				withdrawal.setFee(fee);
			}
		} catch (Exception e) {
			throw new Exception("Fee could not be calculated!");
		}

		withdrawal.setPayoutCost(BigDecimal.ZERO);

		withdrawal.setNetAmount(withdrawal.getAmount().subtract(withdrawal.getFee()).subtract(withdrawal.getPayoutCost()));
		if (withdrawal.getNetAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new Exception("Net Amount should be greater than zero!");
		}

		withdrawal.setAccount(accountService.getAccount(appSession));
		withdrawal.setCompany(appSession.getCompany());
		withdrawal.setCreatedBy(userService.getUserCreating(appSession));

	}

	public WithdrawalGetOut checkParameters(AppSession appSession, WithdrawalGetOut out) throws Exception {
		//
		if (!canRequest(appSession)) {
			throw new Exception("Withdrawal cannot be performed!");
		}

		CompanyConfig companyConfig = companyConfigRepository.findTop1ByCompanyAndIsactiveTrue(appSession.getCompany());

		out.setWithdrawalDate(DateUtility.addDay(new Date(), companyConfig.getWithdrawalDTP()));
		try {
			out.setFee(companyConfig.getCompany().getPayoutFee());
			if (out.getFee() == null) {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception("Payout Fee not found!");
		}

		return out;
	}

	public boolean canRequest(AppSession appSession) throws Exception {

		if (accountPayoutService.getAccountPayouts(appSession).size() <= 0) {
			throw new Exception("Account has no payout configured!");
		}

		CompanyConfig companyConfig = companyConfigRepository.findTop1ByCompanyAndIsactiveTrue(appSession.getCompany());
		if (companyConfig == null) {
			throw new Exception("Company Config not found!");
		}

		Date currentDate = new Date();

		boolean isDateValid = false;
		// Verifica se é o dia da semana para pedir saque
		try {
			if (DateUtility.getDayOfWeek(currentDate) == companyConfig.getWithdrawalDOW()) {
				isDateValid = true;
			}
		} catch (Exception e) {
		}

		if (!isDateValid) {
			// Verifica se é o dia do mês para pedir saque
			try {
				if (DateUtility.getDayOfMonth(DateUtility.lastDayOfMonth(currentDate)) <= companyConfig.getWithdrawalDOM()) {
					isDateValid = true;
				}
				if (DateUtility.getDayOfMonth(currentDate) == companyConfig.getWithdrawalDOM()) {
					isDateValid = true;
				}
			} catch (Exception e) {
			}
		}

		if (!isDateValid) {
			throw new Exception("Withdrawal cannot be made on the current date!");
		}

		if (companyConfig.getWithdrawalDTP() == null) {
			throw new Exception("Parameter Withdrawal Date to Pay not found!");
		}

		return true;
	}

}

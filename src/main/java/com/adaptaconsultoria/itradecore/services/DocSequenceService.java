package com.adaptaconsultoria.itradecore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.DocSequence;
import com.adaptaconsultoria.itradecore.repositories.DocSequenceRepository;

@Service
public class DocSequenceService {

	@Autowired private DocSequenceRepository docSequenceRepository;

	public DocSequence save(DocSequence docSequence) {
		return docSequenceRepository.save(docSequence);
	}

	public void remove(DocSequence docSequence) {
		docSequenceRepository.delete(docSequence);
	}

	public Long nextSequence(DocSequence docSequence) throws Exception {
		try {
			docSequence = docSequenceRepository.findById(docSequence.getId()).get();
		} catch (Exception e) {
			throw new Exception("No sequence for next account!");
		}

		if (docSequence == null) {
			throw new Exception("No sequence for next account!");
		}

		Long next = docSequence.getNextValue();

		if (next > docSequence.getMaxValue()) {
			throw new Exception("Exceeded maximum limit!");
		}

		docSequence.setNextValue(docSequence.getNextValue() + docSequence.getIncrease());
		docSequence = save(docSequence);

		return next;
	}

}
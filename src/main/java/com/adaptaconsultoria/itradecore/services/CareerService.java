package com.adaptaconsultoria.itradecore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.BinaryNetwork;
import com.adaptaconsultoria.itradecore.objects.out.CareerStatementGetOut;
import com.adaptaconsultoria.itradecore.objects.rest.BinaryJournal;
import com.adaptaconsultoria.itradecore.repositories.BinaryJournalRepository;
import com.adaptaconsultoria.itradecore.repositories.BinaryNetworkRepository;

@Service
public class CareerService {

	@Autowired private AccountService accountService;
	@Autowired private BinaryNetworkRepository binaryNetworkRepository;
	@Autowired private BinaryJournalRepository binaryJournalRepository;

	public CareerStatementGetOut getCareer(AppSession appSession, CareerStatementGetOut out) throws Exception {

		Account account = accountService.getAccount(appSession);

		BinaryNetwork binaryNetwork = binaryNetworkRepository.findTop1ByCompanyAndAccount(appSession.getCompany(), account);
		if (binaryNetwork == null) {
			throw new Exception("Binary Network not found!");
		}

		out.setLeftPoints(binaryNetwork.getLeftPoints());
		out.setRightPoints(binaryNetwork.getRightPoints());
		out.setLeftTotalPoints(binaryNetwork.getLeftTotalPoints());
		out.setRightTotalPoints(binaryNetwork.getRightTotalPoints());

		out.setJournals(BinaryJournal.fromModels(binaryJournalRepository.findByCompanyAndBinaryNetwork(appSession.getCompany(), binaryNetwork)));

		return out;
	}

}

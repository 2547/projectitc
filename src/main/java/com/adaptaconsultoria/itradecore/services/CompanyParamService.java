package com.adaptaconsultoria.itradecore.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.repositories.CompanyParamRepository;

@Service
public class CompanyParamService {

	@Autowired private CompanyParamRepository companyParamRepository;

	public String getAppUrl(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "app_url").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("URL App not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"app_url\" not found!");
		}
		return param;
	}

	public String getRemoteEndpointAuthentication(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "remote_endpoint_authentication").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Endpoint Remote Authentication not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"remote_endpoint_authentication\" not found!");
		}
		return param;
	}

	public String getRemoteEndpointRecovery(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "remote_endpoint_recovery").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Endpoint Remote Recovery not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"remote_endpoint_recovery\" not found!");
		}
		return param;
	}

	public String getAlcUrl(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "alc_url").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("URL Alc not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"alc_url\" not found!");
		}
		return param;
	}

	public String getAlcEndpointSendemail(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "alc_endpoint_sendmail").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Endpoint Alc Send E-mail not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"alc_endpoint_sendmail\" not found!");
		}
		return param;
	}

	public String getAlcEndpointAuthentication(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "alc_endpoint_authentication").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Endpoint Alc Authentication not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"alc_endpoint_authentication\" not found!");
		}
		return param;
	}

	public String getAlcCodeEmailRecovery(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "alc_code_email_recovery").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Code E-mail Alc not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"alc_code_email_recovery\" not found!");
		}
		return param;
	}

	public String getAlcCodeEmailWellcome(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "alc_code_email_wellcome").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Code E-mail Alc not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"alc_code_email_wellcome\" not found!");
		}
		return param;
	}

	public String getAlcCodeOrderReceived(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "alc_code_order_received").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Code E-mail Alc Order Received not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"alc_code_order_received\" not found!");
		}
		return param;
	}

	public String getAlcCodeOrderPaid(Company company) throws Exception {
		String param;
		try {
			param = companyParamRepository.findTop1ByCompanyAndNameAndIsactiveTrue(company, "alc_code_order_paid").getValue();
			if (StringUtils.isBlank(param)) {
				throw new Exception("Code E-mail Alc Order Paid not found!");
			}
		} catch (Exception e) {
			throw new Exception("Parameter \"alc_code_order_paid\" not found!");
		}
		return param;
	}

}

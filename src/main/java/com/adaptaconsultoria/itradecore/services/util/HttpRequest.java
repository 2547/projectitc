package com.adaptaconsultoria.itradecore.services.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 *
 * @author Gabriel Augustin
 * @author www.adaptaconsultoria.com.br
 * @version 1.0.0
 *
 */
public class HttpRequest {

	private HttpRequest() {
	}

	public static String post(String url, List<NameValuePair> nameValuePairs) throws Exception {
		return post(url, nameValuePairs, "");
	}
	public static String post(String url, List<NameValuePair> nameValuePairs, String... headers) throws Exception {
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		post.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

		for (String header : headers) {
			try {
				String[] pair = header.split(":");
				post.addHeader(pair[0].trim(), pair[1].trim());
			} catch (Exception e) {
			}
		}

		HttpResponse response = client.execute(post);

		System.out.println("**POST** request Url: " + post.getURI());
		// System.out.println("Parameters : " + nameValuePairs);
		System.out.println("Response Code: " + response.getStatusLine().getStatusCode());
		System.out.println("Content:-\n");
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		String line = "", retorno = "";
//		if (response.getStatusLine().getStatusCode() == 200) {
			while ((line = rd.readLine()) != null) {
				retorno += line;
				System.out.println(line);
			}
//		} else {
//			retorno = "Error: " + response.getStatusLine().getStatusCode();
//		}
		return retorno;
	}

	// recebe uma url, uma lista de parametros que não são arquivos, um arquivo, e o nome do parametro que esse arquivo precissa ter
	public static String postFile(String url, List<NameValuePair> nameValuePairs, File file, String fileParamName) throws Exception {
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

	    FileBody bin = new FileBody(file, ContentType.create("*", MIME.UTF8_CHARSET));

	    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
	    builder.setCharset(Charset.forName("UTF-8"));
	    builder.addPart(fileParamName, bin);
	    
	    for (NameValuePair nameValuePair : nameValuePairs) {
	    	builder.addTextBody(nameValuePair.getName(), nameValuePair.getValue(), ContentType.create("text/plain", MIME.UTF8_CHARSET));
		}

	    post.setEntity(builder.build());

		HttpResponse response = client.execute(post);

		System.out.println("**POST** request Url: " + post.getURI());
		// System.out.println("Parameters : " + nameValuePairs);
		System.out.println("Response Code: " + response.getStatusLine().getStatusCode());
		System.out.println("Content:-\n");
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		String line = "", retorno = "";
//		if (response.getStatusLine().getStatusCode() == 200) {
			while ((line = rd.readLine()) != null) {
				retorno += line;
				System.out.println(line);
			}
//		} else {
//			retorno = "Error: " + response.getStatusLine().getStatusCode();
//		}
		return retorno;
	}

	public static String get(String url) throws Exception {
		return get(url, "");
	}
	public static String get(String url, String... headers) throws Exception {
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);

		for (String header : headers) {
			try {
				String[] pair = header.split(":");
				request.addHeader(pair[0].trim(), pair[1].trim());
			} catch (Exception e) {
			}
		}

		HttpResponse response = client.execute(request);

		System.out.println("**GET** request Url: " + request.getURI());
		System.out.println("Response Code: " + response.getStatusLine().getStatusCode());
		System.out.println("Content:-\n");
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		String line = "", retorno = "";
//		if (response.getStatusLine().getStatusCode() == 200) {
			while ((line = rd.readLine()) != null) {
				retorno += line;
				System.out.println(line);
			}
//		} else {
//			retorno = "Error: " + response.getStatusLine().getStatusCode();
//		}

		return retorno;
	}
}
package com.adaptaconsultoria.itradecore.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.objects.in.AuthAlcPostIn;
import com.adaptaconsultoria.itradecore.objects.out.AuthPostOut;
import com.adaptaconsultoria.itradecore.services.util.AppInfoService;
import com.adaptaconsultoria.itradecore.utils.JsonUtility;

@Service
public class TokenAlcService {

	@Autowired private RequestService requestService;
	@Autowired private AppInfoService appInfoService;
	@Autowired private CompanyParamService companyParamService;

	public String getAcessToken(Company company) throws Exception {

		if (company == null) {
			throw new Exception("Company not found!");
		}

		String urlAlc                    = companyParamService.getAlcUrl(company);
		String endpointAlcAuthentication = companyParamService.getAlcEndpointAuthentication(company);

		// Autenticação ALC
		AuthAlcPostIn authIn = new AuthAlcPostIn();
		authIn.setAppToken(appInfoService.getAppToken());
		authIn.setAppPassword(appInfoService.getAppPassword());
		authIn.setIpAddress(appInfoService.getIpAdress());
		try {
			AuthPostOut out = (AuthPostOut) JsonUtility.objToObj(requestService.postRequest(urlAlc + endpointAlcAuthentication, authIn), new AuthPostOut());
			if (out == null) {
				throw new Exception();
			}
			if (out.isHasError()) {
				throw new Exception();
			}
			if (StringUtils.isBlank(out.getToken())) {
				throw new Exception();
			}
			return out.getToken();
		} catch (Exception e) {
			throw new Exception("Authentication ALC failed!");
		}

	}

}
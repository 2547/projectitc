package com.adaptaconsultoria.itradecore.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.BinaryNetwork;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.models.enums.BinarySide;
import com.adaptaconsultoria.itradecore.objects.in.BinaryGetIn;
import com.adaptaconsultoria.itradecore.repositories.AccountRepository;
import com.adaptaconsultoria.itradecore.repositories.BinaryNetworkRepository;

@Service
public class BinaryService {

	@Autowired private UserService userService;
	@Autowired private AccountService accountService;
	@Autowired private AccountRepository accountRepository;
	@Autowired private BinaryNetworkRepository binaryNetworkRepository;

	public List<BinaryNetwork> getTree(BinaryGetIn in, AppSession appSession) throws Exception {

		Account accountLogged = accountService.getAccount(appSession);

		Account account = new Account();
		if (StringUtils.isNotBlank(in.getAccountNo())) {
			try {
				account = accountRepository.findTop1ByCompanyAndAccountNo(appSession.getCompany(), in.getAccountNo());
				if (account == null) {
					account = accountRepository.findTop1ByCompanyAndAccountNoOrLogin(appSession.getCompany(), in.getQuery());
					throw new Exception();
				}
			} catch (Exception e) {
				throw new Exception("Account not found!");
			}
		} else if (StringUtils.isNotBlank(in.getQuery())) {
			try {
				account = accountRepository.findTop1ByCompanyAndAccountNoOrLogin(appSession.getCompany(), in.getQuery());
				if (account == null) {
					throw new Exception();
				}
			} catch (Exception e) {
				throw new Exception("Account not found!");
			}
		} else {
			account = accountLogged;
		}

		if (!account.getId().equals(accountLogged.getId())) {
			if (!binaryNetworkRepository.getBinaryIsChild(account.getId(), accountLogged.getId())) {
				throw new Exception("Permission denied!");
			}
		}

		Integer level = 1;
		if (in.getLevel() != null && in.getLevel() > 0) {
			level = in.getLevel();
		}
		String[] tree = binaryNetworkRepository.getTreeAccountAndLevel(account.getId(), level).replace("{", "").replace("}", "").split(",");
		List<BinaryNetwork> binaries = new ArrayList<>();
		for (String id : tree) {
			BinaryNetwork binary = new BinaryNetwork();
			if (StringUtils.isNotBlank(id)) {
				try {
					binary = binaryNetworkRepository.findById(Long.parseLong(id)).get();
				} catch (Exception e) {
				}
			}
			binaries.add(binary);
		}
		return binaries;
	}

	public BinarySide getSide(AppSession appSession) throws Exception {
		Account account = accountService.getAccount(appSession);
		return account.getBinarySide();
	}

	public void setSide(String side, AppSession appSession) throws Exception {
		if (StringUtils.isBlank(side)) {
			throw new Exception("Side is required!");
		}
		BinarySide binarySide;
		try {
			binarySide = BinarySide.valueOf(side);
		} catch (Exception e) {
			throw new Exception("Side not found!");
		}
		Account account = accountService.getAccount(appSession);
		account.setBinarySide(binarySide);

		try {
			User user = userService.getUserCreating(appSession);
			account.setUpdatedBy(user);
			account.setUpdated(new Date());
			account = accountService.save(account);
		} catch (Exception e) {
			throw new Exception("Binary Side could not be saved!");
		}
	}

}

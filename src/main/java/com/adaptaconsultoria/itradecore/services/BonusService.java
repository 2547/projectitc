package com.adaptaconsultoria.itradecore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Bonus;
import com.adaptaconsultoria.itradecore.repositories.BonusRepository;

@Service
public class BonusService {

	@Autowired private BonusRepository bonusRepository;

	public Bonus save(Bonus bonus) {
		return bonusRepository.save(bonus);
	}

	@Transactional
	public void delete(Long bonusId) throws Exception {
		Optional<Bonus> bonus = bonusRepository.findById(bonusId);
		if (bonus.isPresent()) {
			bonusRepository.delete(bonus.get());
		} else {
			throw new Exception("Bonus not found!");
		}
	}

}

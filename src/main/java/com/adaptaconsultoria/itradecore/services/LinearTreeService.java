package com.adaptaconsultoria.itradecore.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.LinearTree;
import com.adaptaconsultoria.itradecore.objects.in.LinearGetIn;
import com.adaptaconsultoria.itradecore.repositories.LinearTreeRepository;

@Service
public class LinearTreeService {

	@Autowired private AccountService accountService;
	@Autowired private LinearTreeRepository linearTreeRepository;

	public List<LinearTree> getTree(LinearGetIn in, AppSession appSession) throws Exception {

		Account account = new Account();
		try {
			account = accountService.getAccount(appSession);
			if (account == null) {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception("Account not found!");
		}

		Integer level = 0;
		if (in.getDepth() != null && in.getDepth() > 0) {
			level = in.getDepth();
		}

		return linearTreeRepository.getTreeAccountAndLevel(account.getId(), level);
	}

}

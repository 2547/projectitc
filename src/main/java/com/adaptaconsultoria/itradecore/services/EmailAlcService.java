package com.adaptaconsultoria.itradecore.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.EventOrder;
import com.adaptaconsultoria.itradecore.models.Order;
import com.adaptaconsultoria.itradecore.objects.out.DefaultOut;
import com.adaptaconsultoria.itradecore.objects.out.EmailSenderOut;
import com.adaptaconsultoria.itradecore.objects.rest.Parameter;
import com.adaptaconsultoria.itradecore.services.util.AppInfoService;
import com.adaptaconsultoria.itradecore.utils.DateUtility;
import com.adaptaconsultoria.itradecore.utils.JsonUtility;

@Service
public class EmailAlcService {

	@Autowired private RequestService requestService;
	@Autowired private CompanyParamService companyParamService;
	@Autowired private AppInfoService appInfoService;
	@Autowired private TokenAlcService tokenAlcService;

	public void sendEmailWellcome(Account account) throws Exception {

		if (account == null) {
			throw new Exception("Account not found!");
		}

		Company company = account.getCompany();

		String urlAlc               = companyParamService.getAlcUrl(company);
		String endpointAlcSendemail = companyParamService.getAlcEndpointSendemail(company);
		String codeEmailAlc         = companyParamService.getAlcCodeEmailWellcome(company);

		// Autenticação ALC
		String token = tokenAlcService.getAcessToken(company);

		// Envio do email
		EmailSenderOut obj = new EmailSenderOut();
		obj.setToken(token);
		obj.setIpAddress(appInfoService.getIpAdress());

		obj.setId(codeEmailAlc);
		obj.getReceivers().add(account.getUser().getEmail());

		try {
			Parameter parameter = new Parameter();
			parameter.setName("name");
			parameter.setValue(account.getUser().getName());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
			throw new Exception("User not found!");
		}

		try {
			Parameter parameter = new Parameter();
			parameter.setName("login");
			parameter.setValue(account.getUser().getLogin());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
			throw new Exception("User not found!");
		}

		try {
			Parameter parameter = new Parameter();
			parameter.setName("link");
			parameter.setValue(companyParamService.getAppUrl(company));
			obj.getParameters().add(parameter);
		} catch (Exception e) {
		}

		enviar(urlAlc + endpointAlcSendemail, obj);

	}

	public void sendEmailRecovery(AppSession appSession, Company company, String urlRecovery, String email) throws Exception {

		if (company == null) {
			throw new Exception("Company not found!");
		}

		String urlAlc               = companyParamService.getAlcUrl(company);
		String endpointAlcSendemail = companyParamService.getAlcEndpointSendemail(company);
		String codeEmailAlc         = companyParamService.getAlcCodeEmailRecovery(company);

		// Autenticação ALC
		String token = tokenAlcService.getAcessToken(company);

		// Envio do email
		EmailSenderOut obj = new EmailSenderOut();
		obj.setToken(token);
		obj.setIpAddress(appInfoService.getIpAdress());

		obj.setId(codeEmailAlc);
		obj.getReceivers().add(email);

		Parameter param = new Parameter();
		param.setName("url");
		param.setValue(urlRecovery);
		obj.getParameters().add(param);

		try {
			Parameter parameter = new Parameter();
			parameter.setName("name");
			parameter.setValue(appSession.getUser().getName());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
			throw new Exception("User not found!");
		}

		try {
			Parameter parameter = new Parameter();
			parameter.setName("login");
			parameter.setValue(appSession.getUser().getLogin());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
			throw new Exception("User not found!");
		}

		try {
			Parameter parameter = new Parameter();
			parameter.setName("obs");
			parameter.setValue("Link estará ativo até: " + DateUtility.dateToString(appSession.getValidto(), "HH:mm:ss"));
			obj.getParameters().add(parameter);
		} catch (Exception e) {
		}

		enviar(urlAlc + endpointAlcSendemail, obj);

	}

	public void sendEmailWelcomeEvent(EventOrder order) throws Exception {

		if (order == null) {
			throw new Exception("Event order not found!");
		}

		Company company = order.getCompany();

		if (order.getEvent().getWelcomeMsgToken() == null) {
			throw new Exception("Welcome Message Token not found!");
		}

		String urlAlc               = companyParamService.getAlcUrl(company);
		String endpointAlcSendemail = companyParamService.getAlcEndpointSendemail(company);
		String codeEmailAlc         = order.getEvent().getWelcomeMsgToken().toString();

		// Autenticação ALC
		String token = tokenAlcService.getAcessToken(company);

		// Envio do email
		EmailSenderOut obj = new EmailSenderOut();
		obj.setToken(token);
		obj.setIpAddress(appInfoService.getIpAdress());

		obj.setId(codeEmailAlc);
		obj.getReceivers().add(order.getEmail());

		try {
			Parameter parameter = new Parameter();
			parameter.setName("url");
			String url = "#";
			if (StringUtils.isNotBlank(order.getUrl())) {
				url = order.getUrl();
			}
			parameter.setValue(url);
			obj.getParameters().add(parameter);
		} catch (Exception e) {
		}

		enviar(urlAlc + endpointAlcSendemail, obj);

	}

	public void sendEmailPaidEvent(EventOrder order) throws Exception {

		if (order == null) {
			throw new Exception("Event order not found!");
		}

		Company company = order.getCompany();

		if (order.getEvent().getConfirmationMsgToken() == null) {
			throw new Exception("Confirmation Message Token not found!");
		}

		String urlAlc               = companyParamService.getAlcUrl(company);
		String endpointAlcSendemail = companyParamService.getAlcEndpointSendemail(company);
		String codeEmailAlc         = order.getEvent().getConfirmationMsgToken().toString();

		// Autenticação ALC
		String token = tokenAlcService.getAcessToken(company);

		// Envio do email
		EmailSenderOut obj = new EmailSenderOut();
		obj.setToken(token);
		obj.setIpAddress(appInfoService.getIpAdress());

		obj.setId(codeEmailAlc);
		obj.getReceivers().add(order.getEmail());

		enviar(urlAlc + endpointAlcSendemail, obj);

	}

	public void sendEmailReceivedOrder(Order order) throws Exception {

		if (order == null) {
			throw new Exception("Event order not found!");
		}

		Company company = order.getCompany();

		String urlAlc               = companyParamService.getAlcUrl(company);
		String endpointAlcSendemail = companyParamService.getAlcEndpointSendemail(company);
		String codeEmailAlc         = companyParamService.getAlcCodeOrderReceived(company);;

		// Autenticação ALC
		String token = tokenAlcService.getAcessToken(company);

		// Envio do email
		EmailSenderOut obj = new EmailSenderOut();
		obj.setToken(token);
		obj.setIpAddress(appInfoService.getIpAdress());

		obj.setId(codeEmailAlc);
		obj.getReceivers().add(order.getAccount().getUser().getEmail());

		try {
			Parameter parameter = new Parameter();
			parameter.setName("name");
			parameter.setValue(order.getAccount().getUser().getName());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
		}

		try {
			Parameter parameter = new Parameter();
			parameter.setName("order_number");
			parameter.setValue(order.getId().toString());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
		}

		enviar(urlAlc + endpointAlcSendemail, obj);

	}

	public void sendEmailPaidOrder(Order order) throws Exception {

		if (order == null) {
			throw new Exception("Event order not found!");
		}

		Company company = order.getCompany();

		String urlAlc               = companyParamService.getAlcUrl(company);
		String endpointAlcSendemail = companyParamService.getAlcEndpointSendemail(company);
		String codeEmailAlc         = companyParamService.getAlcCodeOrderPaid(company);;

		// Autenticação ALC
		String token = tokenAlcService.getAcessToken(company);

		// Envio do email
		EmailSenderOut obj = new EmailSenderOut();
		obj.setToken(token);
		obj.setIpAddress(appInfoService.getIpAdress());

		obj.setId(codeEmailAlc);
		obj.getReceivers().add(order.getAccount().getUser().getEmail());

		try {
			Parameter parameter = new Parameter();
			parameter.setName("name");
			parameter.setValue(order.getAccount().getUser().getName());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
		}

		try {
			Parameter parameter = new Parameter();
			parameter.setName("order_number");
			parameter.setValue(order.getId().toString());
			obj.getParameters().add(parameter);
		} catch (Exception e) {
		}

		enviar(urlAlc + endpointAlcSendemail, obj);

	}

	private void enviar(String url, EmailSenderOut obj) throws Exception {
		String msgError = "E-mail could not be sent!";
		try {
			DefaultOut out = (DefaultOut) JsonUtility.objToObj(requestService.postRequest(url, obj), new DefaultOut());
			if (out == null) {
				throw new Exception();
			}
			if (out.isHasError()) {
				if (StringUtils.isBlank(out.getError().getError())) {
					throw new Exception();
				}
				msgError = out.getError().getError();
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception(msgError);
		}
	}

}

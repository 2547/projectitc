package com.adaptaconsultoria.itradecore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Plan;
import com.adaptaconsultoria.itradecore.repositories.PlanRepository;

@Service
public class PlanService {

	@Autowired private PlanRepository planRepository;

	public Plan save(Plan plan) {
		return planRepository.save(plan);
	}

	@Transactional
	public void delete(Long productId) throws Exception {
		Optional<Plan> plan = planRepository.findById(productId);
		if (plan.isPresent()) {
			planRepository.delete(plan.get());
		} else {
			throw new Exception("Product not found!");
		}
	}

}

package com.adaptaconsultoria.itradecore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.LogAccess;
import com.adaptaconsultoria.itradecore.repositories.LogAccessRepository;

@Service
public class LogAccessService {

	@Autowired private LogAccessRepository logAcessoRepository;
	
	public LogAccess save(LogAccess logAcesso ) {
		return logAcessoRepository.save(logAcesso);
	}

}

package com.adaptaconsultoria.itradecore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Product;
import com.adaptaconsultoria.itradecore.repositories.ProductRepository;

@Service
public class ProductService {

	@Autowired private ProductRepository productRepository;

	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Transactional
	public void delete(Long productId) throws Exception {
		Optional<Product> product = productRepository.findById(productId);
		if (product.isPresent()) {
			productRepository.delete(product.get());
		} else {
			throw new Exception("Product not found!");
		}
	}

}

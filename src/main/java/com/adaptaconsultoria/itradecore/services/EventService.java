package com.adaptaconsultoria.itradecore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Event;
import com.adaptaconsultoria.itradecore.repositories.EventRepository;

@Service
public class EventService {

	@Autowired private EventRepository eventRepository;

	@Transactional
	public Event save(Event event) {
		if (event.getId() == null) {
			event.setId(eventRepository.nextId());
		}
		return eventRepository.save(event);
	}

	@Transactional
	public void delete(Long eventId) throws Exception {
		Optional<Event> event = eventRepository.findById(eventId);
		if (event.isPresent()) {
			eventRepository.delete(event.get());
		} else {
			throw new Exception("Event not found!");
		}
	}

}

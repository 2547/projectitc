package com.adaptaconsultoria.itradecore.services;

import org.springframework.stereotype.Service;

@Service
public interface EmailService {
	
	public Integer runGenerateQueueEmail();
	
	public void loadEmailsSetAndSend();
	
	public String send(	String host, 
						Integer port, 
						String username, 
						String password, 
						String subject,
						String message, 
						String to, 
						String from);
}


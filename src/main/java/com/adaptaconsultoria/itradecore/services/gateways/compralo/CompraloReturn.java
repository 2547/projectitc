package com.adaptaconsultoria.itradecore.services.gateways.compralo;

import lombok.Data;

@Data
public class CompraloReturn {

	private boolean status;
	private String message;
	private String token;
	private String checkout_url;
	private String hash;

	private CompraloErrors errors;

}

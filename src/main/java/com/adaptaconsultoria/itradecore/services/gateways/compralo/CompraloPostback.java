package com.adaptaconsultoria.itradecore.services.gateways.compralo;

import lombok.Data;

@Data
public class CompraloPostback {

	private String status;
	private String token;

}

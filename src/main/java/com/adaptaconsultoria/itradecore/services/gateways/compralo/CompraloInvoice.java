package com.adaptaconsultoria.itradecore.services.gateways.compralo;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CompraloInvoice {

	private String apiKey;
	private String storeName;
	private BigDecimal value;
	private String postbackUrl;
	private String description;
	private String backUrl;

}

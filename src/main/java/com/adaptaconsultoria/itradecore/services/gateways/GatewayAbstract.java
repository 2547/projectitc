package com.adaptaconsultoria.itradecore.services.gateways;

public abstract class GatewayAbstract implements GatewayInterface {

	public Object obj;

	@Override
	public void createObject(Object obj) {
		this.obj = obj;
	}

	@Override
	public Object getObject() {
		return obj;
	}

}

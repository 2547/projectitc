package com.adaptaconsultoria.itradecore.services.gateways;

import lombok.Data;

@Data
public class GatewayReturn {

	private String url;
	private String hash;

	private Object obj;

	private boolean hasError;
	private String message;

}

package com.adaptaconsultoria.itradecore.services.gateways;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.adaptaconsultoria.itradecore.models.EventOrder;
import com.adaptaconsultoria.itradecore.models.Order;
import com.adaptaconsultoria.itradecore.services.gateways.compralo.CompraloInvoice;
import com.adaptaconsultoria.itradecore.services.gateways.compralo.CompraloReturn;
import com.adaptaconsultoria.itradecore.services.util.HttpRequest;
import com.google.gson.Gson;

public class CompraloService extends GatewayAbstract {
	
	private final static String API_URL = "https://app.compralo.io/api/v1";

	@Override
	public void createObject(Object obj) {
		CompraloInvoice invoice = new CompraloInvoice();
		try {
			EventOrder order = (EventOrder) obj;

			invoice.setApiKey(order.getEvent().getGateway().getApiKey());
			invoice.setStoreName(order.getEvent().getName());
			invoice.setValue(order.getAmount());
			invoice.setDescription(order.getTicket());
			invoice.setBackUrl(order.getEvent().getBackUrl());
			invoice.setPostbackUrl(order.getPostbackUrl());

			this.obj = invoice;
			return;
		} catch (Exception e) {
		}

		try {
			Order order = (Order) obj;

			invoice.setApiKey(order.getGateway().getApiKey());
			invoice.setStoreName(order.getCompany().getName());
			invoice.setValue(order.getGrandTotal());
			String desc = "Order: " + order.getId();
			if (StringUtils.isNotBlank(order.getDocumentNo())) {
				desc += " - Doc: " + order.getDocumentNo();
			}
			invoice.setDescription(desc);
			invoice.setBackUrl("javascript();");
			invoice.setPostbackUrl(order.getPostbackUrl());

			this.obj = invoice;
			return;
		} catch (Exception e) {
		}
	}

	@Override
	public GatewayReturn pay() throws Exception {

		GatewayReturn out = new GatewayReturn();

		String apiUrl = API_URL + "/seller/generateInvoice";

		CompraloInvoice invoice = (CompraloInvoice) getObject();

		System.out.println(new Gson().toJson(invoice));

		List<NameValuePair> parameters = new ArrayList<>();

		parameters.add(new BasicNameValuePair("api_key", invoice.getApiKey()));
		parameters.add(new BasicNameValuePair("store_name", invoice.getStoreName()));
		parameters.add(new BasicNameValuePair("value", invoice.getValue().toString()));
		parameters.add(new BasicNameValuePair("description", invoice.getDescription()));
		parameters.add(new BasicNameValuePair("back_url", invoice.getBackUrl()));
		parameters.add(new BasicNameValuePair("postback_url", invoice.getPostbackUrl()));
//		parameters.add(new BasicNameValuePair("postback_url", "http://adi.adaptaconsultoria.com.br:8063/itc/api/eventorder/postback"));

		try {
			String ret = HttpRequest.post(apiUrl, parameters);
			CompraloReturn compraloReturn = new Gson().fromJson(ret, CompraloReturn.class);
			out.setObj(compraloReturn);

			out.setHasError(!compraloReturn.isStatus());
			String msg = "";
			if (StringUtils.isNotBlank(compraloReturn.getMessage())) {
				msg += compraloReturn.getMessage() + "; ";
			}
			if  (out.isHasError()) {
				try {
					for (String str : compraloReturn.getErrors().getValue()) {
						if (StringUtils.isNotBlank(str)) {
							msg += str + "; ";
						}
					}
				} catch (Exception e) {
					msg += "Error not specified!";
				}
			} else {
				out.setUrl(compraloReturn.getCheckout_url());
				out.setHash(compraloReturn.getToken());
			}
			out.setMessage(msg);

		} catch (Exception e) {
			out.setHasError(true);
			out.setMessage(e.getMessage());
		}

		if  (out.isHasError()) {
			throw new Exception(out.getMessage());
		}

		return out;

	}

}

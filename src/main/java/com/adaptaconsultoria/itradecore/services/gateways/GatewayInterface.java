package com.adaptaconsultoria.itradecore.services.gateways;

public interface GatewayInterface {

	void createObject(Object obj);

	Object getObject();

	GatewayReturn pay() throws Exception;

}

package com.adaptaconsultoria.itradecore.services.gateways.compralo;

import lombok.Data;

@Data
public class CompraloErrors {

	private String[] value;

}

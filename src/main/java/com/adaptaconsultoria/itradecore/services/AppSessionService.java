package com.adaptaconsultoria.itradecore.services;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.App;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.User;
import com.adaptaconsultoria.itradecore.repositories.AppSessionRepository;

@Service
public class AppSessionService {

	@Autowired private TokenService tokenService;
	@Autowired private AppSessionRepository appSessionRepository;

	public AppSession save(AppSession appSession) {
		return appSessionRepository.save(appSession);
	}

	public void saveAll(List<AppSession> appSessions) {
		appSessionRepository.saveAll(appSessions);
	}

	public AppSession saveToken(App app, User user, String ipAddress, String serviceName) throws Exception {
		String token = null;
		try {
			token = tokenService.getAccessToken(app, user, ipAddress);
		} catch (Exception e) {
			throw new Exception("Token cannot be created!");
		}

		if (StringUtils.isBlank(token)) {
			throw new Exception("Token cannot be created!");
		}

		AppSession appSession = new AppSession();
		appSession.setCompany(app.getCompany());
		appSession.setApp(app);
		appSession.setUser(user);
		appSession.setIpAddress(ipAddress);
		appSession.setServicename(serviceName);

		appSession.setToken(token);
		appSession.setTokenOriginal(token);
		appSession.setValidto(new Date((new Date()).getTime() + (60000 * 15)));
		appSession.setIsactive(true);

		try {
			appSession = save(appSession);
		} catch (Exception e) {
			throw new Exception("Token cannot be created!");
		}

		return appSession;
	}

	public void closeAllUserSession(User user) {
		List<AppSession> appSessions = appSessionRepository.findByUserAndIsactiveTrue(user);
		for (AppSession appSession : appSessions) {
			appSession.setIsactive(false);
		}
		saveAll(appSessions);
	}

}
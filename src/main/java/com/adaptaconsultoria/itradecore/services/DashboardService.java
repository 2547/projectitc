package com.adaptaconsultoria.itradecore.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.DashboardIndicator;
import com.adaptaconsultoria.itradecore.models.DashboardNetwork;
import com.adaptaconsultoria.itradecore.objects.rest.CurrencyQuote;
import com.adaptaconsultoria.itradecore.repositories.DashboardIndicatorRepository;
import com.adaptaconsultoria.itradecore.repositories.DashboardNetworkRepository;

@Service
public class DashboardService {

	@Autowired private AccountService accountService;
	@Autowired private CurrencyQuoteService currencyQuoteService;
	@Autowired private DashboardNetworkRepository dashboardNetworkRepository;
	@Autowired private DashboardIndicatorRepository dashboardIndicatorRepository;

	public List<DashboardIndicator> getIndicators(AppSession appSession) throws Exception {
		Account account = accountService.getAccount(appSession);
		List<DashboardIndicator> indicators = dashboardIndicatorRepository.getIndicators(account.getId());

		CurrencyQuote quoteBTC = null;
		DashboardIndicator ind = null;

		for (DashboardIndicator indicator : indicators) {
			try {
				if (quoteBTC == null || !ind.getCurrency().equalsIgnoreCase(indicator.getCurrency())) {
					quoteBTC = getBTC(appSession.getCompany(), indicator.getCurrency());
					ind = indicator;
				}
				indicator.setAmountBTC(indicator.getAmount().multiply(quoteBTC.getValue()));
			} catch (Exception e) {
				continue;
			}
		}

		return indicators;
	}

	public List<DashboardNetwork> getNetwork(AppSession appSession) throws Exception {
		Account account = accountService.getAccount(appSession);
		return dashboardNetworkRepository.getNetwork(account.getId());
	}

	private CurrencyQuote getBTC(Company company, String currencyCode) throws Exception {
		List<CurrencyQuote> quotes = currencyQuoteService.getCurrencyQuotes(company, currencyCode, null);
		for (CurrencyQuote quote : quotes) {
			if (quote.getCurrency().equalsIgnoreCase("BTC")) {
				return quote;
			}
		}
		return null;
	}

}

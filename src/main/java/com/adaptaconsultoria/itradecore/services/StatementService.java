package com.adaptaconsultoria.itradecore.services;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Account;
import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Statement;
import com.adaptaconsultoria.itradecore.objects.in.StatementGetIn;
import com.adaptaconsultoria.itradecore.repositories.StatementRepository;
import com.adaptaconsultoria.itradecore.utils.DateUtility;

@Service
public class StatementService {

	@Autowired private AccountService accountService;
	@Autowired private StatementRepository statementRepository;

	public List<Statement> getStatements(StatementGetIn in, AppSession appSession) throws Exception {
		Date startDate = null, endDate = null;
		if (StringUtils.isNotBlank(in.getStartDate())) {
			startDate = DateUtility.lowDateTime(DateUtility.formatDate(in.getStartDate()));
		}
		if (StringUtils.isNotBlank(in.getEndDate())) {
			endDate = DateUtility.lowDateTime(DateUtility.formatDate(in.getEndDate()));
		}
		Account account = accountService.getAccount(appSession);

		if (startDate == null) {
			return statementRepository.getStatements(account.getId());
		}

		if (endDate == null) {
			return statementRepository.getStatementsByStartDate(account.getId(), startDate);
		}

		return statementRepository.getStatementsByStartDateAndEndDate(account.getId(), startDate, endDate);

	}

}

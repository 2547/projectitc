package com.adaptaconsultoria.itradecore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.CompanyCountry;
import com.adaptaconsultoria.itradecore.models.Country;
import com.adaptaconsultoria.itradecore.repositories.CompanyCountryRepository;
import com.adaptaconsultoria.itradecore.repositories.CountryRepository;

@Service
public class CompanyCountryService {

	@Autowired private CountryRepository countryRepository;
	@Autowired private CompanyCountryRepository companyCountryRepository;

	public CompanyCountry getCompanyCountry(Company company, String countryIsoCode) {
		CompanyCountry companyCountry;
		try {
			Country country = countryRepository.findTop1ByCompanyAndIsoCodeIgnoreCaseAndIsactiveTrue(company, countryIsoCode);
			companyCountry = companyCountryRepository.findTop1ByCompanyAndCountryAndIsactiveTrue(company, country);
		} catch (Exception e) {
			companyCountry = null;
		}
		return companyCountry;
	}

}

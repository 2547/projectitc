package com.adaptaconsultoria.itradecore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordEncoderService implements PasswordEncoder {

	@Autowired
	private TokenService tokenService;

	@Override
	public String encode(CharSequence rawPassword) {
		try {
			return tokenService.encrypt(rawPassword.toString());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return tokenService.compare(encodedPassword, rawPassword.toString());
	}

}

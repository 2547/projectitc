package com.adaptaconsultoria.itradecore.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.itradecore.models.AppSession;
import com.adaptaconsultoria.itradecore.models.Company;
import com.adaptaconsultoria.itradecore.models.Currency;
import com.adaptaconsultoria.itradecore.objects.in.CurrencyQuoteGetIn;
import com.adaptaconsultoria.itradecore.objects.pojo.Quote;
import com.adaptaconsultoria.itradecore.objects.pojo.QuoteCompralo;
import com.adaptaconsultoria.itradecore.objects.rest.CurrencyQuote;
import com.adaptaconsultoria.itradecore.repositories.CurrencyRepository;
import com.adaptaconsultoria.itradecore.services.util.HttpRequest;
import com.adaptaconsultoria.itradecore.utils.DateUtility;
import com.adaptaconsultoria.itradecore.utils.JsonUtility;

@Service
public class CurrencyQuoteService {

	@Autowired private CurrencyRepository currencyRepository;

	public List<CurrencyQuote> getCurrencyQuotes(CurrencyQuoteGetIn in, AppSession appSession) throws Exception {
		return getCurrencyQuotes(appSession.getCompany(), in.getCurrency(), in.getValue());
	}

	public List<CurrencyQuote> getCurrencyQuotes(Company company, String currencyCode, BigDecimal value) throws Exception {

		try {
			if (value.compareTo(BigDecimal.ZERO) <= 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			value = BigDecimal.ONE;
		}

		if (StringUtils.isBlank(currencyCode)) {
			throw new Exception("Currency is required!");
		}

		Currency currency;
		try {
			currency = currencyRepository.findByCompanyAndCode(company, currencyCode);
			if (currency == null) {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception("Currency not found!");
		}

		boolean isBRL = currency.getCode().equalsIgnoreCase("BRL");
		boolean isUSD = currency.getCode().equalsIgnoreCase("USD");

		List<CurrencyQuote> quotes = new ArrayList<>();

		CurrencyQuote brl = null;
		if (isBRL || isUSD) {

			try {
				String dtInitial = DateUtility.lowDateTimeToString(DateUtility.subtractDay(new Date(), 7), "MM-dd-yyyy");
				String dtFinal   = DateUtility.lowDateTimeToString(new Date(), "MM-dd-yyyy");

				String ret = HttpRequest.get("https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarPeriodo(dataInicial=@dataInicial,dataFinalCotacao=@dataFinalCotacao)?"
						+ "@dataInicial=%27"      + dtInitial + "%27&"
						+ "@dataFinalCotacao=%27" + dtFinal   + "%27&"
						+ "$top=100&$orderby=dataHoraCotacao%20desc&$format=json");
				Quote qRet = (Quote) JsonUtility.objToObj(JsonUtility.fromJson(ret), new Quote());

				if (isUSD) {
					brl = new CurrencyQuote();
					brl.setValue(value.multiply(qRet.getValue().get(0).getCotacaoVenda()));
					brl.setCurrency("BRL");
					quotes.add(brl);

					CurrencyQuote usd = new CurrencyQuote();
					usd.setValue(value.multiply(BigDecimal.ONE));
					usd.setCurrency("USD");
					quotes.add(usd);
				}
				if (isBRL) {
					brl = new CurrencyQuote();
					brl.setValue(value.multiply(BigDecimal.ONE));
					brl.setCurrency("BRL");
					quotes.add(brl);

					CurrencyQuote usd = new CurrencyQuote();
					usd.setValue(value.setScale(4, BigDecimal.ROUND_HALF_DOWN).divide(qRet.getValue().get(0).getCotacaoVenda(), BigDecimal.ROUND_HALF_DOWN).setScale(4, BigDecimal.ROUND_HALF_DOWN));
					usd.setCurrency("USD");
					quotes.add(usd);
				}

			} catch (Exception e) {
				throw new Exception("Quotation could not be found!");
			}

		}

		if (brl != null) {
			try {

				String ret = HttpRequest.get("https://app.compralo.io/api/v1/seller/getPrice");
				QuoteCompralo qRet = (QuoteCompralo) JsonUtility.objToObj(JsonUtility.fromJson(ret), new QuoteCompralo());

				if (qRet != null) {
					if (qRet.getBTC() != null) {
						CurrencyQuote btc = new CurrencyQuote();
						btc.setValue(brl.getValue().setScale(10, BigDecimal.ROUND_HALF_DOWN).divide(qRet.getBTC(), BigDecimal.ROUND_HALF_DOWN).setScale(10, BigDecimal.ROUND_HALF_DOWN));
						btc.setCurrency("BTC");
						quotes.add(btc);
					}
					if (qRet.getETH() != null) {
						CurrencyQuote eth = new CurrencyQuote();
						eth.setValue(brl.getValue().setScale(10, BigDecimal.ROUND_HALF_DOWN).divide(qRet.getETH(), BigDecimal.ROUND_HALF_DOWN).setScale(10, BigDecimal.ROUND_HALF_DOWN));
						eth.setCurrency("ETH");
						quotes.add(eth);
					}
				}

			} catch (Exception e) {
			}
		}

		return quotes;

	}

}

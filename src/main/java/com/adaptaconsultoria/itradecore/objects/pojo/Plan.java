package com.adaptaconsultoria.itradecore.objects.pojo;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.models.PlanRule;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name;
	
	private String description;

	private boolean isactive;
	
	private List<PlanRule> planRule = new ArrayList<>();

}

package com.adaptaconsultoria.itradecore.objects.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthyCreateUser {

	private String message;
	private Boolean success;
	private AuthyUser user;

}

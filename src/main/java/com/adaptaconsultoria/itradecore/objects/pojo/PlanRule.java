package com.adaptaconsultoria.itradecore.objects.pojo;

import java.io.Serializable;

import lombok.Data;

@Data
public class PlanRule implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Long planId;
	
	private Long bonusId;

	private Long rate;
	
	private Long levelUp;
	
	private String targetdate;
	
	private String deadlineparam;
	
	private Boolean validateactivity;
	
	private Boolean identifylevel;

}
	
	
	
	
package com.adaptaconsultoria.itradecore.objects.pojo;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteCompralo {

	@JsonProperty("BTC")
	private BigDecimal BTC;

	@JsonProperty("ETH")
	private BigDecimal ETH;

}

package com.adaptaconsultoria.itradecore.objects.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.models.ProductLanguage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name;
	
	private String value;

	private String shortDescription;

	private String description;

	private Long currency;
	
	private BigDecimal price;

	private Integer binary = 0;

	private Integer career = 0;

	private Long plan;

	private Boolean isValidActivity = false;

	private Boolean isValidQualify = true;

	private Boolean isRent = false;

	private String bigImg;

	private String smallImg;
	
	private boolean isactive;
	
	private List<ProductLanguage> languages = new ArrayList<>();

}

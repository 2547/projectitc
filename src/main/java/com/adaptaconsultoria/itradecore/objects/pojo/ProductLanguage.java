package com.adaptaconsultoria.itradecore.objects.pojo;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProductLanguage implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String isoLanguage;

	private String productKey;

}

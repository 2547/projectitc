package com.adaptaconsultoria.itradecore.objects.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RegionJson {

	private String code;
	private String name;

	private List<CityJson> cities = new ArrayList<>();

}

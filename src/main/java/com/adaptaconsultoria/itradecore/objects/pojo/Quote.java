package com.adaptaconsultoria.itradecore.objects.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {

	private BigDecimal cotacaoCompra;
	private BigDecimal cotacaoVenda;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dataHoraCotacao;

	private List<Quote> value = new ArrayList<>();

}

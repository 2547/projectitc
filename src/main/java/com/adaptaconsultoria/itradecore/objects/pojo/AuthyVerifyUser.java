package com.adaptaconsultoria.itradecore.objects.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthyVerifyUser {

	private String message;
	private String token;
	private Boolean success;
	private AuthyError errors;
	private String error_code;

}

package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BinaryGetIn extends DefaultIn {

	private String accountNo;
	private String query;
	private Integer level;

}

package com.adaptaconsultoria.itradecore.objects.in;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class EventOrderPostIn {

	private String eventHash;
	private String ticket;
	private String email;
	private Integer qty;
	private String currency;
	private BigDecimal amount;
	private String description;

}

package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CashbackGetIn extends DefaultIn {

	private String dateStart;
	private String dateEnd;

}

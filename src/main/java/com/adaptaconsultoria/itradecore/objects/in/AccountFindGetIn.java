package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountFindGetIn extends DefaultIn {

	private String query;

}

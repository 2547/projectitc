package com.adaptaconsultoria.itradecore.objects.in;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CurrencyQuoteGetIn extends DefaultIn {

	private BigDecimal value;
	private String currency;

}

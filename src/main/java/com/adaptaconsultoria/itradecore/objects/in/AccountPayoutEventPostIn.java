package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPayoutEventPostIn extends DefaultIn {

	private Long accountPayoutCode;
	private String description;

}

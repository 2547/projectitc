package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BinarySidePostIn extends DefaultIn {

	private String side;

}

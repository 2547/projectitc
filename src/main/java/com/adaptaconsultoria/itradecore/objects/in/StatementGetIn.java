package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class StatementGetIn extends DefaultIn {

	private String startDate;
	private String endDate;

}

package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;

@Data
public class DefaultIn {

	private String token;

}

package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class OrderGetIn extends DefaultIn {

	private Long orderCode;

}

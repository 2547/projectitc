package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;

@Data
public class AuthPostIn {
	private String appToken;
	private String appPassword;
	private String login;
	private String userPassword;
}

package com.adaptaconsultoria.itradecore.objects.in;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.OrderLine;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class OrderPostIn extends DefaultIn {

	private Long code;

	private String documentNo;
	private Long gatewayCode;

	private List<OrderLine> lines = new ArrayList<>();

}

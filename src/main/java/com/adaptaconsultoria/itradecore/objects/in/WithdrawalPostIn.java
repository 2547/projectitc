package com.adaptaconsultoria.itradecore.objects.in;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WithdrawalPostIn extends DefaultIn {

	private Long accountPayoutCode;
	private String currency;
	private BigDecimal amount;

}

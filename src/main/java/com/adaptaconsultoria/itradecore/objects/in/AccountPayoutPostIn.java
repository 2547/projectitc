package com.adaptaconsultoria.itradecore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPayoutPostIn extends DefaultIn {

	private Long code;
	private Long gatewayCode;
	private String value;
	private Long code2fa;

}

package com.adaptaconsultoria.itradecore.objects.out;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserLoginGetOut extends DefaultOut {

	private boolean isvalid = false;

}

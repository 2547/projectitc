package com.adaptaconsultoria.itradecore.objects.out;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WithdrawalGetOut extends DefaultOut {

	private BigDecimal fee;
	private Date withdrawalDate;

}

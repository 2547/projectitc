package com.adaptaconsultoria.itradecore.objects.out;

import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;

import lombok.Data;

@Data
public class DefaultOut {

	private String token;

	private boolean hasError = false;
	private DefaultError error;

}

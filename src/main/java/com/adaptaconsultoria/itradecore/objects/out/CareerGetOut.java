package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.BinaryJournal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CareerGetOut extends DefaultOut {

	private Long leftPoints;
	private Long rightPoints;
	private Long leftTotalPoints;
	private Long rightTotalPoints;

	private List<BinaryJournal> journals = new ArrayList<>();

}

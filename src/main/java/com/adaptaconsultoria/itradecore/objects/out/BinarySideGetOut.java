package com.adaptaconsultoria.itradecore.objects.out;

import com.adaptaconsultoria.itradecore.models.enums.BinarySide;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BinarySideGetOut extends DefaultOut {

	private BinarySide side;

}

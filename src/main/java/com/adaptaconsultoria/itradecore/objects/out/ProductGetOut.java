package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.Product;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ProductGetOut extends DefaultOut {

	private List<Product> products = new ArrayList<>();

	public void setProducts(List<com.adaptaconsultoria.itradecore.models.Product> mods) {
		products = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.Product mod : mods) {
			products.add(Product.fromModel(mod));
		}
	}

}

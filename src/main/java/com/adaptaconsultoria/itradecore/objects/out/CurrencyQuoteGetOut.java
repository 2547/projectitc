package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.CurrencyQuote;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CurrencyQuoteGetOut extends DefaultOut {

	private List<CurrencyQuote> quotes = new ArrayList<>();

}

package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.models.DashboardIndicator;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class DashboardGetOut extends DefaultOut {

	private List<DashboardIndicator> indicators = new ArrayList<>();

}

package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.Binary;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BinaryGetOut extends DefaultOut {

	private List<Binary> binaries = new ArrayList<>();

}

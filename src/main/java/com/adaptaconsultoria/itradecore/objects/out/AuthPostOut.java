package com.adaptaconsultoria.itradecore.objects.out;

import com.adaptaconsultoria.itradecore.objects.out.error.AuthPostError;
import com.adaptaconsultoria.itradecore.objects.rest.Account;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true)
public class AuthPostOut {

	private String token;

	private Account account;

	private boolean hasError = false;
	private AuthPostError error;

}

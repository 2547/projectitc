package com.adaptaconsultoria.itradecore.objects.out;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class OrderPostOut extends DefaultOut {

	private Long orderCode;

}

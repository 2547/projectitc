package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.Gateway;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class GatewayGetOut extends DefaultOut {

	private List<Gateway> gateways = new ArrayList<>();

	public void setGateways(List<com.adaptaconsultoria.itradecore.models.Gateway> mods) {
		gateways = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.Gateway mod : mods) {
			gateways.add(Gateway.fromModel(mod));
		}
	}

}

package com.adaptaconsultoria.itradecore.objects.out;

import com.adaptaconsultoria.itradecore.objects.out.error.DefaultError;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true)
public class EventOrderPostOut {

	private String url;

	private boolean hasError = false;
	private DefaultError error;

}

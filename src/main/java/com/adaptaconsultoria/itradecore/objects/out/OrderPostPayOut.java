package com.adaptaconsultoria.itradecore.objects.out;

import com.adaptaconsultoria.itradecore.objects.rest.Order;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class OrderPostPayOut extends DefaultOut {

	private Order order;

}

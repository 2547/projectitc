package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.in.DefaultIn;
import com.adaptaconsultoria.itradecore.objects.rest.Parameter;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class EmailSenderOut extends DefaultIn {

	private String id;
	private String ipAddress;
	private List<String> receivers = new ArrayList<>();
	private List<Parameter> parameters = new ArrayList<>();

}

package com.adaptaconsultoria.itradecore.objects.out;

import com.adaptaconsultoria.itradecore.objects.rest.Career;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class GraduationGetOut extends DefaultOut {

	private Career career;

}

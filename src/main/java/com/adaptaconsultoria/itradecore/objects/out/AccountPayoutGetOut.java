package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.AccountPayout;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPayoutGetOut extends DefaultOut {

	private List<AccountPayout> payouts;

	public void setPayouts(List<com.adaptaconsultoria.itradecore.models.AccountPayout> mods) {
		payouts = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.AccountPayout mod : mods) {
			payouts.add(AccountPayout.fromModel(mod));
		}
	}

}

package com.adaptaconsultoria.itradecore.objects.out;

import java.util.List;

import com.adaptaconsultoria.itradecore.objects.pojo.CountryJson;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class LocalizationGetOut extends DefaultOut {

	private List<CountryJson> countries;
	private String countriesStr;

}

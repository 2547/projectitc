package com.adaptaconsultoria.itradecore.objects.out;

import com.adaptaconsultoria.itradecore.objects.rest.Account;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPostOut extends DefaultOut {

	private boolean didLogin = false;
	private Account account;

}

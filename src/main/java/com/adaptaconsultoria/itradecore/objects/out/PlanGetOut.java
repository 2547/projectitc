package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.Plan;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PlanGetOut extends DefaultOut {

	private List<Plan> plans = new ArrayList<>();

	public void setPlan(List<com.adaptaconsultoria.itradecore.models.Plan> mods) {
		plans = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.Plan mod : mods) {
			plans.add(Plan.fromModel(mod));
		}
	}

}

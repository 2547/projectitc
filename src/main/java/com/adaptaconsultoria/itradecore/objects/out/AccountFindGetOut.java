package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.models.AutoComplete;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountFindGetOut extends DefaultOut {

	private List<AutoComplete> accounts = new ArrayList<>();

}

package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.objects.rest.Currency;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CurrencyGetOut extends DefaultOut {

	private List<Currency> currencies = new ArrayList<>();

	public void setCurrencies(List<com.adaptaconsultoria.itradecore.models.Currency> mods) {
		currencies = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.Currency mod : mods) {
			currencies.add(Currency.fromModel(mod));
		}
	}

}

package com.adaptaconsultoria.itradecore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.itradecore.models.DashboardNetwork;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class DashboardNetworkGetOut extends DefaultOut {

	private List<DashboardNetwork> networks = new ArrayList<>();

}

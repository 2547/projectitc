package com.adaptaconsultoria.itradecore.objects.out.error;

import lombok.Data;

@Data
public class DefaultError {

	private String error;

}

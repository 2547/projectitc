package com.adaptaconsultoria.itradecore.objects.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Product {

	private Long code;
	private String name;
	private String value;
	private String shortDescription;
	private String description;
	private Currency currency;
	private BigDecimal price;
	private Integer binary;
	private Integer career;
	private Plan plan;
	private Boolean isValidActivity;
	private Boolean isValidQualify;
	private Boolean isRent;
	private String bigImg;
	private String smallImg;
	private List<ProductLanguage> languages = new ArrayList<>();

	public static Product fromModel(com.adaptaconsultoria.itradecore.models.Product mod) {
		Product product = new Product();

		try {
			product.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			product.setName(mod.getName());
		} catch (Exception e) {
		}
		try {
			product.setValue(mod.getValue());
		} catch (Exception e) {
		}
		try {
			product.setShortDescription(mod.getShortDescription());
		} catch (Exception e) {
		}
		try {
			product.setDescription(mod.getDescription());
		} catch (Exception e) {
		}
		try {
			product.setCurrency(Currency.fromModel(mod.getCurrency()));
		} catch (Exception e) {
		}
		try {
			product.setPrice(mod.getPrice());
		} catch (Exception e) {
		}
		try {
			product.setBinary(mod.getBinary());
		} catch (Exception e) {
		}
		try {
			product.setCareer(mod.getCareer());
		} catch (Exception e) {
		}
		try {
			product.setPlan(Plan.fromModel(mod.getPlan()));
		} catch (Exception e) {
		}
		try {
			product.setIsValidActivity(mod.getIsValidActivity());
		} catch (Exception e) {
		}
		try {
			product.setIsValidQualify(mod.getIsValidQualify());
		} catch (Exception e) {
		}
		try {
			product.setBigImg(mod.getBigImg());
		} catch (Exception e) {
		}
		try {
			product.setSmallImg(mod.getSmallImg());
		} catch (Exception e) {
		}
		try {
			product.setLanguages(ProductLanguage.fromModels(mod.getLanguages()));
		} catch (Exception e) {
		}

		return product;
	}

}

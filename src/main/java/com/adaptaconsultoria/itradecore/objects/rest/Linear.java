package com.adaptaconsultoria.itradecore.objects.rest;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Linear {

	private Integer level;
	private Account childAccount;

	public static List<Linear> fromModels(List<com.adaptaconsultoria.itradecore.models.LinearTree> mods) {
		List<Linear> binaries = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.LinearTree mod : mods) {
			binaries.add(Linear.fromModel(mod));
		}
		return binaries;
	}

	public static Linear fromModel(com.adaptaconsultoria.itradecore.models.LinearTree mod) {
		if (mod == null) {
			return null;
		}

		Linear binary = new Linear();

		try {
			binary.setLevel(mod.getLevel());
		} catch (Exception e) {
		}

		try {
			binary.setChildAccount(Account.fromModel(mod.getChildAccount()));
		} catch (Exception e) {
		}

		return binary;
	}

}

package com.adaptaconsultoria.itradecore.objects.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Order {

	private Long code;
	private String documentNo;
	private Currency currency;
	private Date dateOrdered;
	private Date dateActivity;
	private Boolean isValid;
	private Boolean isPaid;
	private Gateway gateway;
	private BigDecimal totalLines;
	private BigDecimal fee;
	private BigDecimal grandTotal;
	private String url;
	private String hash;
	private Date deadline;

	private List<OrderLine> lines = new ArrayList<>();

	public static List<Order> fromModels(List<com.adaptaconsultoria.itradecore.models.Order> mods) {
		List<Order> orders = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.Order mod : mods) {
			orders.add(fromModel(mod));
		}
		return orders;
	}
	public static Order fromModel(com.adaptaconsultoria.itradecore.models.Order mod) {
		Order order = new Order();

		try {
			order.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			order.setDocumentNo(mod.getDocumentNo());
		} catch (Exception e) {
		}
		try {
			order.setCurrency(Currency.fromModel(mod.getCurrency()));
		} catch (Exception e) {
		}
		try {
			order.setDateOrdered(mod.getDateOrdered());
		} catch (Exception e) {
		}
		try {
			order.setDateActivity(mod.getDateActivity());
		} catch (Exception e) {
		}
		try {
			order.setIsValid(mod.getIsValid());
		} catch (Exception e) {
		}
		try {
			order.setIsPaid(mod.getIsPaid());
		} catch (Exception e) {
		}
		try {
			order.setGateway(Gateway.fromModel(mod.getGateway()));
		} catch (Exception e) {
		}
		try {
			order.setTotalLines(mod.getTotalLines());
		} catch (Exception e) {
		}
		try {
			order.setFee(mod.getFee());
		} catch (Exception e) {
		}
		try {
			order.setGrandTotal(mod.getGrandTotal());
		} catch (Exception e) {
		}
		try {
			order.setUrl(mod.getUrl());
		} catch (Exception e) {
		}
		try {
			order.setHash(mod.getHash());
		} catch (Exception e) {
		}
		try {
			order.setDeadline(mod.getDeadline());
		} catch (Exception e) {
		}
		try {
			order.setLines(OrderLine.fromModels(mod.getLines()));
		} catch (Exception e) {
		}

		return order;
	}

}

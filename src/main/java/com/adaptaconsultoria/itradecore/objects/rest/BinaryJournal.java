package com.adaptaconsultoria.itradecore.objects.rest;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class BinaryJournal {

	private Long leftPoints;
	private Long rightPoints;
	private String description;

	public static List<BinaryJournal> fromModels(List<com.adaptaconsultoria.itradecore.models.BinaryJournal> mods) {
		List<BinaryJournal> binaries = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.BinaryJournal mod : mods) {
			binaries.add(BinaryJournal.fromModel(mod));
		}
		return binaries;
	}

	public static BinaryJournal fromModel(com.adaptaconsultoria.itradecore.models.BinaryJournal mod) {
		if (mod == null) {
			return null;
		}

		BinaryJournal journal = new BinaryJournal();

		try {
			journal.setLeftPoints(mod.getLeftPoints());
		} catch (Exception e) {
		}

		try {
			journal.setRightPoints(mod.getRightPoints());
		} catch (Exception e) {
		}

		try {
			journal.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		return journal;
	}

}

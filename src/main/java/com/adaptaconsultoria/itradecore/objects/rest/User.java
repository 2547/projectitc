package com.adaptaconsultoria.itradecore.objects.rest;

import java.util.Date;

import lombok.Data;

@Data
public class User {

	private String firstname;
	private String lastname;
	private String name;
	private String email;
	private String login;
	private Long company;
	private boolean isactive;
	private String phone;
	private String role;
	private String gender;
	private Date birthdate;

	private String avatar;

	public static User fromModel(com.adaptaconsultoria.itradecore.models.User mod) {
		if (mod == null) {
			return null;
		}

		User user = new User();

		try {
			user.setFirstname(mod.getFirstname());
		} catch (Exception e) {
		}

		try {
			user.setLastname(mod.getLastname());
		} catch (Exception e) {
		}

		try {
			user.setName(mod.getName());
		} catch (Exception e) {
		}

		try {
			user.setEmail(mod.getEmail());
		} catch (Exception e) {
		}

		try {
			user.setLogin(mod.getLogin());
		} catch (Exception e) {
		}

		try {
			user.setCompany(mod.getCompany().getId());
		} catch (Exception e) {
		}

		try {
			user.setIsactive(mod.getIsactive());
		} catch (Exception e) {
		}

		try {
			user.setPhone(mod.getPhone());
		} catch (Exception e) {
		}

		try {
			user.setGender(mod.getGender());
		} catch (Exception e) {
		}

		try {
			user.setBirthdate(mod.getBirthdate());
		} catch (Exception e) {
		}

		try {
			user.setPhone(mod.getPhone());
		} catch (Exception e) {
		}

		try {
			user.setAvatar("https://adaptadigital.com.br/wp-content/uploads/2019/05/avatar-5.png");
		} catch (Exception e) {
		}

		return user;
	}

}

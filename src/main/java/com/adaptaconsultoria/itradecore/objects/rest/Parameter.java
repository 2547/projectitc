package com.adaptaconsultoria.itradecore.objects.rest;

import lombok.Data;

@Data
public class Parameter {

	private String name;
	private String value;

}

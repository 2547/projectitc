package com.adaptaconsultoria.itradecore.objects.rest;

import lombok.Data;

@Data
public class Token {

	private String token;
	private String error;

}

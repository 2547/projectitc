package com.adaptaconsultoria.itradecore.objects.rest;

import lombok.Data;

@Data
public class Currency {

	private String code;
	private String name;
	private String sign;
	private Integer precision;
	private String icon;

	public static Currency fromModel(com.adaptaconsultoria.itradecore.models.Currency mod) {
		Currency currency = new Currency();

		try {
			currency.setCode(mod.getCode());
		} catch (Exception e) {
		}
		try {
			currency.setName(mod.getName());
		} catch (Exception e) {
		}
		try {
			currency.setSign(mod.getSign());
		} catch (Exception e) {
		}
		try {
			currency.setPrecision(mod.getPrecision());
		} catch (Exception e) {
		}
		try {
			currency.setIcon(mod.getIcon());
		} catch (Exception e) {
		}

		return currency;
	}

}

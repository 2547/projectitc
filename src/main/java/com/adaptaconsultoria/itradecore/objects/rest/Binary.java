package com.adaptaconsultoria.itradecore.objects.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Binary {

	private Account account;
	private Long leftPoints;
	private Long rightPoints;
	private Long leftTotalPoints;
	private Long rightTotalPoints;
	private Long leftDirect;
	private Long rightDirect;
	private Long leftTeam;
	private Long rightTeam;
	private Date positioned;

	public static List<Binary> fromModels(List<com.adaptaconsultoria.itradecore.models.BinaryNetwork> mods) {
		List<Binary> binaries = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.BinaryNetwork mod : mods) {
			binaries.add(Binary.fromModel(mod));
		}
		return binaries;
	}

	public static Binary fromModel(com.adaptaconsultoria.itradecore.models.BinaryNetwork mod) {
		if (mod == null) {
			return null;
		}

		Binary binary = new Binary();

		try {
			binary.setAccount(Account.fromModel(mod.getAccount()));
		} catch (Exception e) {
		}

		try {
			binary.setLeftPoints(mod.getLeftPoints());
		} catch (Exception e) {
		}

		try {
			binary.setRightPoints(mod.getRightPoints());
		} catch (Exception e) {
		}

		try {
			binary.setLeftTotalPoints(mod.getLeftTotalPoints());
		} catch (Exception e) {
		}

		try {
			binary.setRightTotalPoints(mod.getRightTotalPoints());
		} catch (Exception e) {
		}

		try {
			binary.setLeftDirect(mod.getLeftDirect());
		} catch (Exception e) {
		}

		try {
			binary.setRightDirect(mod.getRightDirect());
		} catch (Exception e) {
		}

		try {
			binary.setLeftTeam(mod.getLeftTeam());
		} catch (Exception e) {
		}

		try {
			binary.setRightTeam(mod.getRightTeam());
		} catch (Exception e) {
		}

		try {
			binary.setPositioned(mod.getPositioned());
		} catch (Exception e) {
		}

		return binary;
	}

}

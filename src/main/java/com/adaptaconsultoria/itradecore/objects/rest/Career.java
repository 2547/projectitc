package com.adaptaconsultoria.itradecore.objects.rest;

import lombok.Data;

@Data
public class Career {

	private String name;
	private String activeIcon;
	private String inactiveIcon;
	private String dashboardIcon;

	public static Career fromModel(com.adaptaconsultoria.itradecore.models.Career mod) {
		Career career = new Career();

		try {
			career.setName(mod.getName());
		} catch (Exception e) {
		}

		try {
			career.setActiveIcon(mod.getActiveIcon());
		} catch (Exception e) {
		}

		try {
			career.setInactiveIcon(mod.getInactiveIcon());
		} catch (Exception e) {
		}

		try {
			career.setDashboardIcon(mod.getDashboardIcon());
		} catch (Exception e) {
		}

		return career;
	}

}

package com.adaptaconsultoria.itradecore.objects.rest;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CurrencyQuote {

	private String currency;
	private BigDecimal value;

}

package com.adaptaconsultoria.itradecore.objects.rest;

import lombok.Data;

@Data
public class Plan {

	private Long code;
	private String name;
	private String description;

	public static Plan fromModel(com.adaptaconsultoria.itradecore.models.Plan mod) {
		Plan plan = new Plan();

		try {
			plan.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			plan.setName(mod.getName());
		} catch (Exception e) {
		}
		try {
			plan.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		return plan;
	}

}

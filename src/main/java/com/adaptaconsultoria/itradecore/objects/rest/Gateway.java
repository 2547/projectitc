package com.adaptaconsultoria.itradecore.objects.rest;

import lombok.Data;

@Data
public class Gateway {

	private Long code;
	private String name;
	private String description;
	private Long valueLength;
	private String className;

	public static Gateway fromModel(com.adaptaconsultoria.itradecore.models.Gateway mod) {
		Gateway gateway = new Gateway();

		try {
			gateway.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			gateway.setName(mod.getName());
		} catch (Exception e) {
		}
		try {
			gateway.setDescription(mod.getDescription());
		} catch (Exception e) {
		}
		try {
			gateway.setValueLength(mod.getValueLength());
		} catch (Exception e) {
		}
		try {
			gateway.setClassName(mod.getClassName().toString());
		} catch (Exception e) {
		}

		return gateway;
	}

}

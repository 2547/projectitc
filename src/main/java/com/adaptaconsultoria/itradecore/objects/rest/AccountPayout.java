package com.adaptaconsultoria.itradecore.objects.rest;

import lombok.Data;

@Data
public class AccountPayout {

	private Long code;
	private Gateway gateway;
	private Long gatewayCode;
	private String value;
	private Boolean isBlocked;
	private Boolean isCanceled;

	public static AccountPayout fromModel(com.adaptaconsultoria.itradecore.models.AccountPayout mod) {
		if (mod == null) {
			return null;
		}

		AccountPayout payout = new AccountPayout();

		try {
			payout.setCode(mod.getId());
		} catch (Exception e) {
		}

		try {
			payout.setGateway(Gateway.fromModel(mod.getGateway()));
		} catch (Exception e) {
		}

		try {
			payout.setGatewayCode(mod.getGateway().getId());
		} catch (Exception e) {
		}

		try {
			payout.setValue(mod.getValue());
		} catch (Exception e) {
		}

		try {
			payout.setIsBlocked(mod.getIsBlocked());
		} catch (Exception e) {
		}

		try {
			payout.setIsCanceled(mod.getIsCanceled());
		} catch (Exception e) {
		}

		return payout;
	}

}

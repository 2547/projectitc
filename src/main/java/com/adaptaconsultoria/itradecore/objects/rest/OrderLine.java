package com.adaptaconsultoria.itradecore.objects.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class OrderLine {

	private Long code;
	private Long productCode;
	private Product product;
	private BigDecimal qty;
	private BigDecimal price;
	private BigDecimal lineAmount;
	private String isoLanguage;

	public static List<OrderLine> fromModels(List<com.adaptaconsultoria.itradecore.models.OrderLine> mods) {
		List<OrderLine> lines = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.OrderLine mod : mods) {
			lines.add(fromModel(mod));
		}
		return lines;
	}
	public static OrderLine fromModel(com.adaptaconsultoria.itradecore.models.OrderLine mod) {
		OrderLine line = new OrderLine();

		try {
			line.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			line.setProductCode(mod.getProduct().getId());
		} catch (Exception e) {
		}
		try {
			line.setProduct(Product.fromModel(mod.getProduct()));
		} catch (Exception e) {
		}
		try {
			line.setQty(mod.getQty());
		} catch (Exception e) {
		}
		try {
			line.setLineAmount(mod.getLineAmount());
		} catch (Exception e) {
		}
		try {
			line.setIsoLanguage(mod.getIsoLanguage().toString());
		} catch (Exception e) {
		}

		return line;
	}

}

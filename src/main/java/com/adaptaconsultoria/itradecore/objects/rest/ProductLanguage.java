package com.adaptaconsultoria.itradecore.objects.rest;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ProductLanguage {

	private Long code;
	private String isoLanguage;

	public static List<ProductLanguage> fromModels(List<com.adaptaconsultoria.itradecore.models.ProductLanguage> mods) {
		List<ProductLanguage> languages = new ArrayList<>();
		for (com.adaptaconsultoria.itradecore.models.ProductLanguage mod : mods) {
			languages.add(fromModel(mod));
		}
		return languages;
	}
	public static ProductLanguage fromModel(com.adaptaconsultoria.itradecore.models.ProductLanguage mod) {
		ProductLanguage product = new ProductLanguage();

		try {
			product.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			product.setIsoLanguage(mod.getIsoLanguage().toString());
		} catch (Exception e) {
		}

		return product;
	}

}

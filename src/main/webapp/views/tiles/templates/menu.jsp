<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
	
	<div class="brand-sidebar center-align  hide-on-med-and-up" >
		<h1 class="logo-wrapper ">
			<a href="${pageContext.request.contextPath}/dashboard" class="brand-logo darken-1"> <img src="${pageContext.request.contextPath}/resources/images/logo/logo-itrade.png" alt="Itrade"> <span
				class="logo-text  white-text text-darken-2">iTrade</span>
			</a>
			<a href="#" class="navbar-toggler">
				<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>
	
	<div class="brand-sidebar center-align  hide-on-med-and-down" >
		<h1 class="logo-wrapper indigo darken-4">
			<a href="${pageContext.request.contextPath}/dashboard" class="brand-logo darken-1"> <img src="${pageContext.request.contextPath}/resources/images/logo/logo-itrade.png" alt="Itrade"> <span
				class="logo-text  white-text text-darken-2">iTrade</span>
			</a>
			<a href="#" class="navbar-toggler">
				<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>
	
	
	<ul id="slide-out" class="side-nav fixed leftside-navigation grey lighten-3">
		<li class="no-padding">
			<ul class="collapsible" data-collapsible="accordion">
				<li class="bold" id="dashboard-menu">
					<a href="${pageContext.request.contextPath}/dashboard" class="waves-effect waves-cyan">
						<i class="material-icons">dashboard</i>
						<span class="nav-text">Home</span>
					</a>
				</li>
				<li class="bold" id="accountaccess-menu">
					<a href="${pageContext.request.contextPath}/accountaccess" class="waves-effect waves-cyan">
						<i class="material-icons">verified_user</i>
						<span class="nav-text">Account Access</span>
					</a>
				</li>
				<li class="bold" id="payout-menu">
					<a href="${pageContext.request.contextPath}/gateway/list" class="waves-effect waves-cyan">
						<i class="material-icons">attach_money</i>
						<span class="nav-text">Gateway</span>
					</a>
				</li>
				<li class="bold" id="entity-menu">
					<a href="${pageContext.request.contextPath}/bonus/list" class="waves-effect waves-cyan">
						<i class="material-icons">card_membership</i>
						<span class="nav-text">Bonus</span>
					</a>
				</li>
				<li class="bold" id="entity-menu">
					<a href="${pageContext.request.contextPath}/currency/list" class="waves-effect waves-cyan">
						<i class="material-icons">attach_money</i>
						<span class="nav-text">Currency</span>
					</a>
				</li>
				<li class="bold" id="payout-event-menu">
					<a href="${pageContext.request.contextPath}/payoutevent" class="waves-effect waves-cyan">
						<i class="material-icons">money_off</i>
						<span class="nav-text">Payout Event</span>
					</a>
				</li>
				<li class="bold" id="payout-event-menu">
					<a href="${pageContext.request.contextPath}/product/list" class="waves-effect waves-cyan">
						<i class="material-icons">aspect_ratio</i>
						<span class="nav-text">Product</span>
					</a>
				</li>
				<li class="bold" id="payout-event-menu">
					<a href="${pageContext.request.contextPath}/plan/list" class="waves-effect waves-cyan">
						<i class="material-icons">redeem</i>
						<span class="nav-text">Plan</span>
					</a>
				</li>
				<li class="bold" id="payout-event-menu">
					<a href="${pageContext.request.contextPath}/event/list" class="waves-effect waves-cyan">
						<i class="material-icons">event_available</i>
						<span class="nav-text">Event</span>
					</a>
				</li>
				<li class="bold" id="payout-event-menu">
					<a href="${pageContext.request.contextPath}/order/list" class="waves-effect waves-cyan">
						<i class="material-icons">reorder</i>
						<span class="nav-text">Order</span>
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only"> <i
		class="material-icons">menu</i>
	</a>
</aside>
<script> 
	var id = "${menuId}"; 
	var d = document.getElementById(id);
	d ? (d.className += ' active') : '';
</script>
<!-- END LEFT SIDEBAR NAV-->

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="card-panel">
									<div class="row row-header">
										<div class="col s12 m12 l12 grey-text text-darken-2">
											<h5>${title}</h5>
											<div class="divider"></div>
										</div>
									</div>

									<div class="row">
										<form:form class="col s12" modelAttribute="obj" id="${formId}">

											<div class="row">
												<div class="toolbar col s12">
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}/list"> <i class="material-icons">arrow_back</i>
													</a>
													<!-- <span class="divider"></span> -->
													<button class="btn btn-floating" type="submit">
														<i class="material-icons right">save</i>
													</button>
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}"> <i class="material-icons">add</i>
													</a>
												</div>
											</div>

											<form:input type="hidden" name="id" id="id" path="id" />

											<div class="row" style="margin-top: 5rem;">

												<div class="input-field col s12 l4">
													<form:input id="name" type="text" class="required" name="name" path="name" maxlength="120" />
													<label for="name">Name *</label>
												</div>
												<div class="input-field col s12 l4">
													<form:input id="career" type="number" name="career" path="career" class="required" maxlength="20" />
													<label for="career">Career *</label>
												</div>
												<div class="input-field col s12  l4">
													<label class="label-control active" for="plan">Plan </label> <select name="plan" id="plan">
														<option value="" selected>Not selected</option>
														<c:forEach items="${plans}" var="plan">
															<option <c:if test='${not empty obj && obj.plan == plan.id}'> selected </c:if> value="${plan.id}">${plan.name}</option>
														</c:forEach>
													</select>

												</div>
											</div>

											<div class="row">
												<div class="input-field col s12  l3">
													<form:input id="value" type="text" name="value" path="value" maxlength="20" class="required" />
													<label for="value">Value</label>
												</div>
												<div class="input-field col s12 l3">
													<form:input id="price" type="number" name="price" path="price" class="required" />
													<label for="price">Price</label>
												</div>
												<div class="input-field col s12 l3">

													<label class="label-control active" for="currency">Currency *</label> <select required name="currency" id="currency">
														<option value="" disabled selected>Not selected</option>
														<c:forEach items="${currencies}" var="currency">
															<option <c:if test='${not empty obj && obj.currency == currency.id}'> selected </c:if> value="${currency.id}">${currency.name}</option>
														</c:forEach>
													</select>


												</div>
												<div class="input-field col s12 l3">
													<form:input id="binary" type="number" name="binary" path="binary" class="required" />
													<label for="binary">Binary</label>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12 l6">
													<form:textarea id="description" class="materialize-textarea" type="text" name="description" path="description" />
													<label for="description">Description</label>
												</div>
												<div class="input-field col s12 l6">
													<form:textarea id="shortDescription" class="materialize-textarea" type="text" name="shortDescription" path="shortDescription" />
													<label for="shortDescription">Short Description</label>
												</div>
											</div>

											<div class="row">
												<div class="col s4">
													<br> <input type="checkbox" class="checkbox" id="isValidActivity" name="isValidActivity" <c:if test="${obj.isValidActivity}">checked</c:if> /> <label for="isValidActivity">Valid Activity</label>
												</div>
												<div class="col s4">
													<br> <input type="checkbox" class="checkbox" id="isValidQualify" name="isValidQualify" <c:if test="${obj.isValidQualify}">checked</c:if> /> <label for="isValidQualify">Valid Qualify</label>
												</div>
												<div class="col s4">
													<br> <input type="checkbox" class="checkbox" id="isactive" name="isactive" <c:if test="${obj.isactive}">checked</c:if> /> <label for="isactive">Active</label>
												</div>
											</div>
										</form:form>
									</div>
									<div class="divider" style="margin: 3rem 0px;"></div>
									<table class="striped  responsive-table bordered  brown lighten-4" id="${tableId}" style="margin-top: 5rem; width: 100%;">
									</table>

									<div id="modal1" class="modal">
										<div class="modal-content">
											<h4>Product Language</h4>
											<form id="language-form">
												<div class="row row-header" style="margin-bottom: 20px;"></div>
												<div class="row">
													<div class="input-field col s12 l6">
														<input id="isoLanguage" class="required" type="text" name="isoLanguage" /> <label for="isoLanguage" class="label-control active">Language</label>
													</div>
<!-- 													<div class="input-field col s12 l4"> -->
<!-- 														<label class="label-control active" for="city">Currency</label> <select class="browser-default required active" name="currency" id="currency"> -->
<!-- 															<option value="" disabled selected>Not selected</option> -->
<%-- 															<c:forEach items="${currencies}" var="currency"> --%>
<%-- 																<option value="${currency.id}">${currency.name}</option> --%>
<%-- 															</c:forEach> --%>
<!-- 														</select> -->
<!-- 													</div> -->
													<div class="input-field col s12 l6">
														<input id="productKey" type="text" name="productKey"  class="required active" step="any"> <label for="productKey" class="label-control active">Product Key</label>
													</div>
												</div>
												<br>
											</form>
										</div>
										<div class="modal-footer">
											<a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a> <a href="#!" class="modal-action waves-effect waves-green btn-flat" id="add-percentage">Add</a>
										</div>
									</div>




								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>

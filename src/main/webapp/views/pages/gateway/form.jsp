<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 l12 ">
								<div class="card-panel">

									<div class="row row-header">
										<div class="col s12 m12 l12 grey-text text-darken-2">
											<h5>${title}</h5>
											<div class="divider"></div>
										</div>
									</div>

									<div class="row">
										<form:form class="col s12" modelAttribute="obj" id="${formId}">

											<div class="row" >
												<div class="toolbar col s12">
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}/list">
														<i class="material-icons">arrow_back</i>
													</a>
<!-- 													<span class="divider"></span> -->
													<button class="btn btn-floating" type="submit">
														<i class="material-icons right">save</i>
													</button>
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}">
														<i class="material-icons">add</i>
													</a>
												</div>
											</div>

											<form:input type="hidden" name="id" id="id" path="id" />

											<div class="row" style="margin-top: 5rem;">
												<div class="input-field col s9 l10">
													<form:input id="name" type="text" class="required" name="name" path="name" />
													<label for="name">Name *</label>
												</div>
												<div class="col s3 l2">
													<br>
													<input type="checkbox" class="checkbox" id="isactive" name="isactive" <c:if test="${obj.isactive}">checked</c:if> /> <label for="isactive">Active</label>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12">
													<form:input id="description" type="text" name="description" path="description" />
													<label for="description">Description</label>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s8">
													<form:input id="valueMask" type="text" name="valueMask" path="valueMask" />
													<label for="valueMask">Mask</label>
												</div>
												<div class="input-field col s4">
													<form:input id="valueLength" type="number" name="valueLength" path="valueLength" />
													<label for="valueLength">Length</label>
												</div>
												<div class="input-field col s4">
													<form:input id="className" type="text" name="className" path="className" />
													<label for="className">Class Name</label>
												</div>
												
												<div class="col s3 l2">
													<br>
													<input type="checkbox" class="payIn" id="payIn" name="payIn" <c:if test="${obj.payIn}">checked</c:if> /> <label for="payIn">Pay in</label>
												</div>
												<div class="col s3 l2">
													<br>
													<input type="checkbox" class="payout" id="payout" name="payout" <c:if test="${obj.payout}">checked</c:if> /> <label for="payout">Pay Out</label>
												</div>
												<div class="input-field col s4">
													<form:input id="payInFee" type="number" name="payInFee" path="payInFee" />
													<label for="payInFee">Pay In Fee</label>
												</div>
												<div class="input-field col s4">
													<form:input id="payoutFee" type="number" name="payoutFee" path="payoutFee" />
													<label for="payoutFee">Pay Out Fee</label>
												</div>
												<div class="input-field col s4">
													<form:input id="icon" type="text" name="icon" path="icon" />
													<label for="icon">Icon</label>
												</div>
												<div class="col s3 l2">
													<br>
													<input type="checkbox" class="isdefault" id="isdefault" name="isdefault" <c:if test="${obj.isdefault}">checked</c:if> /> <label for="isdefault">Is Default</label>
												</div>
											</div>

<%-- 											<div class="row">
												<div class="input-field col s12 l6">
													<label class="label-control active" for="remittanceSequence">Sequence</label>
													<select class="select2 browser-default" name="remittanceSequence" id="remittanceSequence" >
														<option value="" selected>Not selected</option>
														<c:forEach items="${docSequences}" var="sequency">
															<option <c:if test='${not empty obj && obj.remittanceSequence.id == sequency.id}'> selected </c:if> value="${sequency.id}">${sequency.increase} - ${sequency.minValue}/${sequency.nextValue}/${sequency.maxValue} - ${sequency.prefix}/${sequency.sufix}</option>
														</c:forEach>
													</select>
												</div>
												<div class="input-field col s12 l6">
													<form:input id="remittanceClassname" type="text" name="remittanceClassname" path="remittanceClassname" />
													<label for="remittanceClassname">Class name</label>
												</div> --%>
											</div>

											<div class="row">
												<div class="input-field col s12 l6">
													<label class="label-control active" for="paymentMethod">Payment Method *</label>
													<select class="select2 browser-default required" name="paymentMethod" id="paymentMethod" >
														<option value="" disabled selected>Not selected</option>
														<c:forEach items="${paymentMethods}" var="paymentMethod">
															<option <c:if test='${not empty obj && obj.paymentMethod.id == paymentMethod.id}'> selected </c:if> value="${paymentMethod.id}">${paymentMethod.name}</option>
														</c:forEach>
													</select>
												</div>
												<%-- <div class="input-field col s12 l6">
													<label class="label-control active" for="partner">Partner *</label>
													<select class="browser-default required" name="partner" id="partner" >
														<option value="" disabled selected>Not selected</option>
														<c:if test='${not empty obj && not empty obj.partner}'>
															<option selected value="${obj.partner.id}">${obj.partner.name}</option>
														</c:if> 
													</select>
												</div> --%>
											</div>

										</form:form>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>

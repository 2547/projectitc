<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="card-panel">
									<div class="row row-header">
										<div class="col s12 m12 l12 grey-text text-darken-2">
											<h5>${title}</h5>
											<div class="divider"></div>
										</div>
									</div>

									<div class="row">
										<form:form class="col s12" modelAttribute="obj" id="${formId}">
											<div class="row">
												<div class="toolbar col s12">
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}/list"> <i class="material-icons">arrow_back</i>
													</a>
													<!-- <span class="divider"></span> -->
													<button class="btn btn-floating" type="submit">
														<i class="material-icons right">save</i>
													</button>
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}"> <i class="material-icons">add</i>
													</a>
												</div>
											</div>

											<form:input type="hidden" name="id" id="id" path="id" />

											<div class="row" style="margin-top: 5rem;">

												<div class="input-field col s12 l9">
													<form:input id="name" type="text" class="required" name="name" path="name" maxlength="120" />
													<label for="name">Name *</label>
												</div>
												<div class="col s6 l3">
													<br> <input type="checkbox" class="checkbox" id="isactive" name="isactive" <c:if test="${obj.isactive}">checked</c:if> /> <label for="isactive">Active</label>
												</div>
											</div>
											<div class="row">
												<div class="input-field col s12 l12">
													<form:textarea id="description" class="materialize-textarea" type="text" name="description" path="description" />
													<label for="description">Description</label>
												</div>
											</div>


											<!-- 												<div class="input-field col s12  l4"> -->
											<!-- 													<label class="label-control active" for="plan">Plan </label> <select name="plan" id="plan"> -->
											<!-- 														<option value="" selected>Not selected</option> -->
											<%-- 														<c:forEach items="${plans}" var="plan"> --%>
											<%-- 															<option <c:if test='${not empty obj && obj.plan == plan.id}'> selected </c:if> value="${plan.id}">${plan.name}</option> --%>
											<%-- 														</c:forEach> --%>
											<!-- 													</select> -->

											<!-- 												</div> -->




										</form:form>
									</div>
									<div class="divider" style="margin: 3rem 0px;"></div>
									<table class="striped  responsive-table bordered  brown lighten-4" id="${tableId}" style="margin-top: 5rem; width: 100%;">
									</table>

									<div id="modal1" class="modal">
										<div class="modal-content">
											<h4>Plan Rule</h4>
											<form id="plan-rule-form">
												<div class="row row-header" style="margin-bottom: 20px;"></div>
												<div class="row">
													<div class="input-field col s12 l6">
														<input id="planId" class="required" type="text" name="planId" /> <label for="planId" class="label-control active">Plan ID</label>
													</div>
													<div class="input-field col s12  l6">
														<label class="label-control active" for="bonus">Bonus </label> <select name="bonus" id="bonus" required>
															<option value="" selected>Not selected</option>
															<c:forEach items="${bonuss}" var="bonus">
																<option  value="${bonus.id}">${bonus.name}</option>
															</c:forEach>
														</select>

													</div>

													<div class="input-field col s12 l6">
														<input id="rate" class="required" type="text" name="rate" /> <label for="rate" class="label-control active">Rate</label>
													</div>
													<div class="input-field col s12 l6">
														<input id="levelUp" class="required" type="text" name="levelUp" /> <label for="levelUp" class="label-control active">Level Up</label>
													</div>

													<div class="input-field col s12 l6">

														<label class="label-control active" for="targetdate">Target Date </label> <select name="targetdate" id="targetdate">
															<option value="" disabled selected>Not selected</option>
															<c:forEach items="${targetdates}" var="targetdate">
																<option value="${targetdate}">${targetdate.description}</option>
															</c:forEach>
														</select>
													</div>
													<div class="input-field col s12 l6">
														<input id="deadlineParam" class="required" type="text" name="deadlineParam" /> <label for="deadlineParam" class="label-control active">Deadline Param</label>
													</div>


													<div class="col s6 l3">
														<br> <input type="checkbox" class="checkbox" id="isactive-rule" name="isactive-rule"> <label for="isactive-rule">Active</label>
													</div>
													<!-- 													<div class="col s6 l3"> -->
													<%-- 														<br> <input type="checkbox" class="checkbox" id="validateActivity" name="validateActivity" <c:if test="${obj.validateActivity}">checked</c:if> /> <label for="validateActivity">Validate Activity</label> --%>
													<!-- 													</div> -->
													<!-- 													<div class="col s6 l3"> -->
													<%-- 														<br> <input type="checkbox" class="checkbox" id="identifylevel" name="identifylevel" <c:if test="${obj.identifylevel}">checked</c:if> /> <label for="identifylevel">Identify Level</label> --%>
													<!-- 													</div> -->
													<!-- 													<div class="input-field col s12 l4"> -->
													<!-- 														<label class="label-control active" for="city">Currency</label> <select class="browser-default required active" name="currency" id="currency"> -->
													<!-- 															<option value="" disabled selected>Not selected</option> -->
													<%-- 															<c:forEach items="${currencies}" var="currency"> --%>
													<%-- 																<option value="${currency.id}">${currency.name}</option> --%>
													<%-- 															</c:forEach> --%>
													<!-- 														</select> -->
													<!-- 													</div> -->

												</div>
												<br>
											</form>
										</div>
										<div class="modal-footer">
											<a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a> <a href="#!" class="modal-action waves-effect waves-green btn-flat" id="add-percentage">Add</a>
										</div>
									</div>




								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>

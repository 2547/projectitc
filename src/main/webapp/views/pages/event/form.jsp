<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 l12 ">
								<div class="card-panel">

									<div class="row row-header">
										<div class="col s12 m12 l12 grey-text text-darken-2">
											<h5>${title}</h5>
											<div class="divider"></div>
										</div>
									</div>

									<div class="row">
										<form:form class="col s12" modelAttribute="obj" id="${formId}">

											<div class="row">
												<div class="toolbar col s12">
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}/list"> <i class="material-icons">arrow_back</i>
													</a>
													<!-- 													<span class="divider"></span> -->
													<button class="btn btn-floating" type="submit">
														<i class="material-icons right">save</i>
													</button>
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}"> <i class="material-icons">add</i>
													</a>
												</div>
											</div>

											<form:input type="hidden" name="id" id="id" path="id" />

											<div class="row" style="margin-top: 5rem;">

												<div class="input-field col s9 l4">
													<form:input id="name" type="text" class="required" name="name" path="name" maxlength="80" />
													<label for="name">Name *</label>
												</div>
												<div class="input-field col s9 l4">
													<form:input id="saleUntil" type="date" data-date-format="DD MMMM YYYY" class="required" name="saleUntil" path="saleUntil" maxlength="80" />
													<label for="saleUntil"> </label>
												</div>
												<div class="input-field col s12 l4">

													<label class="label-control active" for="gateway">Gateway </label> <select name="gateway" id="gateway">
														<option value="" disabled selected>Not selected</option>
														<c:forEach items="${gateways}" var="gateway">
															<option <c:if test='${not empty obj && obj.gateway.id == gateway.id}'> selected </c:if> value="${gateway.id}">${gateway.name}</option>
														</c:forEach>
													</select>
												</div>
												<div class="input-field col s9 l3">
													<form:input id="welcomeMsgToken" type="number" class="welcomeMsgToken" name="welcomeMsgToken" path="welcomeMsgToken" maxlength="80" />
													<label for="welcomeMsgToken">welcome Msg Token </label>
												</div>
												<div class="input-field col s9 l3">
													<form:input id="confirmationMsgToken" type="number" class="confirmationMsgToken" name="confirmationMsgToken" path="confirmationMsgToken" maxlength="80" />
													<label for="confirmationMsgToken">Confirmation Msg Token </label>
												</div>
												<div class="input-field col s12 l6">
													<form:input id="hash" type="text" class="hash" name="hash" path="hash" maxlength="80" />
													<label for="hash">Hash</label>
												</div>
												<div class="col s12 l2">
													<br> <input type="checkbox" class="checkbox" id="isactive" name="isactive" <c:if test="${obj.isactive}">checked</c:if> /> <label for="isactive">Active</label>
												</div>

												<div class="input-field col s12">
													<form:input id="description" type="text" name="description" path="description" />
													<label for="description">Description</label>
												</div>
												<div class="input-field col s12">
													<form:input id="backUrl" type="text" name="backUrl" path="backUrl" maxlength="255"/>
													<label for="backUrl">Back URL</label>
												</div>
											</div>
										</form:form>
									</div>
								</div>
								
									<div id="profile-card" class="card col s12 l12">
										<div class="card-content">
											<h5>Paid</h5>
											<div class="divider"></div>

											<div id="table-datatables">
												<table class="striped  responsive-table bordered  brown lighten-4" id="eventOrderTrue" style="width: 100%;">
												</table>
											</div>
										</div>
									</div>
								
								
								<div id="profile-card" class="card col s12 l12">
									<div class="card-content">
										<h5>Not Paid</h5>
										<div class="divider"></div>

										<div id="table-datatables">

											<table class="striped  responsive-table bordered  brown lighten-4" id="eventOrderFalse" style="width: 100%;">
											</table>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script> --%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>

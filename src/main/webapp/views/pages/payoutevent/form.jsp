<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row" >
							<div class="col s12 m12 l12 ">
								<div class="card-panel" >

									<div class="row row-header">
										<div class="col s12 m12 l12 grey-text text-darken-2">
											<h5>${title}</h5>
											<div class="divider"></div>
										</div>
									</div>

									<div class="row">
										<form:form class="col s12" modelAttribute="obj" id="${formId}">

											<div class="row">
												<div class="toolbar col s12">
													<button class="btn btn-floating" type="submit">
														<i class="material-icons right">save</i>
													</button>
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}">
														<i class="material-icons">add</i>
													</a>
												</div>
											</div>

											<div class="row" style="margin-top: 5rem;">
												<div class="input-field col s12 l6">
													<label class="label-control active" for="account">Account *</label>
													<select class="select2 browser-default required" id="account" >
														<option value="" disabled selected>Not selected</option>
													</select>
												</div>
												<div class="input-field col s12 l6">
													<label class="label-control active" for="accountPayout">Account Payout *</label>
													<select class="browser-default required" name="accountPayout" id="accountPayout" >
														<option value="" disabled selected>Not selected</option>
													</select>
												</div>
												<div class="input-field col s12 l8">
													<form:input id="description" type="text" class="required" name="description" path="description" />
													<label for="description">Description *</label>
												</div>
												<div class="input-field col s12 l4">
													<label class="label-control active" for="eventType">Event Type *</label>
													<select class="browser-default required" name="eventType" id="eventType" >
														<option value="" disabled selected>Not selected</option>
														<c:forEach items="${eventTypes}" var="eventType">
															<option value="${eventType}">${eventType.description}</option>
														</c:forEach>
													</select>
												</div>
											</div>

										</form:form>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>

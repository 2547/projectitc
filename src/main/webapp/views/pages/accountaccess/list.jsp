<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="row">
					<div class="col s12">
						<div id="profile-card" class="card card-tabs ">
							<div class="card-content">
								<div class="card-title  hide-on-med-and-up center-align grey-text text-darken-2">
									<h5>${title}</h5>
								
								</div>
								<div class="card-title hide-on-small-only grey-text text-darken-2" style="margin-bottom: -65px;">
									<h5>${title}</h5>
									<!-- 									<div class="divider"></div> -->
								</div>
								<div id="table-datatables" class="active">
									<h4 class="header">${tableTitle}</h4>
									<table class=" striped  responsive-table bordered  brown lighten-4" id="${tableId}">
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>
</body>
</html>
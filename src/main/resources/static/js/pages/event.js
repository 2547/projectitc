var getEventUrlTrue  = contextPath + uri + "/geteventordertrue";
var getEventUrlFalse  = contextPath + uri + "/geteventorderfalse";

var eventId;
var falseTable;
var trueTable;

function getDataTrue(){
	return new Promise(function(resolve, reject) {
		$.ajax({
			 method: "GET",
			  url: getEventUrlTrue,
			  data: { 
				  eventId: eventId
			  },
			  success: (obj) => {
				  resolve(obj)
			  } 
		})
	});
}

function getDataFalse(){
	return new Promise(function(resolve, reject) {
		$.ajax({
			 method: "GET",
			  url: getEventUrlFalse,
			  data: { 
				  eventId: eventId
			  },
			  success: (obj) => {
				  resolve(obj)
			  } 
		})
	});
}

async function startTables() {

	let falseArrays = await getDataFalse();
	let trueArrays = await getDataTrue();

	eventOrderFalseTable(falseArrays);
	eventOrderTrueTable(trueArrays);
}

function eventOrderFalseTable(falseArrays) {
	falseTable = $("#eventOrderFalse")
    .DataTable({
    	data: falseArrays,
		language: {
				"sLengthMenu": ""
		},
        columns: getColumns(),
        columnDefs: getColumnDefs(),
        dom: 'lBfrtip',
        select: true,
        order: [
            [1, 'asc']
        ]
    });
}

function eventOrderTrueTable(trueArrays) {
	trueTable = $("#eventOrderTrue")
    .DataTable({
    	data: trueArrays,
		language: {
				"sLengthMenu": ""
		},
        columns: getColumns(),
        columnDefs: getColumnDefs(),
        dom: 'lBfrtip',
        select: true,
        order: [
            [1, 'asc']
        ]
    });
}
function getColumns() {
	 return [{
		         title: "id",
		         data: "id"
		     }, 
		     {
		         title: "Ticket",
		         data: "ticket"
		     },
		     {
		         title: "qty",
		         data: "qty"
		     },  
		     {
		         title: "Amount",
		         data: "amount",
		         render: function ( data, type, full ) {
			 		   if (full.currency != undefined || full.currency != null)
							return data.toFixed﻿(2)﻿ + ' / ' + full.currency.code;
			 		   return data;
					}
		     },  
		     {
		         title: "Description",
		         data: "description"
		     },  
		     {
		         title: "Email",
		         data: "email"
		     }

];
}

function getColumnDefs() {
	return [{
       targets: [0],
       visible: false
   },
   {
       className: "alinhar-direita mr-4",
       targets: [3]
   }];
}

$(document).ready(function() {
	validate();
	select2Initializer();
	// table();
	eventId = getParamUrl().id;
	startTables()
});

function save() {
	$("#isactive").val( Boolean( $("#isactive").prop("checked") ) );
	$.ajax({
		type : "POST",
		data : $(formId).serializeObject(),
		url : contextPath + uri,
		success : function(obj) {
			if (obj.sucesso) {
				
				if (!obj.isEditing) {
					swal({
					    title: obj.message,
					    text: "Would like to keep registering?",
					    icon: "success",
					    buttons: {
			                cancel: {
			                    text: "List",
			                    value: null,
			                    visible: true,
			                    className: "",
			                    closeModal: true,
			                },
			                confirm: {
			                    text: "Yes",
			                    value: true,
			                    visible: true,
			                    className: "",
			                    closeModal: true
			                }
					    }
					})
					.then((isConfirm) => {
					    if (isConfirm) {
					    	window.location.href = contextPath + uri;
					    } else {
					    	window.location.href = contextPath + uri + "/list";
					    }
					});
					
				} else {
					$("#version").val(obj.obj.version);
					swal({
					    title: obj.message,
					    text: "Continue to edit?",
					    icon: "success",
					    buttons: {
			                cancel: {
			                    text: "Exit",
			                    value: null,
			                    visible: true,
			                    className: "",
			                    closeModal: true,
			                },
			                confirm: {
			                    text: "Continue",
			                    value: true,
			                    visible: true,
			                    className: "",
			                    closeModal: true
			                }
					    }
					})
					.then((isConfirm) => {
					    if (isConfirm) {
					    	
					    } else {
					    	window.location.href = contextPath + uri;
					    }
					});
					
				}
			} else {
				swal("Canceled", obj.message, "error");
				return 0;
			}
		}
	})
	return false;
}

function validate() {
	$(formId).validate({
		submitHandler : function(form) {
			save();
		},
		rules: {},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				if ($(element)[0].tagName === 'SELECT') {
					$(element).parent().append(error);
				} else {
					error.insertAfter(element);
				}
			}
		}
	});

}

function select2Initializer() {
}

var table;
var len = 10;

$(document).ready(function() {
	openTable()
})

function openTable() {
	table() 
}

function table() {
    table = $(tableId)
        .DataTable({
        	ajax: {
				type: "GET",
				url: contextPath + uri + "/getlist",
				dataSrc:""
			},
			language: {
					"sLengthMenu": ""
			},
            columns: getColumns(),
            buttons: [],
            columnDefs: getColumnDefs(),
            select: true,
            order: [
                [1, 'asc']
            ]
        });
}

function getColumns() {
	 return [{
		         title: "id",
		         data: "id"
		     }, {
		         title: "Account No",
		         data: "accountNo"
		     }, {
		         title: "Tax id",
		         data: "taxid"
			 }, { 
				 	title: "Name",
				 	data: "name",
			 		render: function ( data, type, full ) {
			 		   if (full.user != undefined || full.user != null)
							return full.user.name;
						else return '';
					}
			 }, { 
				 	title: "Login",
				 	data: "login",
			 		render: function ( data, type, full ) {
			 		   if (full.user != undefined || full.user != null)
							return full.user.login;
						else return '';
					}
			 }, { 
				 	title: "Phone",
				 	data: "phone",
			 		render: function ( data, type, full ) {
			 		   if (full.user != undefined || full.user != null)
							return full.user.phone;
						else return '';
					}
			 }, { 
				 	title: "Access",
				 	data: "access",
			 		render: function ( data, type, full ) {
						return ''
							+ '<a class="btn btn-floating" rel="noopener noreferrer" target="_blank" href="' + contextPath + uri + '/redirect?id=' + full.id + '">'
			 		   			+ '<i class="material-icons">queue_play_next</i>'
							+ '</a>';
					}
	     }];
}

function getColumnDefs() {
	return [{
        targets: [0],
        visible: false
    }];
}

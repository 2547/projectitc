var getPlansUrl  = contextPath + uri + "/getplanRule";

var saveUrl  = contextPath + uri;

var palntableId;
var id = 0;
var planModal = $('.modal').modal();
var addPercentageButton = $('#add-percentage');
var isEditingRules = false;
var sellerPercentageForm = $('#plan-rule-form');
var planId = $("#planId");
var bonusId = $("#bonusId");
var rate = $("#rate");
var levelUp = $("#levelUp");
var targetdate = $("#targetdate");
var deadlineParam = $("#deadlineParam");
var isactive = $("#isactive");
var currentRules;
var rowIndex;
var table;
var isEditingRules = false;

$(document).ready(function() {
	console.log(tableId)
	validate();
	// select2Initializer();
	palntableId = getParamUrl().id;
	table();
	addPercentageClick();
});

function table() {
    table = $(tableId).DataTable({
            ajax: {
                type: "GET",
                data: {
                	'palntableId': palntableId
                },
                url: getPlansUrl,
                dataSrc: ""
            },
            language: {
                "sLengthMenu": ""
            },
            columns: getColumns(),
            buttons: getButtons(),
            columnDefs: getColumnDefs(),
            initComplete: function(settings, json) {
              selectTableConfig(this.DataTable());
            },
            searching: false,
            dom: 'lBfrtip',
            select: true,
            order: [
                [1, 'asc']
            ]
        });
}

function addPercentageClick() {
	addPercentageButton.click(() => {
		if (!isEditingRules) {
			addPercentage()
		} else {
			edition(currentRules)
		}
	})
}

function validate() {
	$(formId).validate({
		submitHandler : function(form) {
			save();
		},
		ignore: [],
		rules: {
			rule: {
				percentages: true
			}
		},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				if ($(element)[0].tagName === 'SELECT') {
					$(element).parent().append(error);
				} else {
					error.insertAfter(element);
				}
			}
		}
	});
	
	sellerPercentageForm.validate({
		rules: {
			pcCashback: {
				number: true
			}
		},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				if ($(element)[0].tagName === 'SELECT') {
					$(element).parent().append(error);
				} else {
					error.insertAfter(element);
				}
			}
		}
	});
}

function addPercentage() {
	//bonusName = $("#currency option:selected").text();
	if (sellerPercentageForm.valid()) {
		table.row.add( {
			"id": '',
			"planId": planId.val(),
			"bonusId": bonusId.val(),
			"rate": rate.val(),
			"levelUp": levelUp.val(),
			"targetdate": targetdate.val(),
			"deadlineParam": deadlineParam.val(),
			"isactive": Boolean( $("#isactive-rule").prop("checked") ),
	    } ).draw();
		planModal.modal('close');
		clearForm()
	}
	return 0;
}
function clearForm() {
	planId.val("");
	bonusId.val("");
	rate.val("");
	levelUp.val("");
	targetdate.val("");
	deadlineParam.val("");
	isactive.val("");
}

 function getPlans() {
  var credenciadoPercentualDoacaos = [];
  table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
  var data = this.data();
  data.planId = data.planId;
  data.bonusId = data.bonusId;
  data.rate = data.rate;
  data.levelUp = data.levelUp;
  data.targetdate = data.targetdate;
  data.deadlineParam = data.deadlineParam;
  data.isactive = data.isactive;
  credenciadoPercentualDoacaos.push(data);
 } );
  return credenciadoPercentualDoacaos;
 }

function selectDeselectTable(table) {
	var selectedRows = table.rows( { selected: true } ).count();
    table.buttons( [1, 2] ).enable( selectedRows > 0 );
}


function selectTableConfig(table) {
	table.on( 'select', function () {
		selectDeselectTable(table)
	});
	
	table.on( 'deselect', function () {
		clearForm();
// $('#currency').val('').trigger('change');
		selectDeselectTable(table)
	});
	
	$(tableId + ' tbody').on( 'dblclick', 'tr', function() {
		table.rows(this).select()
		rowIndex = table.row( this ).index();
		var obj = table.row({
			selected : true
		}).data();
		isEditingRules = true;
		currentRules = obj;
		planModal.modal('open');
		tableClick(obj)
    	$('#plan-rule-form .input-field label').addClass('active');
	});

	
}

// function setValueSelect2(data) {
// var option = data.id;
// // currency.val(option).trigger('change')
// }

function tableClick(data) {
// obj = data.currency;
	data.id ? id = data.id : id = ''; 
	// currency.val(data.currency)
// setValueSelect2(obj)
	
	planId.val(data.planId);
	bonusId.val(data.bonusId);
	rate.val(data.rate);
	levelUp.val(data.levelUp);
	targetdate.val(data.targetdate);
	deadlineParam.val(data.deadlineParam);
	isactive.val(data.isactive);
	
	return 0;
}

function remove(obj) {
	return new Promise((resolve, reject) => {
		var idPercentage = obj.id;
		if (rowIndex !== '' && rowIndex !== null && idPercentage !== '' && idPercentage !== null) {
			removePercentage(idPercentage, resolve);
		} else {
			table.row(rowIndex).remove().draw();
			clearForm()
			resolve(true)
		}
	});
}

function request(data, url) {
	return fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: data,
    })

}

function save() {
	$("#isactive").val( Boolean( $("#isactive").prop("checked") ) );
	$("#isactive-rule").val( Boolean( $("#isactive-rule").prop("checked") ) );

	var obj = $(formId).serializeObject();
	var obj2 = getPlans();
	debugger
	var object = $.extend(obj, {
		planRule: obj2
	});
	

	
	
	var data = JSON.stringify(object);
	request(data, saveUrl).then(function(response) {
	    return response.json();
	  }).then(function(data) {
	   console.log(data); 
	   
	  if (data.sucesso == true) {
		  swal("Success!", "Saved successfully.", "success");
			} else {
				swal("Unexpected error! Sorry, an error has occurred, please try again.", "error");
			}
	 
	  });
}

function getItems() {
	var items = [];
	table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
		  var data = this.data();
		  items.push(data);
	} );
	
	var mappedItems = items.map(c => {
		return {
			id: c.id,
			planId: c.planId,
			bonusId: c.bonusId,
			rate: c.rate,
			levelUp: c.levelUp,
			targetdate: c.targetdate,
			deadlineParam: c.deadlineParam,
			isactive: c.isactive

			// currency: {
			// id: c.currency.id,
			// name: c.currency.name
			// }
		}
	});
	return {
		rules: mappedItems 
	};
}

function edition(data) {
// currencyName = $("#currency option:selected").text();
	if (sellerPercentageForm.valid()) {
		table.row(rowIndex).data({
			
// "currency": {
// id: currency.val(),
// name: currencyName
// },
			"id": '',
			"planId": planId.val(),
			"bonusId": bonusId.val(),
			"rate": rate.val(),
			"levelUp": levelUp.val(),
			"targetdate": targetdate.val(),
			"deadlineParam": deadlineParam.val(),
			"isactive": isactive.val()
	    }).draw()
	    planModal.modal('close');
	}
}


function saveAwait() {
	$.ajax({
		type : "POST",
		data : $(formId).serializeObject(),
		url : contextPath + uri,
		success : function(obj) {
			saveItems(getItems(), obj.obj);
		}
	})
	return false;
}

function getParamUrl() {
	var queries = {};
	$.each(document.location.search.substr(1).split('&'), function(c, q) {
		var i = q.split('=');
		try {
			queries[i[0].toString()] = i[1].toString();
		}catch {
			return null;
		}
	});
	return queries;
}

function getButtons() {
    return [{
        text: '<i class="material-icons left">add</i></a>',
        className: 'btn btn-floating',
        action: function(e, dt, node, config) {
// clearForm()
// isEditingRules = false;
// currency.val('').trigger('change');
//           
            planModal.modal('open');
        },
        enabled: true
    }, {
        text: '<i class="material-icons left">edit</i></a>',
        className: 'btn btn-floating',
        attr: {
            'data-position': 'top',
            'data-delay': '50',
            'data-tooltip': 'Limpar Formulário'
        },
        action: function(e, dt, node, config) {
        	
            var obj = dt.row({
                selected: true
            }).data();
            
            rowIndex = dt.row({
                selected: true
            }).index()
            
            isEditingRules = true;
            tableClick(obj)
            currentRules = obj;
            planModal.modal('open');
        	$('#plan-rule-form .input-field label').addClass('active');
        },
        enabled: false
    }, {
        text: '<i class="material-icons left ">clear</i>',
        className: 'btn btn-floating red',
        action: function(e, dt, node, config) {
            var o = dt.row({
                selected: true
            }).data();
            remove(o).then(r => {
            	selectDeselectTable(table)
            })
        },
        enabled: false
    }];
}

function getColumns() {
	
	 return [{
		         title: "id",
		         data: "id"
		     },
		     {
		         title: "Bonus Id",
		         data: "bonusId"
		     },  
		     {
		         title: "Rate",
		         data: "rate"
		     },  
		     {
		         title: "Level Up",
		         data: "levelUp"
		     },  
		     {
		         title: "Targets Date",
		         data: "targetdate"
		     }
		     ,  
		     {
		         title: "Dead Line Param",
		         data: "deadlineParam"
		     }

];
}

function getColumnDefs() {
	return [{
        targets: [0],
        visible: false
    }];
}

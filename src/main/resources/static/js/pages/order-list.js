var table;
var len = 10;

$(document).ready(function() {
	openTable()
})

function openTable() {
	table() 
}

function table() {
	
    table = $(tableId)
        .DataTable({
        	ajax: {
				type: "GET",
				url: contextPath + uri + "/getlist",
				dataSrc:""
			},
			language: {
					"sLengthMenu": ""
			},
            columns: getColumns(),
            columnDefs: getColumnDefs(),
//            initComplete: function(settings, json) {
//              selectTableConfig(this.DataTable());
//            },
            dom: 'lBfrtip',
            select: true,
            order: [
                [1, 'asc']
            ]
        });
}

function getTableId(table) {
	return '#'+table.tables().nodes().to$().attr('id');
}

function getLen() {
	return len;
}

function selectDeselectTable(table) {
	var selectedRows = table.rows( { selected: true } ).count();
	table.buttons([1,2]).enable( selectedRows == 1 );
   
}

function promptRemove(id) {
	swal({
	    title: "Remove",
	    text: "TemAre you sure you want to remove this record?",
	    icon: "warning",
	    buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
	    }
	})
	.then((isConfirm) => {
	    if (isConfirm) {
	    	remove(id)
	    } else {
	    	swal("Canceled", "Record removed!", "error");
	    }
	});
}


function getColumns() {
		 
		return [{
	         title: "ID",
	         data: "id"
	     },{
	         title: "Currency",
	         data: "currency",     	 
	         render: function ( data, type, full ) {
	 		   if (full.currency != undefined || full.currency != null)
					return full.currency.code;
				else return '';
			}
	     },
	     
	     {
	         title: "DocumentNo",
	         data: "documentNo"
	     }, {
	         title: "Total Lines",
	         data: "totalLines"
	     },
	     {
	         title: "Grand Total",
	         data: "grandTotal"
	     },
	     {
	         title: "Pay",
	         
	         render: function(data, type, full) {
	        	 return gatePaymentButton(full.id);
            }
	     }];
	
}



function gatePaymentButton(orderId) {
	return `<button class="btn btn-floating blue darken-1" onclick="payByUser(${orderId})"> 
				<i class="material-icons">payment</i> Pay
			</button>`;
}

function payByUser(orderId) {
	 swal({
		    title: "Paid",
		    text: "Are you sure you want to pay?",
		    icon: "warning",
		    buttons: {
	            cancel: {
	                text: "No",
	                value: null,
	                visible: true,
	                className: "",
	                closeModal: true,
	            },
	            confirm: {
	                text: "Yes",
	                value: true,
	                visible: true,
	                className: "",
	                closeModal: true
	            }
		    }
		})
		.then((confirm ) => {
			if (confirm == true) {
			$.ajax({
				type: "POST",
				url: contextPath + uri + "/pay",
				data: {
					orderId:orderId
					
				},
				success:(obj) => {
					
					if (obj === true ) {
						table.row({
			                 selected : true
			               }).remove();
						 table.draw();
						 
						 swal("Paid!", " paid successfully.", "success");
					} 
				} ,
				  error: function(jqXHR, textStatus, errorThrown) {
					  swal("Error to pay!", "Sorry, but it was not possible to remove this record!", "error");
				  }           
			})
			}else {
				swal("Canceled!", " canceled successfully.", "error");
		    	
		    }
			
		});
	
	console.log(id)
}

function getColumnDefs() {
	return [{
        targets: [],
        visible: false
    }];
}
